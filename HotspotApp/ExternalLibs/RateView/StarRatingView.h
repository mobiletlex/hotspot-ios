//
//  StarRatingView.h
//  StarRatingDemo
//
//  Created by HengHong on 5/4/13.
//  Copyright (c) 2013 Fixel Labs Pte. Ltd. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol StartRateDelegate <NSObject>

-(void) rateDidChangeTo:(int) rate;

@end

@interface StarRatingView : UIView

@property (assign) id<StartRateDelegate> delegate;

- (id)initWithFrame:(CGRect)frame andRating:(int)rating withLabel:(BOOL)label animated:(BOOL)animated;


@end