//
//  CategoryDate.h
//  RaoVatApp
//
//  Created by hmchau on 6/15/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface CategoryDate : BaseModel

@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * identifier;


@end
