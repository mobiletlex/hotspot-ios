//
//  EventInfo.h
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface EventInfo : BaseModel

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * eventId;
@property (nonatomic, retain) NSString * eventUrl;


@end
