//
//  NewsData.h
//  RaoVatApp
//
//  Created by hmchau on 6/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface NewsData : BaseModel

@property (nonatomic,retain) NSString *title;
@property (nonatomic,retain) NSString *content;
@property (nonatomic,retain) NSString *idnew;

@end
