//
//  ZoneInfo.m
//  HotspotApp
//
//  Created by hmchau on 7/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ZoneInfo.h"

@implementation ZoneInfo

@synthesize location;
@synthesize zoneId;
@synthesize name;

@end
