//
//  LandmarkInfo.h
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface LandmarkInfo : BaseModel

@property (assign) CLLocationCoordinate2D location;
@property (assign) int locatinId;
@property (assign) int zoneId;

@property (assign) BOOL unlocked;
@property (assign) BOOL unlock_required;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * funfact;

@property (nonatomic, retain) NSMutableArray * photos;



@end
