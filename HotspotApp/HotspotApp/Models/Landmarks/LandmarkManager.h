//
//  LandmarkManager.h
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LandmarkInfo.h"

@interface LandmarkManager : NSObject

+ (LandmarkManager *) shareInstance;

@property (readonly) NSArray * foundUnlockLandmarks;

- (BOOL) scanLandmarkInRegion:(CLLocationCoordinate2D) location;

-(void) resetWithLandmarkList:(NSArray *) array;
-(void) resetZoneList:(NSArray *) array;
-(CLLocationCoordinate2D) getZoneLocatonById:(int) zoneid;
-(void) resetEventList:(NSArray *) array;


- (BOOL) checkUnlockedLocationId:(NSString *) locationId;
- (LandmarkInfo *) getLocationDetailById:(int) locationId;
- (NSString *) zoneNameById:(int) zoneid;
- (NSString *) getZoneIdNearByLocation:(CLLocationCoordinate2D) location;

-(void) addArchivalLocationIfNeed:(NSString *) locationId;
-(NSDictionary *) generateTableSessionArray;
-(NSMutableArray *) getEventList;
-(int) countUnreadEvent;
-(int) openEventAtIndex:(int) index;

@end
