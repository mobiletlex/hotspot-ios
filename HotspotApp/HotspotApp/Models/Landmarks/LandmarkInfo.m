//
//  LandmarkInfo.m
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LandmarkInfo.h"

@implementation LandmarkInfo

@synthesize location;
@synthesize locatinId;
@synthesize zoneId;

@synthesize unlocked;
@synthesize unlock_required;
@synthesize name;
@synthesize address;
@synthesize funfact;

@synthesize photos;

@end
