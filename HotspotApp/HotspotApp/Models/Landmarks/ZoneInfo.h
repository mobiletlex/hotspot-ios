//
//  ZoneInfo.h
//  HotspotApp
//
//  Created by hmchau on 7/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface ZoneInfo : BaseModel

@property (assign) CLLocationCoordinate2D location;
@property (assign) int zoneId;
@property (nonatomic, retain) NSString * name;

@end
