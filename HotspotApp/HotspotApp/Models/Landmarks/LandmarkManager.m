//
//  LandmarkManager.m
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LandmarkManager.h"
#import "ZoneInfo.h"
#import "JsonUtil.h"
#import "SBJSON.h"
#import "CommonUtils.h"
#import "EventInfo.h"

//@interface LocationUnlockedInfo : NSObject
//@property (retain, nonatomic) NSString *locationId;
//@property (retain, nonatomic) NSString *timeStamp;
//@end
//
//@implementation LocationUnlockedInfo
//
//@synthesize locationId;
//@synthesize timeStamp;
//
//@end



@implementation LandmarkManager{
    NSMutableArray *_zoneList;
    NSMutableArray *_eventList;

    NSMutableArray *_landmarkList;
    NSMutableArray *_justScanFoundArray;
    
    NSMutableDictionary *_unlockedLocationArray;
    NSMutableArray *_eventItemRead;
    int _unreadEventCount;

}

@synthesize foundUnlockLandmarks = _justScanFoundArray;

static LandmarkManager* instance;
+ (LandmarkManager *) shareInstance{
    
    if (instance == nil) {
        instance = [[LandmarkManager alloc] init];
    }
    
    return instance;
}


-(id) init{
    self = [super init];
    _unreadEventCount = -1;
    _landmarkList = [[NSMutableArray alloc] init];
    _justScanFoundArray = [[NSMutableArray alloc] init];
    _unlockedLocationArray = [[NSMutableDictionary alloc] init];
    _zoneList = [[NSMutableArray alloc] init];
    _eventItemRead = [[NSMutableArray alloc] init];
    _eventList = [[NSMutableArray alloc] init];
    
    [self loadUnlockIdList];
    [self loadReadEventList];
    
    return self;
}


#pragma mark - EVENT LIST

-(void) saveReadEventList{
    NSString *jsonString = [JsonUtil getJsonStringFromArray:_eventItemRead];

    if (jsonString.length != 0) {
        [[NSUserDefaults standardUserDefaults] setValue:jsonString forKey:@"readEventArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

-(void) loadReadEventList{
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] stringForKey:@"readEventArray"];
    if (jsonString != nil) {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        _eventItemRead = [jsonParser objectWithString:jsonString];
    }
}

-(int) countUnreadEvent{
    if (_unreadEventCount >= 0) {
        return _unreadEventCount;
    }
    
    int count = 0;
    for (EventInfo *item in _eventList ) {
        BOOL unread = [self isUnreadEvent:item.eventId];
        
        if (unread) {
            //not found
            count ++;
        }
    }
    _unreadEventCount = count;
    return count;
}

-(int) openEventAtIndex:(int) index{
    
    
    index = _eventList.count - index - 1;
    
    int isunread = 0;
    if (index >= 0 && index < _eventList.count) {
        
        EventInfo *item = _eventList[index];
        if (item != nil) {
            
            BOOL unread = [self isUnreadEvent:item.eventId];
            
            if (unread) {
                [_eventItemRead addObject:item.eventId];
                
                //save read even list
                [self saveReadEventList];
                
                _unreadEventCount --;
                if (_unreadEventCount < 0) {
                    _unreadEventCount = 0;
                }
                
                isunread = 1;
                
            }
            
            if (item.eventUrl != nil && item.eventUrl.length > 0) {
                BOOL canopen = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.eventUrl ]];
                if (!canopen) {
                    isunread= -1;
                }
            }else{
                isunread= -1;
            }
        }
    }
    
    return isunread;
}

-(BOOL) isUnreadEvent:(NSString *) eventId{
    
    int i;
    for (i = 0; i < _eventItemRead.count; i ++) {
        NSString *eventid = _eventItemRead[i];
        if ([eventid isEqualToString:eventId]) {
            //Event have been read
            return NO;
        }
    }
    
    //unread event
    return YES;
}


-(NSMutableArray *) getEventList{
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    NSString *color;
    for (int i = 0; i < _eventList.count; i++) {
        EventInfo *item = _eventList[i];
        BOOL unread = [self isUnreadEvent:item.eventId];
        if (unread) {
            color = @"#1DA59B";
        }else{
            color = @"#000000";
        }
        
        NSString *text = [NSString stringWithFormat:@"<font color='#%@' size=18 face='Futura-Bold'>%@</font>\n<font size=17 face='FuturaOblique'>%@</font>",color, item.title, item.content];
        
        //[arr addObject:text];
        [arr insertObject:text atIndex:0];
    }
    
    return arr;
}




#pragma mark - LOCATION
-(BOOL) checkUnlockedLocationId:(NSString *) locationId{
//hmchau alway unlock location
    
    return YES;
    
    
//    if (locationId == nil || [locationId isEqualToString:@""]) {
//        return NO;
//    }
//    
//    
//    for (NSString* key in _unlockedLocationArray.allKeys) {
//        NSArray * arr = [_unlockedLocationArray valueForKey:key];
//        for (NSString *item in arr) {
//            
//            if ([item isEqualToString:locationId]) {
//                return YES;
//            }
//        }
//    }
//    
//    return NO;
}

- (void) loadUnlockIdList{
    [_unlockedLocationArray removeAllObjects];
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] stringForKey:@"locationIdArrayJson"];
    
    if (jsonString != nil) {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        _unlockedLocationArray = [jsonParser objectWithString:jsonString];
    }
}

- (void) saveUnlockIdList{
    NSString *jsonString = [JsonUtil getJsonStringFromDictionary:_unlockedLocationArray];
    
    if (jsonString.length != 0) {
        [[NSUserDefaults standardUserDefaults] setValue:jsonString forKey:@"locationIdArrayJson"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


#define DISTANCE_TO_UNLOCK_LOCATION 200


//hmchau not use any more
- (BOOL) scanLandmarkInRegion:(CLLocationCoordinate2D) location{
    
    BOOL scanningOut = NO;

//    if (! CLLocationCoordinate2DIsValid(location)) {
//        return NO;
//    }
//    
//    [_justScanFoundArray removeAllObjects];
//    
//    CLLocationCoordinate2D userCoordinate = location;
//    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:userCoordinate.latitude longitude:userCoordinate.longitude];
//    
//    for (LandmarkInfo *landmark in _landmarkList) {
//        //neu da save roi thi ko can check lai
//        if (landmark.unlocked || landmark.unlock_required == NO) {
//            continue;
//        }
//        
//        //can check lai cho nay
//        if ( [self checkUnlockedLocationId:[NSString stringWithFormat:@"%d", landmark.locatinId]]) {
//            
//            continue;
//        }
//        
//        CLLocation *pinLocation = [[CLLocation alloc] initWithLatitude:landmark.location.latitude longitude:landmark.location.longitude ];
//        
//        CLLocationDistance distanceinmetre = [pinLocation distanceFromLocation:userLocation];
//        if (distanceinmetre <= DISTANCE_TO_UNLOCK_LOCATION ) {
//            
//            scanningOut = YES;
//            
//            //unlock landmark on list
//            landmark.unlocked = YES;
//            
//            [self addUnlockedLocationId: [NSString stringWithFormat:@"%d", landmark.locatinId] ];
//            
//            [_justScanFoundArray addObject:landmark];
//        }
//    }
//    
//    if (scanningOut) {
//        [self saveUnlockIdList];
//    }
    
    
    return scanningOut;
}

-(void) addArchivalLocationIfNeed:(NSString *) locationId{
    
    if (locationId == nil || [locationId isEqualToString:@""]) {
        return ;
    }


    for (NSString* key in _unlockedLocationArray.allKeys) {
        NSArray * arr = [_unlockedLocationArray valueForKey:key];
        for (NSString *item in arr) {

            if ([item isEqualToString:locationId]) {
                
                //Added already
                return ;
            }
        }
    }
    
    [self addUnlockedLocationId:locationId];
    
}

-(void) addUnlockedLocationId:(NSString *) locationid{

    
    NSDateComponents *datetime = [CommonUtils getSystemTime];
    int month = datetime.month;
    NSString *monthstr = [NSString stringWithFormat : month > 9 ? @"%d":@"0%d", month];
    int date = datetime.day;
    NSString *datestr = [NSString stringWithFormat : date > 9 ? @"%d":@"0%d", date];
    NSString *timestamp = [NSString stringWithFormat:@"%d%@%@", datetime.year, monthstr, datestr];

    NSMutableArray *arr = [_unlockedLocationArray objectForKey:timestamp];
    if (arr) {
        for (NSString *item in arr) {
            if ([item isEqualToString:locationid]) {
                return;
            }
        }
        
        [arr addObject:locationid];
        
    }else{
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [arr addObject:locationid];
        [_unlockedLocationArray setObject:arr forKey:timestamp];
    }
    
    [self saveUnlockIdList];
}

-(NSDictionary *) generateTableSessionArray{
    return [[NSDictionary alloc] initWithDictionary:_unlockedLocationArray copyItems:YES];
}



-(void) resetWithLandmarkList:(NSArray *) array{
    [_landmarkList removeAllObjects];
    [_landmarkList addObjectsFromArray:array];
}

-(void) resetZoneList:(NSArray *) array{
    [_zoneList removeAllObjects];
    [_zoneList addObjectsFromArray:array];
}

-(void) resetEventList:(NSArray *) array{
    [_eventList removeAllObjects];
    [_eventList addObjectsFromArray:array];
}

-(CLLocationCoordinate2D) getZoneLocatonById:(int) zoneid{
    for (ZoneInfo *zone in _zoneList) {
        if (zone.zoneId == zoneid ) {
            return zone.location;
        }
    }
    
    return CLLocationCoordinate2DMake(0, 0);
}

- (LandmarkInfo *) getLocationDetailById:(int) locationId{
    for (LandmarkInfo *landmark in _landmarkList) {
        if (landmark.locatinId == locationId) {
            return landmark;
        }
    }
    
    return nil;
}


- (NSString *) zoneNameById:(int) zoneid{
    
    for (ZoneInfo *zone in _zoneList) {
        if (zoneid == zone.zoneId) {
            return zone.name;
        }
    }
    
    return @"Unknow";
}

- (NSString *) getZoneIdNearByLocation:(CLLocationCoordinate2D) location{
    
    if(_zoneList == nil || _zoneList.count == 0)
        return nil;
    
    ZoneInfo *foundzone = nil;
    float nearestDistance = -1.0;
    
    CLLocation *userlocation = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude ];
    
    for (ZoneInfo *zone in _zoneList) {
        
        CLLocation *zonelocation = [[CLLocation alloc] initWithLatitude:zone.location.latitude longitude:zone.location.longitude ];

        CLLocationDistance distanceinmetre = [zonelocation distanceFromLocation:userlocation];

        if (nearestDistance == -1 || distanceinmetre < nearestDistance) {
            nearestDistance = distanceinmetre;
            foundzone = zone;
        }
    }
    
    if ( foundzone != nil){
        return [NSString stringWithFormat:@"%d",foundzone.zoneId];
    }
    
    return nil;
}


@end
