//
//  AccountInfo.h
//  RaoVatApp
//
//  Created by hmchau on 6/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface AccountInfo : BaseModel
@property (strong, nonatomic) NSString * fullNname;
@property (strong, nonatomic) NSString * phoneNumber;
@property (strong, nonatomic) NSString * fax;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * userId;
@property ( nonatomic) bool rejectNews;
@property ( nonatomic) bool hideEmail;
@property ( nonatomic) int gold;
@property ( nonatomic) int numberOfNews;
@end

