//
//  PoemInfo.h
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"
#import "PoetIInfo.h"

@interface PoemInfo : BaseModel

@property (nonatomic, retain) NSString * poemid;

@property (assign) CLLocationCoordinate2D location;

@property (assign) int locationId;
@property (assign) int zoneId;
@property (retain,nonatomic) NSString* locationName;//Just for display

@property (assign) int languageId;

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSArray * photos;
@property (nonatomic, retain) NSArray * photoCaptions;
@property (assign) BOOL liked;
@property (assign) BOOL reported;

@property (nonatomic, retain) NSString * audiolink;
@property (nonatomic, retain) NSString * urlshare;
@property (nonatomic, retain) NSString * poemUrlShare;
@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * updated_at;
@property (assign) float average_rating;

@property (nonatomic, retain) PoemInfo *subLanguagePoem;
@property (nonatomic, retain) PoetIInfo *poetInfo;


@property (assign) BOOL isFeaturedPoem;
@property (assign) BOOL isSubmitted;

-(BOOL) hasSubLanguage;

@end
