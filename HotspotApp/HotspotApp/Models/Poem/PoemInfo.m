//
//  PoemInfo.m
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "PoemInfo.h"

@implementation PoemInfo

@synthesize poemid;

@synthesize location;
@synthesize locationId;
@synthesize zoneId;
@synthesize locationName;


@synthesize title;
@synthesize content;
@synthesize author;
@synthesize photos;
@synthesize photoCaptions;
@synthesize liked;
@synthesize reported;

@synthesize audiolink;
@synthesize urlshare;
@synthesize poemUrlShare;

@synthesize category_id;
@synthesize updated_at;
@synthesize average_rating;

@synthesize languageId;
@synthesize subLanguagePoem;
@synthesize poetInfo;


//Not in json
@synthesize isFeaturedPoem;
@synthesize isSubmitted;


-(id) init{
    self = [super init];
    
    self.isFeaturedPoem = YES;
    self.average_rating = 0.0;
    
    return self;
}

-(BOOL) hasSubLanguage{
    if (subLanguagePoem != nil) {
        return YES;
    }
    
    return NO;
}


@end
