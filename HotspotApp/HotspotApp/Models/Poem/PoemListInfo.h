//
//  PoemListInfo.h
//  HotspotApp
//
//  Created by hmchau on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface PoemListInfo : BaseModel

@property (assign) int totalPage;
@property (assign) int currentPage;
@property (nonatomic, retain) NSMutableArray * dataArray;

@end
