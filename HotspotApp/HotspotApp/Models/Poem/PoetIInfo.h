//
//  PoetIInfo.h
//  HotspotApp
//
//  Created by hmchau on 8/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface PoetIInfo : BaseModel

@property (nonatomic, retain) NSString * poetId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * pen_name;
@property (nonatomic, retain) NSString * bio;
@property (nonatomic, retain) NSString * photourl;

@end
