//
//  PoetIInfo.m
//  HotspotApp
//
//  Created by hmchau on 8/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "PoetIInfo.h"

@implementation PoetIInfo


@synthesize poetId;
@synthesize name;
@synthesize pen_name;
@synthesize bio;
@synthesize photourl;

@end
