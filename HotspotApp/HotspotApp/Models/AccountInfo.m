//
//  AccountInfo.m
//  RaoVatApp
//
//  Created by hmchau on 6/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "AccountInfo.h"

@implementation AccountInfo

-(id) init{
    self = [super init];
    self.fullNname = @"Chris Lee";
    self.userName = @"mail@mail.com";
    self.phoneNumber = @"012345678";
    self.rejectNews = NO;
    self.hideEmail = YES;
    return self;
}
@end
