//
//  ImageSessionManager.h
//  RaoVatApp
//
//  Created by hmchau on 6/12/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageSessionItem.h"

@interface ImageSessionManager : NSObject

@property (nonatomic,retain) NSMutableArray *arrayImage;

-(void) addImage:(UIImage *)newimage;
-(void) removeImageAtIndex:(int) index;
-(void) selectedAtIndex:(int) index;

-(void) resetImages;

@end
