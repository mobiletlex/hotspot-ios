//
//  GoogleLocationInfo.h
//  HotspotApp
//
//  Created by hmchau on 9/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface GoogleLocationInfo : BaseModel
    @property (nonatomic, retain) NSString* name;
    @property (nonatomic, retain) NSString* address;
@end
