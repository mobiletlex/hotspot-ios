//
//  ImageSessionManager.m
//  RaoVatApp
//
//  Created by hmchau on 6/12/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ImageSessionManager.h"

@implementation ImageSessionManager{
    int _itemCount;
}

@synthesize arrayImage;

-(id) init {
    
    self = [super init];
    if (self) {
        self.arrayImage = [[NSMutableArray alloc] initWithCapacity:10];
        _itemCount = 0;
    }
    
    return self;
}


-(void) addImage:(UIImage *)newimage{
    ImageSessionItem *item = [[ImageSessionItem alloc] init];
    _itemCount ++;
    
    item.image = newimage;
    item.selected = NO;
    item.name = [[NSString alloc] initWithFormat:@"%d",_itemCount];
    
    [self.arrayImage addObject:item];
}

-(void) removeImageAtIndex:(int) index{
    if (index >= 0 && index < self.arrayImage.count) {
        ImageSessionItem *item = [self.arrayImage objectAtIndex:index];
        if (! item.selected) {
            [self.arrayImage removeObjectAtIndex:index];
        }
    }
}

-(void) selectedAtIndex:(int) index{
    if (index >= 0 && index < self.arrayImage.count) {
        ImageSessionItem *item = [self.arrayImage objectAtIndex:index];
        item.selected = YES;
    }
}


-(void) resetImages{
    [self.arrayImage removeAllObjects];
}

@end
