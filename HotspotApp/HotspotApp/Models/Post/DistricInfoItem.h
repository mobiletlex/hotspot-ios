//
//  DistricInfoData.h
//  RaoVatApp
//
//  Created by reoxinh on 6/22/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseModel.h"

@interface DistricInfoItem : BaseModel
@property (strong, nonatomic) NSString * districtName;
@property (strong, nonatomic) NSString * districtID;
@property (strong, nonatomic) NSString * locationID;
@end
