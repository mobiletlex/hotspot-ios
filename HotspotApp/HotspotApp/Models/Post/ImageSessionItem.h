//
//  ImageSessionItem.h
//  RaoVatApp
//
//  Created by hmchau on 6/12/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageSessionItem : NSObject

@property (retain, nonatomic) UIImage *image;
@property (assign) BOOL selected;
@property (strong, nonatomic) NSString *name;

@end
