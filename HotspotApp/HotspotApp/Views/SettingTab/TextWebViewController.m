//
//  TextWebViewController.m
//  HotspotApp
//
//  Created by hmchau on 8/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "TextWebViewController.h"

@interface TextWebViewController ()

@end

@implementation TextWebViewController
@synthesize htmlText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.htmlText) {
        
        NSString *htmlString = [self.htmlText stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        htmlString = [NSString stringWithFormat:@"<font face='Futura'>%@", htmlString];
        [self.webView loadHTMLString:htmlString baseURL:nil];
    }
}


@end
