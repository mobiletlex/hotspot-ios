//
//  SettingViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingViewCtrl : BaseViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *logoutBtn;

@property (weak, nonatomic) IBOutlet UIButton *eventUnreadNotifyBtn;


- (IBAction)onAboutBtn:(id)sender;
- (IBAction)onTermConditionsBtn:(id)sender;
- (IBAction)onCreditBtn:(id)sender;
- (IBAction)onPrivacyBtn:(id)sender;
- (IBAction)onCompetittionBtn:(id)sender;

- (IBAction)onTuturialPage:(id)sender;

@end
