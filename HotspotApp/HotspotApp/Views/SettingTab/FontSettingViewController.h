//
//  FontSettingViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/27/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface FontSettingViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UIWebView *previewContentWeb;

- (IBAction)onSliderValueChanged:(id)sender;

@end
