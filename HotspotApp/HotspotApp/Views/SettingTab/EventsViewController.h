//
//  EventsViewController.h
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface EventsViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
