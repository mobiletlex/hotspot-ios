//
//  TextWebViewController.h
//  HotspotApp
//
//  Created by hmchau on 8/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface TextWebViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (retain,nonatomic) NSString *htmlText;

@end
