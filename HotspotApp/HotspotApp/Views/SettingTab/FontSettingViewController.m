//
//  FontSettingViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/27/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "FontSettingViewController.h"
#import "AppConfiguration.h"

@interface FontSettingViewController ()

@end

@implementation FontSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#define CONTENT_STANDARD_FONT_SIZE 3.0

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (int i = 1; i < 2; i++) {
        UILabel *bt = (UILabel *)[self.view viewWithTag:i];
        if (bt) {
            bt.font = [UIFont fontWithName:@"Futura-Bold" size:17];
        }
    }
    
    
    [self setupProgressbarControl];
    
    self.slider.value = [AppConfiguration sharedInstance].fontSizePreview;
    
    //float fontsize = 15.0 * [AppConfiguration sharedInstance].fontSizePreview;
    float fontsize = CONTENT_STANDARD_FONT_SIZE * [AppConfiguration sharedInstance].fontSizePreview;
    self.previewContentWeb.scalesPageToFit = YES;

    [self updateFontPreview:fontsize];
}

-(void) setupProgressbarControl{
    
    [self.slider setThumbImage:[UIImage imageNamed:@"img_slider_thumb.png"] forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:[[UIImage imageNamed:@"video_slider_colored.png"] stretchableImageWithLeftCapWidth:0.3 topCapHeight:0.0] forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:[[UIImage imageNamed:@"video_slider_gray.png"] stretchableImageWithLeftCapWidth:0.3 topCapHeight:0.0] forState:UIControlStateNormal];
}

- (IBAction)onSliderValueChanged:(UISlider *)sender {
    
    float value = sender.value;
    
    [AppConfiguration sharedInstance].fontSizePreview = value;
    [[AppConfiguration sharedInstance] saveConfig];
    
    float fontsize = CONTENT_STANDARD_FONT_SIZE * [AppConfiguration sharedInstance].fontSizePreview;
    [self updateFontPreview:fontsize];
}

-(void) updateFontPreview:(float) fontsize{
    
    NSString *content = @"Donec convallis tellus non tortor semper, a suscipit tellus elementum. Maecenas varius erat a auctor congue.";

    NSString *embedHTML = @"<html><body><font face='Futura' size='%f'><meta name='viewport' content='width=device-width'/>";
    
    NSString *endHtml=@"</font></body></html>";
    NSString *htmltext = [NSString stringWithFormat:embedHTML,fontsize ];
    
    NSString *strDes=[NSString stringWithFormat:@"%@%@%@",htmltext, content ,endHtml];
    [self.previewContentWeb loadHTMLString:strDes baseURL:nil];
}

@end
