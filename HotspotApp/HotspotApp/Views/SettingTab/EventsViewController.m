//
//  EventsViewController.m
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "EventsViewController.h"
#import "RTLabel.h"
#import "LandmarkManager.h"

@interface EventsViewController ()

@end

@implementation EventsViewController{
    NSMutableArray *textArray;
    NSMutableArray *itemHeightArray;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    itemHeightArray = [[NSMutableArray alloc] init];
    textArray = [[LandmarkManager shareInstance] getEventList];
    
    RTLabel *contentLabel = [[RTLabel alloc] initWithFrame:CGRectMake(0, 0, 315, 10)];

    for (int i = 0; i < textArray.count; i ++) {
        
        contentLabel.frame = CGRectMake(0, 0, 315, 10);
        contentLabel.text = textArray[i];
        CGSize optimumSize = [contentLabel optimumSize];
        
        [itemHeightArray addObject:[NSNumber numberWithInt:optimumSize.height + 20]];
    }
}


-(void) calculateHeight{
    
}

#pragma mark - UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return textArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSNumber *number = itemHeightArray[indexPath.row];
    return [number intValue];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    RTLabel *contentLabel = (RTLabel *)[cell.contentView viewWithTag:1];
    //contentLabel.font = [UIFont fontWithName:@"Futura-Medium" size:16.0];
    
    contentLabel.text = textArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int openunreadevent = [[LandmarkManager shareInstance] openEventAtIndex:indexPath.row];
    if (openunreadevent !=0) {
        
        NSString *text = textArray[indexPath.row];
        text = [text stringByReplacingOccurrencesOfString:@"#1DA59B" withString:@"#000000"];
        [textArray replaceObjectAtIndex:indexPath.row withObject:text];
        
        [self.tableView reloadData];
        
        //[APP_DELEGATE updateUnreadEventCountBadge];
        
        if (openunreadevent == -1) {
            [self showAlert:@"Can not open event detail page"];
        }
    }
}

@end
