//
//  SettingViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "SettingViewCtrl.h"
#import "AppConfiguration.h"
#import "TextWebViewController.h"
#import "CommonUtils.h"
#import "LandmarkManager.h"

@interface SettingViewCtrl () <UIAlertViewDelegate>

@end

@implementation SettingViewCtrl{
    
    NSString *_viewTitle;
    NSString *_htmlText;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set up font
    for (int i = 1; i <= 8; i++) {
        UIButton *bt = (UIButton *)[self.view viewWithTag:i];
        if (bt) {
            bt.titleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:17];
        }
    }
    
    
    if (IS_IPHONE_4) {
        for (UIView *view in self.view.subviews) {
            view.center = CGPointMake(view.center.x, view.center.y + 40);
        }
    }
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    int unreadcount = [[LandmarkManager shareInstance] countUnreadEvent];
    if (unreadcount != 0) {
        NSString *text = [NSString stringWithFormat:@"%d", unreadcount];
        [self.eventUnreadNotifyBtn setTitle:text forState:UIControlStateNormal] ;
    }else{
        self.eventUnreadNotifyBtn.hidden = YES;
    }

    
    
    if ([AppConfiguration isLogin]) {
        self.logoutBtn.enabled = YES;
        [self.logoutBtn setImage:[UIImage imageNamed:@"btn_logout.png"]];

    }else{
        
        self.logoutBtn.enabled = NO;
        [self.logoutBtn setImage:nil];

    }
}


- (IBAction)onLogoutBtn:(UIBarButtonItem * )sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to log out?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Log out", nil];
    [alert show];
    
}


#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.cancelButtonIndex != buttonIndex) {
        [[AppConfiguration sharedInstance] clearSession];
        
        //[self showAlert:@"Signout done"];
        [APP_DELEGATE appLogOut];
        
        self.logoutBtn.enabled = NO;
        [self.logoutBtn setImage:nil];
    }
}

-(void) readFileContent:(NSString *) file{
    
    _htmlText = @"";
    
    NSString *filename = [CommonUtils resoucePathForFileName:file];
    NSData *filedata = [NSData dataWithContentsOfFile:filename];
    if (filedata) {
        _htmlText = [[NSString alloc] initWithData:filedata encoding:NSUTF8StringEncoding];
    }

}

- (IBAction)onAboutBtn:(id)sender {
    _viewTitle = @"About us";
    [self readFileContent:@"about.txt"];

    [self performSegueWithIdentifier:@"SEGUE_SHOW_WEBTEXT" sender:nil];
}

- (IBAction)onTermConditionsBtn:(id)sender {
    _viewTitle = @"Terms and Conditions";
    [self readFileContent:@"termsandconditions.txt"];

    [self performSegueWithIdentifier:@"SEGUE_SHOW_WEBTEXT" sender:nil];

}

- (IBAction)onCreditBtn:(id)sender {
    _viewTitle = @"Credit";
    [self readFileContent:@"credit.txt"];
    [self performSegueWithIdentifier:@"SEGUE_SHOW_WEBTEXT" sender:nil];

}

- (IBAction)onPrivacyBtn:(id)sender {
    _viewTitle = @"Privacy";
    [self readFileContent:@"privacy.txt"];

    [self performSegueWithIdentifier:@"SEGUE_SHOW_WEBTEXT" sender:nil];
}

- (IBAction)onCompetittionBtn:(id)sender {
//    _viewTitle = @"Competition";
//    [self readFileContent:@"competition.txt"];
//    [self performSegueWithIdentifier:@"SEGUE_SHOW_WEBTEXT" sender:nil];
    
    if ([SERVICE_BASE_URL hasPrefix:@"https"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://textinthecity.sg/competition"]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://textinthecity.epsilon-mobile.com/competition"]];
    }
}

- (IBAction)onTuturialPage:(id)sender {
    
    [APP_DELEGATE showTutorialPage];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_SHOW_WEBTEXT"]) {
        TextWebViewController *vc = (TextWebViewController *)segue.destinationViewController;

        vc.title = _viewTitle;
        vc.htmlText = _htmlText;
    }
}

@end
