//
//  BIOViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BIOViewController.h"
#import "UIImageView+WebCache.h"

@interface BIOViewController ()

@end

@implementation BIOViewController

@synthesize parentController;
@synthesize poetInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.headerTitleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
    
    self.view.layer.borderWidth = 1.0;
    self.view.layer.borderColor = [UIColor darkGrayColor].CGColor;

    
    if (self.poetInfo) {
        self.poetName.text = self.poetInfo.name;
        self.poetContent.text = self.poetInfo.bio;
        
        NSString *url = self.poetInfo.photourl;
        if (url != nil && ! [url isEqualToString:@""]) {
            [self.poetImage setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"img_no_photo.png"]];
        }else{
            self.poetImage.image = [UIImage imageNamed:@"img_no_photo.png"];
        }
    }
    
}


- (IBAction)onCloseBtn:(id)sender {
    
    if (self.parentController) {
        [self.parentController performSelector:@selector(dismissPopoverViewController)];
    }
}
@end
