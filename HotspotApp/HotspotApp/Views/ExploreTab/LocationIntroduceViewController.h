//
//  LocationIntroduceViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
#import "LandmarkInfo.h"
#import "PoemInfo.h"

@protocol LocationIntroduceDelegate <NSObject>

-(void) onCloseButton;

@end

@interface LocationIntroduceViewController : BaseViewController

@property (assign) id<LocationIntroduceDelegate> delegate;

@property (nonatomic,retain) LandmarkInfo *locationInfo;
@property (weak, nonatomic) IBOutlet UITextView *funfactTextview;
@property (weak, nonatomic) IBOutlet UIPageControl *slicePageControl;
@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (IBAction)onClosePopupBtn:(id)sender;

@end
