//
//  DistricMapViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "DistricMapViewCtrl.h"
#import "DistricParser.h"
#import "PoemInfo.h"
#import "LandmarkManager.h"
#import "AppDelegate.h"
#import "LandmarkParser.h"
#import "Location.h"
#import "DetailViewController.h"
#import "AppUtils.h"

#import "WEPopoverController.h"
#import "LocationPoemViewController.h"

@interface DistricMapViewCtrl () <MKMapViewDelegate, UITableViewDelegate, LocationPoemViewControllerDelegate> {
    NSMutableArray *_poemList;
    NSMutableArray *_locationList;
    NSMutableArray *_poemListInLocation;

    int _selectIndex;
    
    int _currentPage;
    int _totalPage;
    NSMutableDictionary *_pinOrderNameDic;
    
    WEPopoverController *_popoverController;
    NSString *_locationNameSelection;
}

@end

@implementation DistricMapViewCtrl
@synthesize districtID;


#define ITEMS_PER_PAGE 10

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _locationList = [[NSMutableArray alloc] init];
    _poemList = [[NSMutableArray alloc] init];
    _pinOrderNameDic = [[NSMutableDictionary alloc] init];
    _poemListInLocation = [[NSMutableArray alloc] init];

    
    _currentPage = 0;
    _totalPage = 0;
    
    
    self.mapView.delegate = self;
    //AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;    
    
    CLLocationCoordinate2D zonelocation = [[LandmarkManager shareInstance] getZoneLocatonById:[districtID intValue]];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(zonelocation, 20000, 20000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];

    //Load location
    NSString *geturl = [NSString stringWithFormat:SERVICE_REQUEST_LOCATIONS_IN_ZONE, districtID];
    [self sendHttpGetRequest:geturl andRequestId:1];
    
    NSString *zonename = [[LandmarkManager shareInstance] zoneNameById:[districtID intValue] ];
    self.zoneNameLabel.text = zonename;
    
    if ([zonename hasSuffix:@"s"] || [zonename hasSuffix:@"S"]) {
        self.poemZoneTitle.text = [NSString stringWithFormat:@"%@' Poems",zonename];
    }else{
        self.poemZoneTitle.text = [NSString stringWithFormat:@"%@'s Poems",zonename];
    }
    
    [APP_DELEGATE GALTrackerInCategory:@"Zone" withAction:@"Open" withLabel:zonename];    
}

-(void) startLoadPoemList{
    //Load poem list
    NSString *geturl = [SERVICE_GET_POEMLIST stringByAppendingFormat:@"?sort=location&zone_id=%@&per_page=%d&page=%d&category_id=%@",districtID,ITEMS_PER_PAGE, _currentPage + 1,POEM_CATEGORY_FEATURED ];

    
    [self sendHttpGetRequest:geturl andRequestId:2];
}

- (void) loadMorePoemList{
    [self startLoadPoemList];
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (IBAction)onMyLocationBtn:(id)sender {
    
    if (! [CLLocationManager locationServicesEnabled]){
        [self showAlert:@"Can not detect your location, please turn on and allow access Location service"];
    }
    
    [self.mapView setCenterCoordinate:self.mapView.userLocation.location.coordinate animated:YES];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if ([self loadMoreAvailable]) {
//
//        return 35.0;
//    }
    
    return 5.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if ([self loadMoreAvailable]) {
        
        UITableViewHeaderFooterView *footer = [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        label.textAlignment = NSTextAlignmentCenter;
        //label.text = @"Loading more...";
        [footer addSubview:label];
        
        return footer;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
    if ([item hasSubLanguage]) {
        return  100.0;
    }
    
    return 80.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _poemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"DistricMapCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    UILabel *centralname = (UILabel *)[cell.contentView viewWithTag:101];
    UILabel *authorname = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *titlename = (UILabel *)[cell.contentView viewWithTag:103];
    titlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    UILabel *subtitlename = (UILabel *)[cell.contentView viewWithTag:104];
    subtitlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    UILabel *locationName = (UILabel *)[cell.contentView viewWithTag:105];
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
    titlename.text = item.title;
    authorname.text = item.author;
    centralname.text = [[LandmarkManager shareInstance] zoneNameById:item.zoneId];
    
    if ([item hasSubLanguage]) {
        subtitlename.text = item.subLanguagePoem.title;
        //locationName.center = CGPointMake(locationName.center.x, locationName.center.y - 10);
    }else{
        subtitlename.text = @"";
    }
    
    NSString *name = [AppUtils zonePrefixByZoneId: [self.districtID intValue]];
    name = [name stringByAppendingString:[_pinOrderNameDic valueForKey: [NSString stringWithFormat:@"%d", item.locationId] ]];
    locationName.text = name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected posted item index %d", indexPath.row);
    _selectIndex = indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self performSegueWithIdentifier:@"SHOW_DETAIL_SCREEN_FROM_MAPVIEW_SEQUE" sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section{

    if ([self loadMoreAvailable]) {
        //NSLog(@"Loading more in table");

        [self loadMorePoemList];
    }
}


-(BOOL) loadMoreAvailable{
    if (_currentPage < _totalPage) {
        
        return YES;
    }
    
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([segue.identifier isEqualToString:@"SHOW_DETAIL_SCREEN_FROM_MAPVIEW_SEQUE"]){
        DetailViewController *vc = segue.destinationViewController;
        
        if ([sender isKindOfClass:[NSNumber class]]) {
            NSNumber *number = (NSNumber *) sender;
            vc.poemInfo = [_poemListInLocation objectAtIndex:[number intValue]];

        }else
        {
            vc.poemInfo = [_poemList objectAtIndex:_selectIndex];
        }
    }
}

#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    if (data != nil) {
        
        switch (requestId) {
            case 1://locatio nlist
                _locationList = [LandmarkParser parse:data];
                if (_locationList) {
                    //set up map view
                    [self setupMapView];
                    
                    [self startLoadPoemList];
                }
                break;
                
            case 2: //poem follow zone
            {
                PoemListInfo *listpoem = [DistricParser parse:data];
                
                if (listpoem) {
                    _currentPage = listpoem.currentPage;
                    _totalPage = listpoem.totalPage;
                    
                    
                    if (listpoem.dataArray) {
                        int count = _poemList.count;//get before add nrew

                        [_poemList addObjectsFromArray:listpoem.dataArray];
                        NSMutableArray *indexarray = [[NSMutableArray alloc] init];

                        if (listpoem.dataArray.count != 0) {
                            
                            for (int i = 0 ; i < listpoem.dataArray.count; i ++) {
                                NSIndexPath *index = [NSIndexPath indexPathForRow:count + i inSection:0];
                                [indexarray addObject:index];
                            }
                        
                            [self.tableView insertRowsAtIndexPaths:indexarray withRowAnimation:UITableViewRowAnimationAutomatic];
                        }

                    }
                    
                    //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
                }else{
                    [self showNetworkErrMsg];
                }
            }
                break;
                
                
            case 3://poem list follow location selected
            {
                PoemListInfo *listpoem = [DistricParser parseRowPoemList:data];
                [_poemListInLocation removeAllObjects];
                if (listpoem) {

                    if (listpoem.dataArray) {
                        [_poemListInLocation addObjectsFromArray:listpoem.dataArray];
                        if (_poemListInLocation.count > 0) {
                            //show list poem
                            
                            [self showPoemListInLocation];
                        }else{
                            [self showAlert:@"There is not poem in this location"];
                        }
                    }
                }
            }
                break;
             
                
            default:
                break;
        }
        
        
    }else{
        [self showNetworkErrMsg];
    }
    
}

-(void) showPoemListInLocation{
    
    UIStoryboard *storyboard = self.storyboard;
    LocationPoemViewController *locationList = [storyboard instantiateViewControllerWithIdentifier:@"LocationPoemViewController"];
    locationList.delegate = self;
    locationList.poemArray = _poemListInLocation;
    locationList.locationName = _locationNameSelection;
    
    locationList.contentSizeForViewInPopover = CGSizeMake(300 , 440 );
    
    if (!_popoverController) {
        _popoverController = [[WEPopoverController alloc] initWithContentViewController:locationList];
    }
    else{
        _popoverController.contentViewController = locationList;
    }
    
    CGPoint point = self.view.center;
    CGRect rect = CGRectMake(point.x, point.y - 440/2 + 4 , 2, 2);
    
    [_popoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

#pragma mark- MapView Delegates.

-(void) setupMapView{
    NSMutableArray *locarray = [[NSMutableArray alloc] init];
    int count = 1;
    for (LandmarkInfo *item in _locationList) {
        Location *location=[[Location alloc] initWithName:item.name  address:@"" coordinate:item.location];
        
        location.unlocked = [[LandmarkManager shareInstance] checkUnlockedLocationId:[NSString stringWithFormat:@"%d",item.locatinId]];
        
        location.noneedunlock = item.unlock_required == NO;
        
        NSString * pinOrderName = [NSString stringWithFormat: count > 9 ?@"%d" : @"0%d",count];
        location.pinName = pinOrderName;;
        [_pinOrderNameDic setValue:pinOrderName forKey:[NSString stringWithFormat:@"%d", item.locatinId]];
        
        [locarray addObject:location];
        count ++;
    }
    
    [self.mapView removeAnnotations:[self.mapView annotations]];
    [self.mapView addAnnotations:locarray];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        //set as default
        return nil;
    
    Location *location = (Location *)annotation;

    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 23,23)];
    
    NSString *name = [AppUtils zonePrefixByZoneId: [self.districtID intValue]];
    name =  [name stringByAppendingString:location.pinName];
    label.text = name;

    label.font = [UIFont boldSystemFontOfSize:8];
    label.textAlignment = NSTextAlignmentCenter;
    [annotationView addSubview:label];
    
    UIImage *pinImage;
    UIButton * rightButton;
    rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];


    if (location.noneedunlock) {
        label.textColor = [UIColor blackColor];
        pinImage = [UIImage imageNamed:@"img_pin_locked.png"];//Pink
//        rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        rightButton.enabled = NO;

    }else{
        
        label.textColor = [UIColor blackColor];
        pinImage = [UIImage imageNamed:@"img_pin_unlocked.png"];//Green
//        rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        
//        if (location.unlocked) {
//            label.textColor = [UIColor blackColor];
//            pinImage = [UIImage imageNamed:@"img_pin_unlocked.png"];
//            rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        }else{
//            label.textColor = [UIColor whiteColor];
//            pinImage = [UIImage imageNamed:@"img_pin_locked.png"];
//            rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            rightButton.enabled = NO;
//        }
    }
    
    annotationView.image=pinImage;
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    
    annotationView.rightCalloutAccessoryView = rightButton;
    rightButton.tag = [location.pinName intValue];

//    CGRect frame = annotationView.frame;
//    frame.size.height -= 10;
//    annotationView.frame = frame;
    
    return annotationView;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)buttn {
    
    NSLog(@"calloutAccessoryControlTapped");
    int tag = buttn.tag - 1;
    
    if (tag >= 0 && tag < _locationList.count) {
        
        LandmarkInfo *item = _locationList[tag];
        if (item == nil) {
            return;
        }
        
        //[APP_DELEGATE showLocationInfoView:item];
        
        //Load poemlist inlocation
        [self loadPoemListInLocation:item];
    }
}

-(void) loadPoemListInLocation:(LandmarkInfo *) locationInfo{
    
    _locationNameSelection = locationInfo.name;
    
    NSString *geturl = [SERVICE_GET_POEMLIST stringByAppendingFormat:@"?location_id=%d&category_id=%@&sort=created_at&order=desc",locationInfo.locatinId , POEM_CATEGORY_FEATURED ];
    
    [self sendHttpGetRequest:geturl andRequestId:3];
}

#pragma mark - LocationPoemViewControllerDelegate
-(void) onCloseButton{
    if (_popoverController) {
        [_popoverController dismissPopoverAnimated:YES];
    }
}

-(void) onSelectItemAtIndex:(int) index{
    if (_popoverController) {
        [_popoverController dismissPopoverAnimated:YES];
    }
    
    //Move to detail poem page
    [self performSelector:@selector(willOpenDetailAtIndex:) withObject:[NSNumber numberWithInt:index] afterDelay:0.25];
}

 -(void) willOpenDetailAtIndex:(NSNumber* ) indexnumber{
     [self performSegueWithIdentifier:@"SHOW_DETAIL_SCREEN_FROM_MAPVIEW_SEQUE" sender:indexnumber];
 }

@end
