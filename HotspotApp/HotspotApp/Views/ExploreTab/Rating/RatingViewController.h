//
//  RatingViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/20/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface RatingViewController : BaseViewController

@property (nonatomic, weak) UIViewController *parentController;
@property (assign) float currentRate;

@end
