//
//  RatingViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/20/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "RatingViewController.h"
#import "StarRatingView.h"

@interface RatingViewController () <StartRateDelegate>

@end

@implementation RatingViewController{
    
    NSArray *_btnArray;
}
@synthesize currentRate;
@synthesize parentController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *bg = [self.view viewWithTag:1002];
    bg.layer.borderWidth = 1.0;
    bg.layer.borderColor = [UIColor darkGrayColor].CGColor;
    bg.backgroundColor = [UIColor blackColor];
    
    UIView *ratecontainer = [bg viewWithTag:1022];
    [self setupRatingView:ratecontainer];
    
}

-(void) setupRatingView:(UIView *) viewContain{
    
    int rate = (self.currentRate/5.0) * 100;
    StarRatingView* starViewNoLabel = [[StarRatingView alloc]initWithFrame:viewContain.frame andRating:rate  withLabel:NO animated:YES];
    [viewContain.superview addSubview:starViewNoLabel];
    starViewNoLabel.delegate = self;
    
}

-(void) rateDidChangeTo:(int) rate{
    
    NSLog(@"rate change to %d",rate);
    rate = rate/20;
    
    if (self.parentController) {
        NSNumber *point = [NSNumber numberWithInt:rate];
        [self.parentController performSelector:@selector(doRatingWithPoint:) withObject:point afterDelay:0.0];
    }
}


@end
