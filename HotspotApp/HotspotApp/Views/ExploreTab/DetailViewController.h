//
//  DetailViewController.h
//  RaoVatApp
//
//  Created by hmchau on 6/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
#import "PoemInfo.h"

@interface DetailViewController : BaseViewController

@property (nonatomic, retain) PoemInfo* poemInfo;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *poemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *poemTitleLabelMainLanguage;

@property (weak, nonatomic) IBOutlet UIButton *captionImageButton;

@property (weak, nonatomic) IBOutlet UIWebView *poemWebContent;
@property (weak, nonatomic) IBOutlet UIView *galleryContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imageLockIndicator;

@property (weak, nonatomic) IBOutlet UIButton *mainLanguageBtn;
@property (weak, nonatomic) IBOutlet UIView *separateMainLanguageBtn;

@property (weak, nonatomic) IBOutlet UIButton *subLangugeBtn;
@property (weak, nonatomic) IBOutlet UIView *separateSubLanguageBtn;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *bioButton;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@property (weak, nonatomic) IBOutlet UIImageView *iconRightSwipe;
@property (weak, nonatomic) IBOutlet UIImageView *iconLeftSwipe;


//For preview poem only
@property (assign) BOOL isReadOnly;


- (IBAction)onSubLanguageBtn:(id)sender;
- (IBAction)onMainLangugeBtn:(id)sender;
- (IBAction)onLikeBtn:(id)sender;
- (IBAction)onCloseCaptainBtn:(UIButton *)sender;


- (IBAction)onAudioBtn:(id)sender;
- (IBAction)onBioBtn:(id)sender;
- (IBAction)onRatingViewBtn:(UIButton *)sender;
- (void) doRatingWithPoint:(NSNumber *) point;
- (IBAction)onReportBtn:(id)sender;
- (IBAction)onShareBtn:(id)sender;

@end
