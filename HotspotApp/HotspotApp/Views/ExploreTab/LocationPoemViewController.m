//
//  LocationPoemViewController.m
//  HotspotApp
//
//  Created by hmchau on 10/31/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LocationPoemViewController.h"
#import "WEPopoverController.h"
#import "PoemInfo.h"

@interface LocationPoemViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation LocationPoemViewController

@synthesize delegate;
@synthesize poemArray;
@synthesize locationName;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.titleLabel.text = self.locationName;
}

- (IBAction)onClosePopupBtn:(id)sender{
    if (self.delegate) {
        [self.delegate onCloseButton];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PoemInfo *item = [self.poemArray objectAtIndex:indexPath.row];
    if (! [item hasSubLanguage]) {
        return  60.0;
    }
    
    return 80.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.poemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"LocationPoemTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    PoemInfo *item = [self.poemArray objectAtIndex:indexPath.row];
    
    UILabel *authorname = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *titlename = (UILabel *)[cell.contentView viewWithTag:3];
    titlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    UILabel *subtitlename = (UILabel *)[cell.contentView viewWithTag:4];
    subtitlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    
    titlename.text = item.title;
    authorname.text = item.author;
    if ([item hasSubLanguage]) {
        subtitlename.text = item.subLanguagePoem.title;
    }else{
        subtitlename.text = @"";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"selected posted item index %d", indexPath.row);
    if (self.delegate) {
        [self.delegate onSelectItemAtIndex:indexPath.row];
    }
}


@end
