//
//  DistricMapViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
@class MKMapView;

@interface DistricMapViewCtrl : BaseViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *poemZoneTitle;
@property (weak, nonatomic) IBOutlet UILabel *zoneNameLabel;


@property (nonatomic,strong) NSString* districtID;
- (IBAction)onMyLocationBtn:(id)sender;
@end
