//
//  DetailViewController.m
//  RaoVatApp
//
//  Created by hmchau on 6/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "DetailViewController.h"
#import "SBJSON.h"
#import "NewsData.h"
#import "LandmarkManager.h"
#import "WEPopoverController.h"
#import "LandmarkInfo.h"
#import "UIPhotoGalleryView.h"
#import "BIOViewController.h"
#import "AppUtils.h"
#import "RatingViewController.h"

#import "LoginViewCtrl.h"

#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AVFoundation/AVFoundation.h"

#import "FbSocialNetwork.h"
#import "TWSocialNetwork.h"
#import "IGSocialNetwork.h"
#import "SDImageCache.h"

#import <MessageUI/MessageUI.h>

@interface DetailViewController() <UIPhotoGalleryDataSource, UIPhotoGalleryDelegate, AVAudioPlayerDelegate, UIScrollViewDelegate, UITextViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate>

@end

@implementation DetailViewController  {
    WEPopoverController *_popoverController;
    UIPhotoGalleryView *vPhotoGallery;
    NSArray *_poemPhotos;
    
    MPMoviePlayerController *_audioPlayer;
    NSTimer * _timer;
    BOOL isSilderTouched;
    
    LoginViewCtrl*loginVC;
    
    
    IGSocialNetwork *_instagramSocial;
}

@synthesize poemInfo;

#define CONTENT_STANDARD_FONT_SIZE 3.0

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)changeScrollBarColorFor:(UIScrollView *)scrollView
{
    for ( UIView *view in scrollView.subviews ) {
        
        if (view.tag == 0 && [view isKindOfClass:UIImageView.class])
        {
            //NSLog(@" --------- %f - %f - %f - %f", view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            
            UIImageView *imageView = (UIImageView *)view;
            imageView.image = [UIImage imageNamed:@"img_scroll_bar_bg"];
            imageView.backgroundColor = THEME_COLOR;
        }
    }
}

//-(void) updateContentPoemWithTitle:(NSString *) title content:(NSString *) content{
//    
//    UIFont *textviewFont = [UIFont fontWithName:@"Futura-Bold" size:15.0 * [AppConfiguration sharedInstance].fontSizePreview];
//    
//    NSDictionary *attributes = @{NSFontAttributeName: textviewFont };
//    CGRect stringrect = [title boundingRectWithSize:CGSizeMake(self.poemContentText.frame.size.width, CGFLOAT_MAX)
//                                            options:NSStringDrawingUsesLineFragmentOrigin
//                                         attributes:attributes
//                                            context:nil];
//    
//    float heighOfTitle = stringrect.size.height + 4;
//
//    UILabel *label = (UILabel *)[self.poemContentText viewWithTag:1003];
//    CGRect frame = CGRectMake(3, - heighOfTitle, self.poemContentText.frame.size.width, heighOfTitle);
//    if (!label) {
//        label = [[UILabel alloc] initWithFrame:frame];
//        
//        label.numberOfLines = 0;
//        label.textColor = [UIColor blackColor];
//        label.tag = 1003;
//
//        [self.poemContentText addSubview:label];
//
//    }else{
//        label.frame = frame;
//    }
//    
//    label.font = textviewFont;
//    label.text = title;
//    
//    self.poemContentText.text = content;
//    self.poemContentText.contentInset = UIEdgeInsetsMake(heighOfTitle, 0, 0, 0);// top, left, bottom, right
//    self.poemContentText.contentOffset = CGPointMake(0, -heighOfTitle);
//}


-(void) updateContentPoemWithTitle:(NSString *) title content:(NSString *) content{
    
    float fontsize = CONTENT_STANDARD_FONT_SIZE * [AppConfiguration sharedInstance].fontSizePreview;
    
    NSString *tilehtml = [NSString stringWithFormat:@"<html><body><font face='Futura' size='%f'><b>%@</b></font><p/>",fontsize,title];
    
    NSString *htmlString= [NSString stringWithFormat:@"<font face='Futura' size='%f'>%@</font><meta name='viewport' content='width=device-width'/>",fontsize, content];

    NSString *endHtml=@"</body></html>";
    
    [self.poemWebContent loadHTMLString:[NSString stringWithFormat:@"%@%@%@", tilehtml,htmlString,endHtml] baseURL:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLockPoemStatus) name:NOTIFICATION_LOCAION_UNLOCK object:nil];
    
    
    //set font
    self.poemTitleLabel.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    self.poemTitleLabelMainLanguage.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    self.mainLanguageBtn.titleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
    self.subLangugeBtn.titleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
    
    self.captionImageButton.titleLabel.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    [self.captionImageButton setTitle:@"" forState:UIControlStateNormal];
    
    self.poemWebContent.scrollView.delegate = self;
    self.poemWebContent.scrollView.showsHorizontalScrollIndicator = NO;
    self.poemWebContent.delegate = self;
    self.poemWebContent.scalesPageToFit=YES;
    
    if (self.poemInfo) {
        [APP_DELEGATE GALTrackerInCategory:@"Poem" withAction:@"Open" withLabel:self.poemInfo.poemid];
        
        self.title = @"PUBLIC";
        if ([self isFeaturedPoem]) {
            self.title = @"FEATURE";
            
            //Add to archival location if need
            [[LandmarkManager shareInstance] addArchivalLocationIfNeed:[NSString stringWithFormat:@"%d", self.poemInfo.locationId] ];
        }else{
            self.poemInfo.urlshare = URL_DOWNLOAD_APP;
        }
        
        self.authorLabel.text = self.poemInfo.author;
        self.poemTitleLabel.text = self.poemInfo.title;
        
        [self updateContentPoemWithTitle:self.poemInfo.title content:self.poemInfo.content];
        
        //self.poemContentText.text = self.poemInfo.content;
        _poemPhotos = self.poemInfo.photos;
        
        
        if (self.poemInfo.liked) {
            [self.likeButton setImage:[UIImage imageNamed:@"img_liked_icon.png"] forState:UIControlStateNormal];
        }else{
            [self.likeButton setImage:[UIImage imageNamed:@"img_like_icon.png"] forState:UIControlStateNormal];
        }
        
        
        if (self.poemInfo.average_rating != 0.0) {
            self.rateButton.selected = YES;
        }
        
        if (self.poemInfo.reported) {
            self.reportButton.selected = YES;
        }
        
        
        //setup language button
        if ([self.poemInfo hasSubLanguage]) {
            
            self.poemTitleLabelMainLanguage.text = self.poemInfo.title;
            self.poemTitleLabel.text = self.poemInfo.subLanguagePoem.title;
            
            NSString *title = [AppUtils languageNameFromLanguageId:self.poemInfo.languageId ];
            [self.mainLanguageBtn setTitle:title forState:UIControlStateNormal];
            
            //adjust sub language button
            NSDictionary *attributes = @{NSFontAttributeName: self.mainLanguageBtn.titleLabel.font };
            CGRect stringrect = [title boundingRectWithSize:CGSizeMake(320.0, CGFLOAT_MAX)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:attributes
                                                      context:nil];
            
            int mainbuttonwidth = stringrect.size.width + 5;
            CGRect frame = self.subLangugeBtn.frame;
            frame.origin.x = self.mainLanguageBtn.frame.origin.x +  mainbuttonwidth + 5;
            self.subLangugeBtn.frame = frame;
            
            frame = self.separateSubLanguageBtn.frame;
            frame.origin.x = self.mainLanguageBtn.frame.origin.x + mainbuttonwidth;
            self.separateSubLanguageBtn.frame = frame;

            
        }else{
            [self.mainLanguageBtn setTitle:@"Eng" forState:UIControlStateNormal];
            self.poemTitleLabelMainLanguage.text = @"";
            

            self.subLangugeBtn.hidden = YES;
            self.separateSubLanguageBtn.hidden = YES;
            
            
            //change title position
            self.poemTitleLabel.frame = self.poemTitleLabelMainLanguage.frame;
            CGRect frame = self.poemTitleLabel.frame;
            frame.origin.y += 10;
            self.poemTitleLabel.frame = frame;
            
            frame = self.authorLabel.frame;
            frame.origin.y += 10;
            self.authorLabel.frame = frame;
            
        }
        
        //setup audio link
        if (self.poemInfo.audiolink == nil || [self.poemInfo.audiolink isEqualToString:@""]) {
            self.playBtn.hidden = YES;
            self.playBtn.userInteractionEnabled = NO;

            float width = self.playBtn.frame.size.width + 5;
            UIView *langugagebtnview = [self.view viewWithTag:10022];
            CGRect frame = langugagebtnview.frame;
            frame.origin.x -= width;
            langugagebtnview.frame = frame;

        }
        
    }else{
        
        _poemPhotos = [[NSArray alloc] init];
    }

    [self setupImageScroller];
    
    //for audio player
    [self setupProgressbarControl];
    
    
    //setup view detail for featured poem
    if ( [self isFeaturedPoem]) {
        //self.reportButton.hidden = YES;
        //self.rateButton.hidden = YES;
        
        if (! self.poemInfo.poetInfo) {
            self.bioButton.enabled = NO;
        }
    }else{
        self.bioButton.hidden = YES;
    }
    

    if (self.isReadOnly) {
        [self.view viewWithTag:3493].hidden = YES;
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if (_audioPlayer) {
        [_audioPlayer stop];
    }
    
    [self moviePlaybackComplete:nil];
    
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];

}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    self.poemContentText.font = [UIFont fontWithName:@"Futura" size:15.0 * [AppConfiguration sharedInstance].fontSizePreview];
//    
//    if (self.poemInfo) {
//        NSString *title = @"";
//        NSString *content = @"";
//        UILabel *label = (UILabel *)[self.poemContentText viewWithTag:1003];
//        
//        if (label) {
//            title = label.text;
//        }
//        content = self.poemContentText.text;
//        
//        [self updateContentPoemWithTitle:title content:content];
//    }
    
    if (self.poemInfo) {
        [self updateContentPoemWithTitle:self.poemInfo.title content:self.poemInfo.content];
    }
    
    //Change button following login status
    BOOL logined = [AppConfiguration isLogin];
    
    self.likeButton.hidden = !logined;
    if (self.likeButton.hidden) {
        self.shareButton.center = self.likeButton.center;
    }else{
        self.shareButton.center = CGPointMake(115, self.shareButton.center.y);
    }
    
    
    if ( [self isFeaturedPoem]) {
        self.reportButton.hidden = YES;
        self.rateButton.hidden = YES;

    }else{
        self.reportButton.hidden = !logined;
        self.rateButton.hidden = !logined;
    }
}




-(void) setupImageScroller{
    self.captionImageButton.hidden = YES;
    if (self.poemInfo.photoCaptions && self.poemInfo.photoCaptions.count > 0 ) {
        NSString *caption = self.poemInfo.photoCaptions[0];
        if (caption != nil && caption.length > 0) {
            [self.captionImageButton setTitle:caption forState:UIControlStateNormal];

        }
    }
    
    if (_poemPhotos == nil || _poemPhotos.count < 2) {
        self.iconLeftSwipe.hidden = YES;
        self.iconRightSwipe.hidden = YES;
    }
    
    UIView *view = self.galleryContainer;
    vPhotoGallery = [[UIPhotoGalleryView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    vPhotoGallery.dataSource =self;
    vPhotoGallery.delegate = self;
    vPhotoGallery.galleryMode = UIPhotoGalleryModeImageRemote;
    vPhotoGallery.showsScrollIndicator = YES;
    vPhotoGallery.verticalGallery = vPhotoGallery.peakSubView = NO;
    [view addSubview: vPhotoGallery];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTabGesture:)];
    [view addGestureRecognizer:tapGestureRecognizer];
    
    [self updateLockPoemStatus];
    
}

-(void) updateLockPoemStatus{
    
    if (! [self isFeaturedPoem]) {
        //public poem
        self.imageLockIndicator.hidden = YES;
        
        return;
    }
    
    if ([[LandmarkManager shareInstance] checkUnlockedLocationId: [NSString stringWithFormat:@"%d", self.poemInfo.locationId] ])
    {
        //location unlocked
        vPhotoGallery.userInteractionEnabled = YES;
        
    }else{
        //not come here any more
        
        LandmarkInfo *landmark = [[LandmarkManager shareInstance] getLocationDetailById:self.poemInfo.locationId];
        if (landmark == nil) {
            NSLog(@"ERROR There is not location info");
            self.imageLockIndicator.hidden = YES;

            return;
        }

        if (landmark.unlock_required == NO) {
            //Feature poem not need unlocked

            vPhotoGallery.userInteractionEnabled = YES;
        
        }else{
        
            //Feature poem not yet unlocked
            vPhotoGallery.userInteractionEnabled = NO;
        
            double distance = [AppUtils distanceBetweenCoordinate:APP_DELEGATE.currentUserLocation toOther:landmark.location];
            
            NSString *msg;
            if (distance > 1000) {
                distance = distance/1000;
                if (distance < 10000) {
                    msg = [NSString stringWithFormat:@"%.1fkm from unlock zone", distance];
                }else{
                    msg = @"Too far";
                }
            }else{
                msg = [NSString stringWithFormat:@"%.0fm from unlock zone", distance];

            }
            
        }
        
    }
}

-(void) handleTabGesture:(UITapGestureRecognizer*) recognizer{
    //NSLog(@"handleTabGesture");
    
    CGPoint location = [recognizer locationInView:recognizer.view ];
    //NSLog(@"testing  %@", NSStringFromCGPoint(location));
    
    if (CGRectContainsPoint(self.imageLockIndicator.frame, location)) {
        [self openLocationInfo];
    }else if(CGRectContainsPoint(self.iconLeftSwipe.frame, location)) {
        
        LandmarkInfo *item = [[LandmarkManager shareInstance] getLocationDetailById:self.poemInfo.locationId];
        
        if ( ( item.unlock_required == NO || [[LandmarkManager shareInstance] checkUnlockedLocationId: [NSString stringWithFormat:@"%d", self.poemInfo.locationId] ]) )
        {
            //Can be viewable

            if (self.iconLeftSwipe.hidden == NO) {
                if (_poemPhotos != nil && vPhotoGallery != nil && vPhotoGallery.currentIndex > 0) {
                    [vPhotoGallery scrollToPage:vPhotoGallery.currentIndex - 1 animated:YES];
                }
            }
        }
        
    }else if(CGRectContainsPoint(self.iconRightSwipe.frame, location)) {
        
        LandmarkInfo *item = [[LandmarkManager shareInstance] getLocationDetailById:self.poemInfo.locationId];
        
        if ( ( item.unlock_required == NO || [[LandmarkManager shareInstance] checkUnlockedLocationId: [NSString stringWithFormat:@"%d", self.poemInfo.locationId] ]) )
        {
            //Can be viewable
            
            if (self.iconRightSwipe.hidden == NO) {
                if (_poemPhotos != nil && vPhotoGallery != nil && vPhotoGallery.currentIndex < _poemPhotos.count) {
                    [vPhotoGallery scrollToPage:vPhotoGallery.currentIndex + 1 animated:YES];
                }
            }
        }
    }
}

-(void) initTableData{
    
}

-(BOOL) isFeaturedPoem{
    return self.poemInfo.isFeaturedPoem;
}

- (IBAction)onAudioBtn:(UIButton *)sender {

    sender.tag = 10023;
    
    if (self.poemInfo.audiolink != nil) {
        
        sender.selected = !sender.selected;
        
        UIView *view = [self.view viewWithTag:10022];
        if (sender.selected) {
            if (view) {
                CGRect frame = view.frame;
                
                if (frame.origin.x < 50) {
                    frame.origin.x += 140;
                    [UIView animateWithDuration:0.2 animations:^{
                        view.frame = frame;
                    }];
                }
            }
        }else{
            CGRect frame = view.frame;
            frame.origin.x = 42;
            view.frame = frame;

        }
        
        if (_audioPlayer == nil) {
            [self loadAudioFromLink:self.poemInfo.audiolink];
            
//            Class playingInfoCenter = NSClassFromString(@"MPNowPlayingInfoCenter");
//            if (playingInfoCenter) {
//                NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
//                [songInfo setObject:self.poemInfo.title forKey:MPMediaItemPropertyTitle];
//                [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
//            }
        }else{
            if (sender.selected) {
                [_audioPlayer play];
                
//                Class playingInfoCenter = NSClassFromString(@"MPNowPlayingInfoCenter");
//                if (playingInfoCenter) {
//                    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
//                    [songInfo setObject:self.poemInfo.title forKey:MPMediaItemPropertyTitle];
//                    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
//                }
            }else{
                [_audioPlayer pause];
            }
        }
        
    }else{
        [self showAlert:@"There is not audio link."];
    }
}

- (IBAction)onBioBtn:(UIButton * )sender {
    
    if (self.poemInfo == nil || self.poemInfo.poetInfo == nil) {
        return;
    }
    
    UIStoryboard *storyboard = self.storyboard;
    BIOViewController *biodetail = [storyboard instantiateViewControllerWithIdentifier:@"BIOViewController"];
    biodetail.parentController = self;
    biodetail.poetInfo = self.poemInfo.poetInfo;
    
    biodetail.contentSizeForViewInPopover = CGSizeMake(300 , 440 );
    
    if (!_popoverController) {
        _popoverController = [[WEPopoverController alloc] initWithContentViewController:biodetail];
    }
    else{
        _popoverController.contentViewController = biodetail;
    }
        
    CGPoint point = self.view.center;
    CGRect rect = CGRectMake(point.x, point.y - 440/2 + 4 , 2, 2);
    
    [_popoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)onRatingViewBtn:(UIButton *)sender {
    if(APP_DELEGATE.isLogined){
        UIStoryboard *storyboard = self.storyboard;
        RatingViewController *ratingview = [storyboard instantiateViewControllerWithIdentifier:@"RatingViewController"];
        
        ratingview.parentController = self;
        ratingview.currentRate = self.poemInfo.average_rating;
        ratingview.contentSizeForViewInPopover = CGSizeMake(280 , 42 );
        
        if (!_popoverController) {
            _popoverController = [[WEPopoverController alloc] initWithContentViewController:ratingview];
        }
        else{
            _popoverController.contentViewController = ratingview;
        }
        
        [_popoverController presentPopoverFromRect:sender.frame inView:sender permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else{
        //show login view
        [self showLoginView];
    }
}


-(void) doRatingWithPoint:(NSNumber *) point{
    
    if (self.poemInfo && point) {
        
        if (_popoverController) {
            [_popoverController dismissPopoverAnimated:YES];
        }
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setValue:[NSString stringWithFormat:@"%d", [point intValue]] forKeyPath:@"grade"];
        
        NSString *url = [NSString stringWithFormat:SERVICE_RATE_POEM, self.poemInfo.poemid ];
        [self sendHttpPostRequest:url body:dic andRequestId:2];
    }
}

- (IBAction)onReportBtn:(id)sender {
    
    if(APP_DELEGATE.isLogined){
        
        if (self.poemInfo != nil) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Why do you report this poem?" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
            alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            [alertView show];
        }
    }
    else{
        //show login view
        [self showLoginView];
    }

}

- (IBAction)onShareBtn:(id)sender {
    if (! self.poemInfo) {
        return;
    }
    
    UIActionSheet *actionsheet = [[UIActionSheet alloc] initWithTitle:@"Share with..." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
    [actionsheet showInView:self.view];
    
    [APP_DELEGATE GALTrackerInCategory:@"Poem" withAction:@"Share" withLabel:self.poemInfo.poemid];
}
-(void)showLoginView{
    UINavigationController* navi = [[UINavigationController alloc] init];
    navi.title = @"LOGIN";
    loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN_VIEW_CTRL"];
    [navi setViewControllers:@[loginVC] animated:NO];
    
    [self presentViewController:navi animated:YES completion:nil];
}
-(void) dismissLoginScreen:(id)object{
    if(loginVC != nil){
        [loginVC dismissViewControllerAnimated:YES completion:^{
            loginVC = nil;
        }];
    }
}

-(void) dismissPopoverViewController{
    if (_popoverController) {
        [_popoverController dismissPopoverAnimated:YES];
    }
}

- (void) openLocationInfo {
    
    LandmarkInfo *item = [[LandmarkManager shareInstance] getLocationDetailById:self.poemInfo.locationId];
    
    if (item == nil) {
        NSLog(@"ERROR not location info");

        return;
    }
    
    if ([self isFeaturedPoem] && ( item.unlock_required == NO || [[LandmarkManager shareInstance] checkUnlockedLocationId: [NSString stringWithFormat:@"%d", self.poemInfo.locationId] ]) )
    {
        [APP_DELEGATE showLocationInfoView:item];
    
    }else{
        
        NSLog(@"Is not featured poem. Or not yet unlock");
    }
}

- (IBAction)onLikeBtn:(id)sender{
    if(APP_DELEGATE.isLogined){
        if (self.poemInfo) {
            [APP_DELEGATE GALTrackerInCategory:@"Poem" withAction:@"Like" withLabel:self.poemInfo.poemid];
            
            NSString *url = [NSString stringWithFormat:SERVICE_LIKE_POEM, self.poemInfo.poemid ];
            [self sendHttpPostRequest:url body:nil andRequestId:1];
        }
    }
    else{
        [self showLoginView];
    }
 
}

- (IBAction)onCloseCaptainBtn:(UIButton *)sender {
    //sender.superview.hidden = YES;
    sender.selected = !sender.selected;
    
    self.poemTitleLabel.superview.hidden = sender.selected;

    NSString *caption = [self.captionImageButton titleForState:UIControlStateNormal];
    if (caption.length > 0) {
        self.captionImageButton.hidden = !sender.selected;
    }else{
        self.captionImageButton.hidden = YES;
    }
}


- (IBAction)onSubLanguageBtn:(id)sender {
    
    if(self.poemInfo.subLanguagePoem == nil)
        return;
    
    //self.poemContentText.text = self.poemInfo.subLanguagePoem.content;
    NSString *subcontent = self.poemInfo.subLanguagePoem.content;
    
    if (subcontent == nil || [subcontent isEqualToString:@""]) {
        subcontent = @"No English translation for this poem";
    }
    
    [self updateContentPoemWithTitle:self.poemInfo.subLanguagePoem.title content:subcontent];

    self.subLangugeBtn.selected = YES;
    self.mainLanguageBtn.selected = NO;
}

- (IBAction)onMainLangugeBtn:(id)sender {
    
    if(self.poemInfo == nil)
        return;
    
    if (! [self.poemInfo hasSubLanguage]) {
        //has only one language
        return;
    }
    
    
    //self.poemContentText.text = self.poemInfo.content;
    [self updateContentPoemWithTitle:self.poemInfo.title content:self.self.poemInfo.content];
    
    self.subLangugeBtn.selected = NO;
    self.mainLanguageBtn.selected = YES;
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self changeScrollBarColorFor:scrollView];
}


#pragma mark - UIPhotoGalleryDataSource methods

- (NSInteger)numberOfViewsInPhotoGallery:(UIPhotoGalleryView *)photoGallery {
    return _poemPhotos.count;
}

- (NSURL*)photoGallery:(UIPhotoGalleryView *)photoGallery remoteImageURLAtIndex:(NSInteger)index {
    return _poemPhotos[index];
}

- (UIView*)photoGallery:(UIPhotoGalleryView *)photoGallery customViewAtIndex:(NSInteger)index {
    CGRect frame = CGRectMake(0, 0, photoGallery.frame.size.width, photoGallery.frame.size.height);
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [NSString stringWithFormat:@"%d", index+1];
    [view addSubview:label];
    
    return view;
}


- (UIView*)customTopViewForGalleryViewController:(UIPhotoGalleryViewController *)galleryViewController {
    return nil;
}

#pragma mark - UIPhotoGalleryDelegate methods
- (void)photoGallery:(UIPhotoGalleryView *)photoGallery didTapAtIndex:(NSInteger)index {
}

- (void)photoGallery:(UIPhotoGalleryView *)photoGallery didMoveToIndex:(NSInteger)index{
    
    if (self.poemInfo.photoCaptions && index < self.poemInfo.photoCaptions.count) {
        
    }else{
        self.captionImageButton.hidden = YES;
        return;
    }
    
    
    NSString *caption = self.poemInfo.photoCaptions[index];
    [self.captionImageButton setTitle:caption forState:UIControlStateNormal];
    
    if ( self.poemTitleLabel.superview.hidden == YES) {
        
        if (caption != nil && caption.length > 0) {
            self.captionImageButton.hidden = NO;
        }else{
            self.captionImageButton.hidden = YES;
        }
    }
}


#pragma mark - Audio player methods

-(void) setupProgressbarControl{
    
    [self.progressSlider setThumbImage:[UIImage new] forState:UIControlStateNormal];
    [self.progressSlider setMinimumTrackImage:[[UIImage imageNamed:@"video_slider_colored.png"] stretchableImageWithLeftCapWidth:0.3 topCapHeight:0.0] forState:UIControlStateNormal];
    [self.progressSlider setMaximumTrackImage:[[UIImage imageNamed:@"video_slider_gray.png"] stretchableImageWithLeftCapWidth:0.3 topCapHeight:0.0] forState:UIControlStateNormal];
    
    self.progressSlider.userInteractionEnabled = NO;
    
    //self.progressSlider.enabled = NO;
}


- (void)sliderMoveStart{
    
    isSilderTouched = YES;
    
}

-(void)sliderMoving{
    //    lastControlTime = CFAbsoluteTimeGetCurrent();
    //    isSilderTouched = YES;
    //    double time = self.moviePlayer.duration * playerSlider.value;
    //    currentTimeLabel.text = [self secondsToMMSS:time]; //
}

-(void)sliderMoveEnd
{
    
    float rate = self.progressSlider.value;
    NSLog(@"slider changed value %f", rate);
    
    _audioPlayer.currentPlaybackTime = _audioPlayer.duration * rate; // seek movie
    isSilderTouched = NO;
}



-(void) loadAudioFromLink:(NSString *) url{
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    

    NSURL *theURL = [NSURL URLWithString:url];
    
    _audioPlayer = [[MPMoviePlayerController alloc] init];
    [_audioPlayer.view setFrame:CGRectInfinite];
    [_audioPlayer prepareToPlay];
    [_audioPlayer setShouldAutoplay:YES];
    [_audioPlayer setScalingMode:MPMovieScalingModeNone];
    _audioPlayer.view.hidden = YES;
    
    _audioPlayer.controlStyle = MPMovieControlStyleNone;
    [self.view insertSubview:_audioPlayer.view atIndex:0];
    
    _audioPlayer.movieSourceType = MPMovieSourceTypeStreaming;
    [_audioPlayer setContentURL:theURL];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerDurationAvailable:) name:MPMovieDurationAvailableNotification object:_audioPlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(streamStateChanged:) name:MPMoviePlayerLoadStateDidChangeNotification object:_audioPlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:_audioPlayer];
    
}

-(void) streamStateChanged:(NSNotification *)notification{
    MPMoviePlayerController *videoplayer = notification.object;
    NSLog(@"streamStateChanged %d", [videoplayer loadState]);

    if ([videoplayer loadState] == MPMovieLoadStateUnknown || [videoplayer loadState] == 5) {
        UIButton *button = (UIButton *) [self.view viewWithTag:10023];
        if (button) {
            button.selected = NO;
        }
    }
}

-(void) moviePlaybackComplete:(NSNotification *)notification{
    NSLog(@"moviePlaybackComplete");
    
    UIButton *button = (UIButton *) [self.view viewWithTag:10023];
    if (button) {
        button.selected = NO;
    }
    
    if (_audioPlayer) {
        [_audioPlayer stop];
        
        UIView *view = [self.view viewWithTag:10022];
        CGRect frame = view.frame;
        frame.origin.x = 42;
        view.frame = frame;
    }else{
    
        if (self.poemInfo.audiolink == nil) {
            UIView *view = [self.view viewWithTag:10022];
            CGRect frame = view.frame;
            frame.origin.x = 42;
            view.frame = frame;
        }
    }
    
    //[self stopTickTimer];
}

-(void) moviePlayerDurationAvailable:(NSNotification *)notification{
    
    [_audioPlayer play];
    
    //Progress update
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(intervalUpdate) userInfo:nil repeats:YES];
}

-(void) stopTickTimer{
    if(_timer)
    {
        [_timer invalidate];
        _timer = nil;
    }
}


-(void) intervalUpdate{
    
    if(!isSilderTouched){
        float value = _audioPlayer.currentPlaybackTime/_audioPlayer.duration;
        [self.progressSlider setValue: value animated:YES];
    }
}

#pragma mark - Audio player remote control handling


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    //if it is a remote control event handle it correctly
    if (event.type == UIEventTypeRemoteControl)
    {
        if (event.subtype == UIEventSubtypeRemoteControlPlay)
        {
            UIButton *button = (UIButton *) [self.view viewWithTag:10023];
            [self onAudioBtn:button];
        }
        else if (event.subtype == UIEventSubtypeRemoteControlPause)
        {
            UIButton *button = (UIButton *) [self.view viewWithTag:10023];
            [self onAudioBtn:button];
        }
    }
}


#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != alertView.cancelButtonIndex) {
        UITextField * alertTextField = [alertView textFieldAtIndex:0];

        NSString *reason = alertTextField.text;
        if (reason == nil || reason.length == 0) {
            [self showAlert:@"Please let us know why you report this poem"];
            return;
        }
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:reason forKey:@"reason"];
        
        NSString *url = [NSString stringWithFormat:SERVICE_REPORT_POEM, self.poemInfo.poemid ];
        [self sendHttpPostRequest:url body:dic andRequestId:3];
        
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (! self.poemInfo) {
        return;
    }
    
    UIImage *sharedImage;
    if (self.poemInfo.photos && self.poemInfo.photos.count > 0){
        NSString *imageurl = self.poemInfo.photos[0];
        
        SDImageCache *imageCache = [SDImageCache sharedImageCache];
        sharedImage = [imageCache imageFromMemoryCacheForKey:imageurl];
    }
    
    switch (buttonIndex) {
        case 0:
        {
            FbSocialNetwork *fb = [[FbSocialNetwork alloc] init];
            fb.parentController = self;
            
            NSString *formatBody = @"Your friend has shared \"%@\" by %@ with you!\nTo read the poem, download the Text in the City app. Submit your own place-based poem and stand a chance to win up to $1,500 and be showcased in a featured collection!";
            
            NSString *body = [NSString stringWithFormat:formatBody, self.poemInfo.title, self.poemInfo.author];
            
            body = [body stringByAppendingFormat:@"\n\nApple Store\n%@", URL_DOWNLOAD_APP];
            
            body = [body stringByAppendingFormat:@"\n\nGoogle Playstore\n%@", URL_DOWNLOAD_APP_ANDROID];

            if ([fb shareContent:body image:sharedImage url:nil]){

            
            }else{
                
                [self showAlert:@"Facebook account is not available on your device. Please check it in the Settings"];
            }
        }
            
            break;
            
        case 1:
        {
            TWSocialNetwork *fb = [[TWSocialNetwork alloc] init];
            fb.parentController = self;
            NSString *title = self.poemInfo.title;
            
            NSString *formatBody = @"To read \"%@\" by %@, download the mobile app!";
            NSString *body = [NSString stringWithFormat:formatBody, title, self.poemInfo.author];
            
            NSString *footer = @"";
            footer = [footer stringByAppendingFormat:@"\n%@", URL_DOWNLOAD_APP];
            footer = [footer stringByAppendingFormat:@"\n%@", URL_DOWNLOAD_APP_ANDROID];
            //Footer size 48 character
            //Remain still 92 char
            
            if (body.length > 92) {
                title = [title substringToIndex:title.length - 4];
                title = [title stringByAppendingString:@"..."];
                body = [NSString stringWithFormat:formatBody, title, self.poemInfo.author];
                while (body.length > 92) {
                    title = [title substringToIndex:title.length - 4];
                    title = [title stringByAppendingString:@"..."];
                    body = [NSString stringWithFormat:formatBody, title, self.poemInfo.author];
                }
            }
            
            if ([fb shareContent:[body stringByAppendingString:footer] image:nil url:nil]){
                 
            }else{
                
                [self showAlert:@"Twitter account is not available on your device. Please check it in the Settings"];
            }
            
        }
            break;
            
            
        case 2:
        {
            [self openEmailComposer:sharedImage];
        }
            
            break;
            
        default:
            
            //[self showOnprogressMsg];
            break;
    }
}

- (void)openEmailComposer:(UIImage *)image
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if( picker == nil ){
        //[self showAlert:@"You will need to setup a mail account on your device before you can send mail"];
        return;
    }
    
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"A Poem from Text In The City"];
    
    NSString *formatBody = @"Your friend has shared \"%@\" by %@ with you!\nTo read the poem, download the Text in the City app. Submit your own place-based poem and stand a chance to win up to $1,500 and be showcased in a featured collection!";
    
    NSString *body = [NSString stringWithFormat:formatBody, self.poemInfo.title, self.poemInfo.author];
    
    body = [body stringByAppendingFormat:@"\n\nApple Store\n%@", URL_DOWNLOAD_APP];
    
    body = [body stringByAppendingFormat:@"\n\nGoogle Playstore\n%@", URL_DOWNLOAD_APP_ANDROID];

    
    // This is not an HTML formatted email
    [picker setMessageBody:body isHTML:NO];
    
    if (image) {
        // Create NSData object as PNG image data from camera image
        NSData *data = UIImagePNGRepresentation(image);
        
        // Attach image data to the email
        // 'CameraImage.png' is the file name that will be attached to the email
        [picker addAttachmentData:data mimeType:@"image/png" fileName:@"PoemImage"];
    }

    // Show email view
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    NSLog(@"didFinishWithResult %@", error);
    [controller dismissViewControllerAnimated:YES completion:^{
        
    }];
}


#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    if (data != nil) {
        
        switch (requestId) {
            case 1:
                //Like request
            {
                NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                SBJSON *jsonParser = [[SBJSON alloc] init];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSDictionary *dic = [jsonParser objectWithString:jsonString];
                
                if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
                {
                    id errordic = [dic valueForKey:@"error"];
                    if (errordic != nil) {
                        [self showAlert:@"Can not add this poem to your favourite right now, please try again later"];
                    }else{
                        NSNumber *liked = [dic objectForKey:@"is_liked"];
                        if (liked != nil) {
                            self.poemInfo.liked = [liked boolValue];
                        }
                        
                        
                        if (self.poemInfo.liked) {
                            [self.likeButton setImage:[UIImage imageNamed:@"img_liked_icon.png"] forState:UIControlStateNormal];
                        }else{
                            [self.likeButton setImage:[UIImage imageNamed:@"img_like_icon.png"] forState:UIControlStateNormal];
                        }
                        
                        
                    }
                }else{
                    [self showAlert:@"There is something wrong"];
                }
            }
                break;
                
            case 2:
                //Rating request
            {
                NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                SBJSON *jsonParser = [[SBJSON alloc] init];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSDictionary *dic = [jsonParser objectWithString:jsonString];
                
                if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
                {
                    id errordic = [dic valueForKey:@"error"];
                    if (errordic != nil) {
                        [self showAlert:@"Can not rating at this time"];
                    }else{
                        self.rateButton.selected = YES;
                        
                        NSNumber *avg_rate = [dic objectForKey:@"average_rating"];
                        if (avg_rate != nil) {
                            self.poemInfo.average_rating = [avg_rate floatValue];
                        }
                        
                        //if (_popoverController) {
                        //    [_popoverController dismissPopoverAnimated:YES];
                        //}
                    }
                }else{
                    [self showAlert:@"There is something wrong"];
                }
            }
                break;
                
                
            case 3:
                //Report request
            {
                NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                SBJSON *jsonParser = [[SBJSON alloc] init];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSDictionary *dic = [jsonParser objectWithString:jsonString];
                
                if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
                {
                    NSString *errordic = [dic valueForKey:@"error"];
                    if (errordic != nil) {
                        if ([errordic isKindOfClass:[NSString class]]) {
                            [self showAlert:errordic];
                        }else{
                            [self showAlert:@"Can not make report at this time"];
                        }
                    }else{
                        //success
                        self.reportButton.selected = YES;
                        self.poemInfo.reported = YES;
                        
                        [self showAlert:@"This poem has been reported. Thank you"];
                    }
                }else{
                    [self showAlert:@"There is something wrong"];
                }
            }
                break;
                
            default:
                break;
        }
        
        
    }else{
        [self showNetworkErrMsg];
    }
    
}

- (IBAction)likeButton:(id)sender {
}


#pragma mark - UIWebviewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{

    NSString *url = [request.URL absoluteString];
    NSLog(@"open URL %@", url);

    if (url.length == 0 || [url hasPrefix:@"about:"]) {
        return  YES;
    }
    
    [[UIApplication sharedApplication] openURL:request.URL];

    return NO;
}



@end
