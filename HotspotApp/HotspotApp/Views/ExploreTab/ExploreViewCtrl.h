//
//  ExploreViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface ExploreViewCtrl : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)onZoneButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnDist1;
@property (weak, nonatomic) IBOutlet UIButton *btnDist2;
@end
