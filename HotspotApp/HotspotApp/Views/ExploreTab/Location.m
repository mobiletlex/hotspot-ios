//
//  ExploreViewCtrl.m
//  HotspotApp
//
//  Created by hmchau on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//
#import "Location.h"

@implementation Location

@synthesize name = _name;
@synthesize address = _address;
@synthesize coordinate = _coordinate;
@synthesize unlocked;
@synthesize noneedunlock;

@synthesize pinName;

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        _name = name ;
        _address = address ;
        _coordinate = coordinate;
    }
    return self;
}

- (NSString *)title {
    return _name;
}

- (NSString *)subtitle {
    return _address;
}


@end