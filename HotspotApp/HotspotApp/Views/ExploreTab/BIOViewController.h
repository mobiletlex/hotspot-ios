//
//  BIOViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
#import "PoetIInfo.h"

@interface BIOViewController : BaseViewController
- (IBAction)onCloseBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *poetImage;
@property (weak, nonatomic) IBOutlet UILabel *poetName;
@property (weak, nonatomic) IBOutlet UITextView *poetContent;


@property (nonatomic,weak) UIViewController *parentController;
@property (nonatomic,retain) PoetIInfo *poetInfo;

@end
