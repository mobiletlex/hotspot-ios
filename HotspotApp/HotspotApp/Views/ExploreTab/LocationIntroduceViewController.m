//
//  LocationIntroduceViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LocationIntroduceViewController.h"
#import "WEPopoverController.h"
#import "UIPhotoGalleryView.h"

@interface LocationIntroduceViewController () <UIPhotoGalleryDataSource, UIPhotoGalleryDelegate>

@end

@implementation LocationIntroduceViewController{
    
    NSArray *_photoURL;
    UIPhotoGalleryView *vPhotoGallery;

}

@synthesize locationInfo;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.headerTitleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
    self.locationNameLabel.font = [UIFont fontWithName:@"Futura-Bold" size:15];
    
    
    self.view.layer.borderWidth = 1.0;
    self.view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    if (self.locationInfo != nil) {
        self.locationNameLabel.text = self.locationInfo.name;
        self.funfactTextview.text = self.locationInfo.funfact;
        
        _photoURL = self.locationInfo.photos;

        if (_photoURL == nil) {
            _photoURL = @[];
        }
    }
    
    
    [self setupImageScroller];
}

-(void) setupImageScroller{
    
    UIView *view = [self.view viewWithTag:1001];
    vPhotoGallery = [[UIPhotoGalleryView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    vPhotoGallery.dataSource =self;
    vPhotoGallery.delegate = self;
    vPhotoGallery.galleryMode = UIPhotoGalleryModeImageRemote;
    vPhotoGallery.showsScrollIndicator = YES;
    vPhotoGallery.verticalGallery = vPhotoGallery.peakSubView = NO;
    
    [view addSubview: vPhotoGallery];
    
    self.slicePageControl.numberOfPages = _photoURL.count;
}

- (IBAction)onClosePopupBtn:(id)sender {
    NSLog(@"onClosePopupBtn");
    
    if (self.delegate) {
        [self.delegate performSelector:@selector(onCloseButton)];
    }
}


#pragma mark - UIPhotoGalleryDataSource methods

- (NSInteger)numberOfViewsInPhotoGallery:(UIPhotoGalleryView *)photoGallery {
    return _photoURL.count;
}

- (UIImage*)photoGallery:(UIPhotoGalleryView*)photoGallery localImageAtIndex:(NSInteger)index {
    return [UIImage imageNamed:[NSString stringWithFormat:@"sample%d.jpg", index % 10]];
}

- (NSURL*)photoGallery:(UIPhotoGalleryView *)photoGallery remoteImageURLAtIndex:(NSInteger)index {
    return _photoURL[index];
}

- (UIView*)photoGallery:(UIPhotoGalleryView *)photoGallery customViewAtIndex:(NSInteger)index {
    CGRect frame = CGRectMake(0, 0, photoGallery.frame.size.width, photoGallery.frame.size.height);
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [NSString stringWithFormat:@"%d", index+1];
    [view addSubview:label];
    
    return view;
}

- (UIView*)customTopViewForGalleryViewController:(UIPhotoGalleryViewController *)galleryViewController {
    return nil;
}

#pragma UIPhotoGalleryDelegate methods
- (void)photoGallery:(UIPhotoGalleryView *)photoGallery didTapAtIndex:(NSInteger)index {
}

- (void)photoGallery:(UIPhotoGalleryView *)photoGallery didMoveToIndex:(NSInteger)index{
    
    self.slicePageControl.currentPage = index;
}


@end
