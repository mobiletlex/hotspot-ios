//
//  LocationPoemViewController.h
//  HotspotApp
//
//  Created by hmchau on 10/31/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@protocol LocationPoemViewControllerDelegate <NSObject>

-(void) onCloseButton;
-(void) onSelectItemAtIndex:(int) index;


@end

@interface LocationPoemViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (retain,nonatomic) NSString* locationName;


@property (assign) id<LocationPoemViewControllerDelegate> delegate;
@property (retain,nonatomic) NSArray* poemArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)onClosePopupBtn:(id)sender;


@end
