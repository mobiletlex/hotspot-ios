//
//  ExploreViewCtrl.h
//  HotspotApp
//
//  Created by hmchau on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Location : NSObject <MKAnnotation> {
    NSString *_name;
    NSString *_address;
    CLLocationCoordinate2D _coordinate;

}

@property (strong) NSString *name;
@property (strong) NSString *address;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (assign) BOOL unlocked;
@property (assign) BOOL noneedunlock;

@property (nonatomic, retain) NSString *pinName;

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;

@end