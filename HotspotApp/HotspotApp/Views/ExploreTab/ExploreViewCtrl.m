//
//  ExploreViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ExploreViewCtrl.h"
#import "CommonUtils.h"
#import "PoemInfo.h"
#import "ExploreParser.h"
#import "LandmarkManager.h"
#import "DistricMapViewCtrl.h"
#import "DetailViewController.h"

@interface ExploreViewCtrl () <UITableViewDataSource>
{
    NSString* selectedDistrictID;
}

@end

@implementation ExploreViewCtrl{
    
    NSMutableArray *_randomPoemList;
    NSMutableArray *_selectedPoemList;
    
    int _selectIndex;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startLoadingExplore) name:NOTIFICATION_INIT_LOADING_DONE object:nil];
    
    _randomPoemList = [[NSMutableArray alloc] init];
    _selectedPoemList = [[NSMutableArray alloc] init];

    
//    NSTimer *_timer = [NSTimer scheduledTimerWithTimeInterval:5
//                                                       target:self
//                                                     selector:@selector(updateRandomPoem)
//                                                     userInfo:nil
//                                                      repeats:YES];
    
    //[[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void) startLoadingExplore{
    [self sendHttpGetRequest:SERVICE_LOAD_EXPLORER andRequestId:1];
}

-(void) updateRandomPoem{
    //NSLog(@"updateRandomPoem");
    
    if (_randomPoemList == nil || _randomPoemList.count == 0) {
        return;
    }
    
    [_selectedPoemList removeAllObjects];
    
    if (_randomPoemList.count < 3) {
        for (PoemInfo *item in _randomPoemList) {
            [_selectedPoemList addObject:item];
        }
    }else{
        int count = _randomPoemList.count;
        int randomnuber = [CommonUtils randomNumber:count];
        [_selectedPoemList addObject:[_randomPoemList objectAtIndex:randomnuber] ];

        int newrand = [CommonUtils randomNumber:count];
        while (newrand == randomnuber) {
            newrand = [CommonUtils randomNumber:count];
        }
        
        [_selectedPoemList addObject:[_randomPoemList objectAtIndex:newrand] ];
    }
    
    //[self.tableView reloadData];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PoemInfo *item = [_selectedPoemList objectAtIndex:indexPath.row];
    if ([item hasSubLanguage]) {
        return  100.0;
    }
    
    return 80.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _selectedPoemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ExplorerCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *centralname = (UILabel *)[cell.contentView viewWithTag:101];
    UILabel *authorname = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *titlename = (UILabel *)[cell.contentView viewWithTag:103];
    titlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    UILabel *subtitlename = (UILabel *)[cell.contentView viewWithTag:104];
    subtitlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];

    
    PoemInfo *item = [_selectedPoemList objectAtIndex:indexPath.row];
    titlename.text = item.title;
    authorname.text = item.author;
    centralname.text = [[LandmarkManager shareInstance] zoneNameById:item.zoneId];
    
    if ([item hasSubLanguage]) {
        subtitlename.text = item.subLanguagePoem.title;
    }else{
        subtitlename.text = @"";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected posted item index %d", indexPath.row);
    _selectIndex = indexPath.row;
    [self performSegueWithIdentifier:@"SHOW_DETAIL_SCREEN_SEQUE" sender:self];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Network callback
#define INTERNAL_REFRESH_LIST 10

-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    if (data != nil) {
        _randomPoemList = [ExploreParser parse:data];
        if (_randomPoemList) {
            
            [_selectedPoemList removeAllObjects];
            
            int count = 0;
            for (PoemInfo *item in _randomPoemList) {
                [_selectedPoemList addObject:item];
                
                count ++ ;
                if (count >= 2) {
                    break;
                }
            }
            
            [self.tableView reloadData];

            //hmchau remove 
//            [NSTimer scheduledTimerWithTimeInterval:INTERNAL_REFRESH_LIST
//                                               target:self
//                                             selector:@selector(updateRandomPoem)
//                                             userInfo:nil
//                                              repeats:YES];

            
        }else{
            [self showNetworkErrMsg];
        }
        
    }else{
        [self showNetworkErrMsg];
    }
    
}

- (IBAction)onZoneButton:(UIButton *)sender {
    
//    if(sender == _btnDist1){
//        selectedDistrictID = @"11";
//    }
//    else if(sender == _btnDist2){
//        selectedDistrictID = @"1";
//    }
    
    selectedDistrictID = [NSString stringWithFormat:@"%d", sender.tag] ;
    [self performSegueWithIdentifier:@"OPEN_MAP_VIEW_SEQUE" sender:self];
    
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"OPEN_MAP_VIEW_SEQUE"]) {
        DistricMapViewCtrl * vc = [segue destinationViewController];
        vc.districtID = selectedDistrictID;
    }else if ([segue.identifier isEqualToString:@"SHOW_DETAIL_SCREEN_SEQUE"]){
        DetailViewController *vc = segue.destinationViewController;
        vc.poemInfo = [_selectedPoemList objectAtIndex:_selectIndex];
    }
}

@end
