//
//  TutorialViewController.m
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController () <UIScrollViewDelegate>

@end

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mainScrollView.delegate = self;
    self.enterButton.hidden = YES;

    int screenHeight = 568;
    NSString *imageFormat = @"tutorial-0%d.jpg";
    if (IS_IPHONE_4) {
        imageFormat = @"tutorial_iPhone4-0%d.jpg";
        screenHeight = 480;
    }
    
    int xpos = 0;
    for (int i = 0; i < 8;  i++) {
        
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:imageFormat, i +1 ]]];
        img.frame = CGRectMake(xpos, 0, 320, screenHeight);
        [self.mainScrollView addSubview:img];
        
        img.contentMode = UIViewContentModeScaleAspectFit;
        xpos += 320;
    }
    
    self.mainScrollView.contentSize = CGSizeMake(320*8, 1);
    
    if(self.hideBackButton){
        self.backButton.hidden = YES;
    }
}




#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.mainScrollView.frame.size.width;
    
    float fractionalPage = self.mainScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    self.pageController.currentPage = page;
    self.enterButton.hidden = page != 7;
}


- (IBAction)onEnterBtn:(id)sender {
    
    [AppConfiguration sharedInstance].firstRunApp = NO;
    [[AppConfiguration sharedInstance] saveConfig];

    
    [self.view removeFromSuperview];
}

- (IBAction)onBackBtn:(id)sender {
    [self.view removeFromSuperview];

}

@end
