//
//  TutorialViewController.h
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface TutorialViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (assign) BOOL hideBackButton;

- (IBAction)onEnterBtn:(id)sender;
- (IBAction)onBackBtn:(id)sender;

@end
