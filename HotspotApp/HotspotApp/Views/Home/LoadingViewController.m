//
//  LoadingViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LoadingViewController.h"
#import "LandmarkParser.h"
#import "LandmarkManager.h"
#import "ZoneParser.h"
#import "EventParser.h"

@interface LoadingViewController () <UIAlertViewDelegate>

@end

@implementation LoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadLocationCheck];
    
    if (IS_IPHONE_5) {
        UIImageView *imageview = (UIImageView *)[self.view viewWithTag:2];
        imageview.image = [UIImage imageNamed:@"Default-568h.png"];
    
        UIView *image = [self.view viewWithTag:1];
        image.center = CGPointMake(image.center.x, image.center.y + (568 - 480 + 40)/2 );

    }
}


#define RADIANS(degrees) ((degrees * M_PI) / 180.0)


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIView *image = [self.view viewWithTag:1];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 0.6;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [image.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


-(void) loadLocationCheck{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    //NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];//Build version
    
    NSString *url = [SERVICE_REQUEST_FEATURED_LOCATION stringByAppendingFormat:@"?version=%@&os=iOS",version];
    [self sendHttpGetRequest:url andRequestId:INITIAL_REQUEST_LOCATION];
}

-(void) loadZoneListDetail{
    [self sendHttpGetRequest:SERVICE_REQUEST_ZONE andRequestId:INITIAL_REQUEST_ZONE];
}

-(void) loadEventList{
    [self sendHttpGetRequest:SERVICE_EVENT_LIST andRequestId:INITIAL_REQUEST_EVENTS];
}

-(void) initloadingFail{
        
}

#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    if (data != nil) {
        
        switch (requestId) {
            case INITIAL_REQUEST_LOCATION:
            {
                NSArray * array = [LandmarkParser parse:data];
                if (array) {
                    [[LandmarkManager shareInstance] resetWithLandmarkList:array];
                }else{

                    [self showNetworkErrMsg];
                    return;
                }
                
                [self loadZoneListDetail];
            }
                break;
                
            case INITIAL_REQUEST_ZONE:
            {
                NSArray * array = [ZoneParser parse:data];
                if (array) {
                    [[LandmarkManager shareInstance] resetZoneList:array];
                }else{

                    [self showNetworkErrMsg];
                    return;
                }
                
                [self loadEventList];
                
            }
                break;
                
            case INITIAL_REQUEST_EVENTS:
            {
                NSArray * array = [EventParser parse:data];
                if (array) {
                    [[LandmarkManager shareInstance] resetEventList:array];
                    [APP_DELEGATE updateUnreadEventCountBadge];
                }
                
                [self.view removeFromSuperview];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_INIT_LOADING_DONE object:nil];
            }
                break;
                
            default:
                break;
        }
        
    }else{

        [self showNetworkErrMsg];
    }
}

-(void) showNetworkErrMsg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enable internet connection on your device" message:@"" delegate:self cancelButtonTitle:@"Try again" otherButtonTitles:nil, nil];
    
    alert.tag = 10003;
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 10003) {
        [self performSelector:@selector(loadLocationCheck) withObject:nil afterDelay:0.2];
    }
}

@end
