//
//  SearchViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchViewCtrl : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
