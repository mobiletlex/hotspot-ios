//
//  SortTableViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/16/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SortTableDelegete <NSObject>
@required
-(void) selectITemAtIndex:(NSInteger) index expandIndex:(int) expandindex sortType:(NSString *) sorttype;
@end
@interface SortTableViewCtrl : UITableViewController
@property(nonatomic,weak) id<SortTableDelegete> delegete;

@property (assign) int selectedIndex;
@property (assign) int expandedIndex;

@end
