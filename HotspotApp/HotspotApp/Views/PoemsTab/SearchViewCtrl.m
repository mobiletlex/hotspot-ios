//
//  SearchViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "SearchViewCtrl.h"
#import "DistricParser.h"
#import "PoemListInfo.h"
#import "PoemInfo.h"
#import "LandmarkManager.h"
#import "DetailViewController.h"


@interface SearchViewCtrl () <UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation SearchViewCtrl{
    
    int _bkCurrentPage;
    int _currentPage;
    int _totalPage;
    NSMutableArray *_poemList;
    
    NSString *_searchText;
    NSString *_bkSearchText;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initializat ion
    }
    return self;
}

- (IBAction)onCloseBtn:(id)sender {
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _poemList = [[NSMutableArray alloc] init];
    _currentPage = 0;
    _totalPage = 0;
    
    _searchBar.barStyle = UIBarStyleDefault;
    _searchBar.showsCancelButton = NO;
    
     [_searchBar setPinkBorder];
    UITextField *txfSearchField = [_searchBar valueForKey:@"_searchField"];
    if(txfSearchField){
        txfSearchField.clearButtonMode = UITextFieldViewModeNever;
        txfSearchField.clearsOnBeginEditing = NO;
        
    }
    
    
    [_searchBar becomeFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_SHOW_DETAIL"]) {
        
        NSIndexPath *indexPath = sender;
        PoemInfo *item = _poemList[indexPath.row];
        if (item.category_id && [item.category_id isEqualToString:POEM_CATEGORY_FEATURED]) {
            item.isFeaturedPoem = YES;
        }else{
            item.isFeaturedPoem = NO;
        }
        
        DetailViewController *vc = (DetailViewController *) segue.destinationViewController;
        vc.poemInfo = item;
    }
}



#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if ([self loadMoreAvailable]) {
//        
//        return 35.0;
//    }
    
    return 5.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if ([self loadMoreAvailable]) {
        
        UITableViewHeaderFooterView *footer = [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        label.textAlignment = NSTextAlignmentCenter;
        //label.text = @"Loading more...";
        [footer addSubview:label];
        
        return footer;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
    if (! [item hasSubLanguage]) {
        return  80.0;
    }
    
    return 100.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _poemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"PoemCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *centralname = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *authorname = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *titlename = (UILabel *)[cell.contentView viewWithTag:3];
    titlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    UILabel *subtitlename = (UILabel *)[cell.contentView viewWithTag:4];
    subtitlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
    
//    if (item.liked) {
//        centralname.textColor = THEME_COLOR_GREEN;
//        authorname.textColor = THEME_COLOR_GREEN;
//        titlename.textColor = THEME_COLOR_GREEN;
//        subtitlename.textColor = THEME_COLOR_GREEN;
//    }else
//    {
//        centralname.textColor = [UIColor blackColor];
//        authorname.textColor = [UIColor blackColor];
//        titlename.textColor = [UIColor blackColor];
//        subtitlename.textColor = [UIColor blackColor];
//    }
    
    titlename.text = item.title;
    authorname.text = item.author;
    centralname.text = [[LandmarkManager shareInstance] zoneNameById:item.zoneId];
    if ([item hasSubLanguage]) {
        subtitlename.text = item.subLanguagePoem.title;
    }else{
        subtitlename.text = @"";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (_searchBar.isFirstResponder) {
        [_searchBar resignFirstResponder];
        return;
    }
    
    [self performSegueWithIdentifier:@"SEGUE_SHOW_DETAIL" sender:indexPath];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section{
    
    if ([self loadMoreAvailable]) {
        //NSLog(@"Loading more in table");
        
        [self loadMorePoemList];
    }
}


-(BOOL) loadMoreAvailable{
    if (_currentPage < _totalPage) {
        
        return YES;
    }
    
    return NO;
}


#pragma mark - UISearchBarDelegate
#define ITEMS_PER_PAGE 20

-(NSString *)urlEncodeUTF:(NSString *) url {
    NSString* escapedUrlString = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return escapedUrlString;
}

-(void) loadPoemList:(int) requetId{
    
    NSString *geturl = [SERVICE_SEARCH_POEM stringByAppendingFormat:@"?title=%@&per_page=%d&page=%d",_searchText, ITEMS_PER_PAGE, _currentPage + 1];
    
    [self sendHttpGetRequest:geturl andRequestId:requetId];
}


- (void) loadMorePoemList{
    [self loadPoemList:1];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //NSLog(@"searchBarSearchButtonClicked");
    
    _bkCurrentPage = _currentPage;
    _currentPage = 0;
    
    //doSearch
    _bkSearchText = _searchText;
    _searchText = searchBar.text;
    _searchText = [self urlEncodeUTF:_searchText];
    
    [searchBar resignFirstResponder];
    [self loadPoemList:2];
}


#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    
    //NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    if (data != nil) {
        
        switch (requestId) {
            case 1://Load more
            {
                PoemListInfo *listpoem = [DistricParser parse:data];
                
                if (listpoem) {
                    _currentPage = listpoem.currentPage;
                    _totalPage = listpoem.totalPage;
                    
                    if (listpoem.dataArray) {
                        int count = _poemList.count;
                        [_poemList addObjectsFromArray:listpoem.dataArray];
                        
                        NSMutableArray *indexarray = [[NSMutableArray alloc] init];
                        
                        if (listpoem.dataArray.count != 0) {
                            
                            for (int i = 0 ; i < listpoem.dataArray.count; i ++) {
                                NSIndexPath *index = [NSIndexPath indexPathForRow:count + i inSection:0];
                                [indexarray addObject:index];
                            }
                            
                            [self.tableView insertRowsAtIndexPaths:indexarray withRowAnimation:UITableViewRowAnimationAutomatic];
                        }
                        
                    }
                    
                }else{
                    [self showNetworkErrMsg];
                }
            }
                
                break;
                
            case 2://Load refresh
            {
                PoemListInfo *listpoem = [DistricParser parse:data];
                
                if (listpoem) {
                    
                    _currentPage = listpoem.currentPage;
                    _totalPage = listpoem.totalPage;
                    
                    if (listpoem.dataArray) {
                        [_poemList removeAllObjects];
                        [_poemList addObjectsFromArray:listpoem.dataArray];
                        
                        self.tableView.contentOffset = CGPointMake(0, 0);
                        [self.tableView reloadData];
                        
                        if (_poemList.count == 0) {
                            [self showAlert:@"No items match your search"];
                        }
                        
                    }else{
                        
                        _searchText = _bkSearchText;
                        _bkSearchText = @"";
                        _currentPage = _bkCurrentPage;
                        _bkCurrentPage= 0;

                    }
                    
                }else{
                    
                    _searchText = _bkSearchText;
                    _bkSearchText = @"";
                    
                    _currentPage = _bkCurrentPage;
                    _bkCurrentPage= 0;
                    
                }
            }
                
                break;
                
            default:
                
                break;
        }
        
        
    }else{
        [self showNetworkErrMsg];
    }
    
}


@end
