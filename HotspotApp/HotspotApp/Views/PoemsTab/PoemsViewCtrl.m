//
//  PoemsViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "PoemsViewCtrl.h"
#import "SortTableViewCtrl.h"
#import "FPPopoverKeyboardResponsiveController.h"
#import "CommonUtils.h"
#import "DistricParser.h"
#import "PoemInfo.h"
#import "LandmarkManager.h"
#import "DetailViewController.h"


@interface PoemsViewCtrl (){
        FPPopoverKeyboardResponsiveController *popover;
    UITableView* sortTable;
}

@end

@implementation PoemsViewCtrl{
    
    NSMutableArray *_poemList;
    int _currentPage;
    int _totalPage;
    
    int _bkupCurrentPage;
    
    NSString *_categoryPoem;
    NSString *_sortType;
    BOOL _searchByZone;
    BOOL _searchByLanguage;
    
    int _sortIndex;
    int _expandIndex;

    NSString *_bkupSortType;

}

#define ITEMS_PER_PAGE 20

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)btnSortTap:(id)sender {
   popover=nil;
    
    //the controller we want to present as a popover
    SortTableViewCtrl *sortTableCtrl = [[SortTableViewCtrl alloc] initWithStyle:UITableViewStylePlain];
    sortTableCtrl.selectedIndex = _sortIndex;
    sortTableCtrl.expandedIndex = _expandIndex;
    
    popover = [[FPPopoverKeyboardResponsiveController alloc] initWithViewController:sortTableCtrl];
    sortTableCtrl.delegete = self;
    popover.tint = FPPopoverDefaultTint;
    
    if ([AppConfiguration isLogin]) {
        popover.contentSize = CGSizeMake(200, 30 * 11);
    }else{
        popover.contentSize = CGSizeMake(200, 30 * 11);
    }
    
    popover.arrowDirection = FPPopoverNoArrow;
    [popover presentPopoverFromView:sender];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _poemList = [[NSMutableArray alloc] init];
    _currentPage = 0;
    _totalPage = 0;
    
    _sortIndex = -1;
    _expandIndex = -1;
    
    _categoryPoem = POEM_CATEGORY_FEATURED;
    
    [self loadRefreshPoemList];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [self.tableView reloadData];
}

-(void) loadPoemList:(int) requetId{
    
    NSString *geturl;
    if (_sortType == nil || [_sortType isEqualToString:@""]) {
        geturl = [SERVICE_GET_POEMLIST stringByAppendingFormat:@"?sort=created_at&status_id=%@&per_page=%d&page=%d",POEM_STATUS_PUBLIC, ITEMS_PER_PAGE, _currentPage + 1];

    }else{
    
        if (_searchByZone) {
            geturl = [SERVICE_GET_POEMLIST stringByAppendingFormat:@"?sort=title&zone_id=%@&status_id=%@&per_page=%d&page=%d",_sortType,POEM_STATUS_PUBLIC,ITEMS_PER_PAGE, _currentPage + 1];

        }else if (_searchByLanguage){
            geturl = [SERVICE_GET_POEMLIST stringByAppendingFormat:@"?sort=language&language_id=%@&status_id=%@&per_page=%d&page=%d",_sortType,POEM_STATUS_PUBLIC, ITEMS_PER_PAGE, _currentPage + 1];

        }else{
            //Sort by author/title
            geturl = [SERVICE_GET_POEMLIST stringByAppendingFormat:@"?sort=%@&status_id=%@&per_page=%d&page=%d",_sortType,POEM_STATUS_PUBLIC, ITEMS_PER_PAGE, _currentPage + 1];
        }
    }
    
    if ([_categoryPoem isEqualToString:POEM_CATEGORY_FEATURED]) {
        //category_id=1
        geturl = [geturl stringByAppendingString:@"&category_id=1"];

    }

    if (_sortType == nil || [_sortType isEqualToString:@""]) {
        geturl = [geturl stringByAppendingString:@"&order=desc"];

    }else{
        geturl = [geturl stringByAppendingString:@"&order=asc"];
    }
    
    [self sendHttpGetRequest:geturl andRequestId:requetId];
}

- (void) loadMorePoemList{
    [self loadPoemList:1];
}

- (void) loadRefreshPoemList{
    [self loadPoemList:2];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if ([self loadMoreAvailable]) {
//        
//        return 35.0;
//    }
    
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if ([self loadMoreAvailable]) {
        
        UITableViewHeaderFooterView *footer = [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        label.textAlignment = NSTextAlignmentCenter;
        //label.text = @"Loading more...";
        [footer addSubview:label];
        
        return footer;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
    if ([item hasSubLanguage]) {
        return  100.0;
    }
    
    return 80.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _poemList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"PoemCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *centralname = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *authorname = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *titlename = (UILabel *)[cell.contentView viewWithTag:3];
    titlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    UIImageView *favIcon = (UIImageView *)[cell.contentView viewWithTag:4];

    UILabel *subtitlename = (UILabel *)[cell.contentView viewWithTag:5];
    subtitlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];

    if (item.liked) {
        centralname.textColor = THEME_COLOR_GREEN;
        authorname.textColor = THEME_COLOR_GREEN;
        titlename.textColor = THEME_COLOR_GREEN;
        subtitlename.textColor = THEME_COLOR_GREEN;
        favIcon.hidden = NO;
    }else
    {
        centralname.textColor = [UIColor blackColor];
        authorname.textColor = [UIColor blackColor];
        titlename.textColor = [UIColor blackColor];
        subtitlename.textColor = [UIColor blackColor];
        favIcon.hidden = YES;
    }
    
    titlename.text = item.title;
    authorname.text = item.author;
    centralname.text = [[LandmarkManager shareInstance] zoneNameById:item.zoneId];
    if ([item hasSubLanguage]) {
        subtitlename.text = item.subLanguagePoem.title;
    }else{
        subtitlename.text = @"";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"SHOW_DETAIL_SCREEN_SEQUE" sender:indexPath];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section{
    
    if ([self loadMoreAvailable]) {
        //NSLog(@"Loading more in table");
        
        [self loadMorePoemList];
    }
}


-(BOOL) loadMoreAvailable{
    if (_currentPage < _totalPage) {
        
        return YES;
    }
    
    return NO;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SHOW_DETAIL_SCREEN_SEQUE"]){
        
        NSIndexPath *indexPath = sender;
        PoemInfo *item = _poemList[indexPath.row];
        
        if ([_categoryPoem isEqualToString:POEM_CATEGORY_PUBLIC]) {
            item.isFeaturedPoem = NO;
        }
        
        DetailViewController *vc = (DetailViewController *) segue.destinationViewController;
        vc.poemInfo = item;
    }
}


#pragma Popover dissmiss delegete

- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController{
    
}

- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController{
    
}
-(void) selectITemAtIndex:(NSInteger) index expandIndex:(int) expandindex sortType:(NSString *) sorttype
{
    [popover dismissPopoverAnimated:YES];
    _sortIndex = index;
    _expandIndex = expandindex;
    
    _currentPage = 0;
    _searchByZone = NO;
    _searchByLanguage = NO;

    if ([sorttype hasPrefix:@"zone:"]) {
        //sort by zone
        _sortType = [sorttype substringFromIndex:5];
        _searchByZone = YES;
    }else if([sorttype hasPrefix:@"language:"]){
        _searchByLanguage = YES;
        _sortType = [sorttype substringFromIndex:9];
    }else{
        _sortType = sorttype;
    }
    
    [self loadRefreshPoemList];

}

- (IBAction)onFeatureBtn:(id)sender {
    
    _categoryPoem = POEM_CATEGORY_FEATURED;

    if (self.feartureButton.selected) {
        return;
    }
    
    _bkupCurrentPage = _currentPage;
    _currentPage = 0;
    
    _bkupSortType = _sortType;
    _sortType = @"";
    _sortIndex = -1;
    _expandIndex = -1;
    _searchByZone = NO;
    _searchByLanguage = NO;
    
    [self loadRefreshPoemList];
}

- (IBAction)onPublicBtn:(id)sender {
    _categoryPoem = POEM_CATEGORY_PUBLIC;
    
    if (self.publicButton.selected) {
        return;
    }
    
    _bkupCurrentPage = _currentPage;
    _currentPage = 0;
    
    _bkupSortType = _sortType;
    _sortType = @"";
    _sortIndex = -1;
    _expandIndex = -1;
    _searchByZone = NO;
    _searchByLanguage = NO;
    
    
    [self loadRefreshPoemList];
}



#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    
    //NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];

    if (data != nil) {
        
        switch (requestId) {
            case 1://Load more
            {
                PoemListInfo *listpoem = [DistricParser parse:data];
                
                if (listpoem) {
                    _currentPage = listpoem.currentPage;
                    _totalPage = listpoem.totalPage;
                    
                    
                    if (listpoem.dataArray) {
                        int count = _poemList.count;
                        [_poemList addObjectsFromArray:listpoem.dataArray];
                        
                        NSMutableArray *indexarray = [[NSMutableArray alloc] init];
                        
                        if (listpoem.dataArray.count != 0) {
                            
                            for (int i = 0 ; i < listpoem.dataArray.count; i ++) {
                                NSIndexPath *index = [NSIndexPath indexPathForRow:count + i inSection:0];
                                [indexarray addObject:index];
                            }
                            
                            [self.tableView insertRowsAtIndexPaths:indexarray withRowAnimation:UITableViewRowAnimationAutomatic];
                        }
                        
                    }
                    
                }else{
                    [self showNetworkErrMsg];
                }
            }
            
                break;
                
            case 2://Load refresh
            {
                PoemListInfo *listpoem = [DistricParser parse:data];
                
                if (listpoem) {
                    if ([_categoryPoem isEqualToString:POEM_CATEGORY_PUBLIC]) {
                        self.feartureButton.selected = NO;
                        self.publicButton.selected = YES;

                    }else{
                        self.feartureButton.selected = YES;
                        self.publicButton.selected = NO;
                    }
                    
                    _currentPage = listpoem.currentPage;
                    _totalPage = listpoem.totalPage;
                    
                    if (listpoem.dataArray) {
                        [_poemList removeAllObjects];
                        [_poemList addObjectsFromArray:listpoem.dataArray];
                    }
                    
                    self.tableView.contentOffset = CGPointMake(0, 0);
                    [self.tableView reloadData];
                    
                }else{
                    _currentPage = _bkupCurrentPage;
                    _bkupCurrentPage = 0;
                    
                    _sortType = _bkupSortType;
                    _bkupSortType = @"";
                    
                    [self showNetworkErrMsg];
                }
            }
                
                break;
                
            default:
                
                break;
        }
        
        
    }else{
        [self showNetworkErrMsg];
    }
    
}

@end