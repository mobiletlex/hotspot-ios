//
//  PoemsViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
#import "FPPopoverController.h"
#import "SortTableViewCtrl.h"

@interface PoemsViewCtrl : BaseViewController<UITableViewDataSource,UITableViewDelegate,FPPopoverControllerDelegate,SortTableDelegete>


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *btnSort;

@property (weak, nonatomic) IBOutlet UIButton *feartureButton;
@property (weak, nonatomic) IBOutlet UIButton *publicButton;

- (IBAction)onFeatureBtn:(id)sender;
- (IBAction)onPublicBtn:(id)sender;


@end
