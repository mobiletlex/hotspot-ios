//
//  SortTableViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/16/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "SortTableViewCtrl.h"
#import "AppConfiguration.h"

@interface SortTableViewCtrl ()

@end

@implementation SortTableViewCtrl{
    
    NSMutableArray * _sortTitleArray;
    int _expandedType;
    
    int _languageSelectedIndex;
    int _zoneSelectedIndex;
}
@synthesize selectedIndex;
@synthesize expandedIndex;

@synthesize delegete;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = THEME_COLOR_GREEN;
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.bounces = NO;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _expandedType = -1;
    _languageSelectedIndex = -1;
    _zoneSelectedIndex = -1;
    
    _sortTitleArray = [[NSMutableArray alloc] init];
    
    if (self.expandedIndex == 0) {
        [self setupTitleSortWithLanguage];
        _languageSelectedIndex = self.selectedIndex;

    }else if (self.expandedIndex == 1){
        [self setupTitleSortWithZone];
        _zoneSelectedIndex = self.selectedIndex;
        
    }else{
        [self setupTitleSorPure];
    }
}

-(void) setupTitleSorPure{
    [_sortTitleArray removeAllObjects];
    _expandedType = -1;
    
    NSArray *array = [NSArray arrayWithObjects:@"Sort by Authors",
                      @"Sort by Titles",
                      @"Sort by Languages",
                      @"Sort by Locations",
                      
                      nil];
    
    [_sortTitleArray addObjectsFromArray:array];
    
    if ([AppConfiguration isLogin]) {
        [_sortTitleArray addObject:@"Sort by Favourites"];
    }
    
}


-(void) setupTitleSortWithLanguage{
    [_sortTitleArray removeAllObjects];
    _expandedType = 0;
    
    NSArray *array = [NSArray arrayWithObjects:@"Sort by Authors",
                      @"Sort by Titles",
                      @"Sort by Languages",
                      @"  • English",
                      @"  • 中文",
                      @"  • Melayu",
                      @"  • Tamil",
                      @"Sort by Locations",

                      nil];
    
    [_sortTitleArray addObjectsFromArray:array];
    
    if ([AppConfiguration isLogin]) {
        [_sortTitleArray addObject:@"Sort by Favourites"];
    }
}

-(void) setupTitleSortWithZone{
    [_sortTitleArray removeAllObjects];
    _expandedType = 1;
    
    NSArray *array = [NSArray arrayWithObjects:@"Sort by Authors",
                      @"Sort by Titles",
                      @"Sort by Languages",
                      @"Sort by Locations",
                      @"  • Central",
                      @"  • East",
                      @"  • North",
                      @"  • North-East",
                      @"  • West",
                      @"  • Islands",
                      
                      nil];
    
    [_sortTitleArray addObjectsFromArray:array];
    
    if ([AppConfiguration isLogin]) {
        [_sortTitleArray addObject:@"Sort by Favourites"];
    }
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _sortTitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SortTableCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] init];
        cell.textLabel.font = [UIFont fontWithName:@"Futura-Medium" size:14];
    }
  
    // Configure the cell...
    cell.textLabel.text = _sortTitleArray[indexPath.row];
    
    if(_expandedType != -1){
        if (_expandedType == 0 && indexPath.row == _languageSelectedIndex) {
            cell.textLabel.textColor = [UIColor blackColor];
        }else if (_expandedType == 1 && indexPath.row == _zoneSelectedIndex){
            cell.textLabel.textColor = [UIColor blackColor];
        }else if (_expandedType == -1 && indexPath.row == selectedIndex){
            cell.textLabel.textColor = [UIColor blackColor];
        }else{
            cell.textLabel.textColor = [UIColor whiteColor];
        }
    }
    else{
        cell.textLabel.textColor = [UIColor whiteColor];
        if (self.expandedIndex == -1 && self.selectedIndex != -1) {
            if (indexPath.row == self.selectedIndex) {
                cell.textLabel.textColor = [UIColor blackColor];
            }
        }
    }
    
    int row = indexPath.row;
    cell.backgroundColor = THEME_COLOR_GREEN;
    if (_expandedType == 0) {//language
        if (row >= 3 && row <= 6) {
            cell.backgroundColor = [UIColor colorWithRed:90.0/255.0 green:195.0/255.0 blue:188.0/255.0 alpha:1.0];
        }
    }else if (_expandedType== 1){//location
        if (row >= 4 && row <= 9) {
            cell.backgroundColor = [UIColor colorWithRed:90.0/255.0 green:195.0/255.0 blue:188.0/255.0 alpha:1.0];
        }
    }

    
    return cell;
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int row = indexPath.row;
    
    if (row == 2) {//language
        if(_expandedType == 0){
            return;
        }
        
        if(_expandedType != 0){
            
            selectedIndex = -1;

            [self setupTitleSortWithLanguage];
            [self.tableView reloadData];
            
            return;
        }
    }
    
    
    if (row == 3) {//location
        if(_expandedType == 1){
            return;
        }
        
        if(_expandedType != 1 && _expandedType != 0){
            
            selectedIndex = -1;
            
            [self setupTitleSortWithZone];
            [self.tableView reloadData];
            
            return;
        }
    }
    
    
    if (row == 7) {//location
        if(_expandedType == 0){
            selectedIndex = -1;

            [self setupTitleSortWithZone];
            [self.tableView reloadData];
            
            return;
        }
        
    }
    
    NSString *sorttype = @"";
    NSString *subsorttype = @"";

    if (row == 0) {
        sorttype = @"author";
    }else if (row == 1){
        sorttype = @"title";
    }else if(row == _sortTitleArray.count - 1){
        if ([AppConfiguration isLogin]) {
            sorttype = @"favourite";
        }
    }
    
    if ([sorttype isEqualToString:@""]) {
        
        if (_expandedType == 0) {//language
            if (row >= 3 && row <= 6) {
                subsorttype = @"language:";
                
                switch (row) {
                    case 3:
                        //english
                        sorttype = [subsorttype stringByAppendingString:@"1"];
                        break;
                    case 4:
                        //chinease
                        sorttype = [subsorttype stringByAppendingString:@"2"];
                        break;
                    case 5:
                        //malay
                        sorttype = [subsorttype stringByAppendingString:@"4"];
                        break;
                    case 6:
                        //tamil,
                        sorttype = [subsorttype stringByAppendingString:@"3"];
                        
                        break;
                        
                    default:
                        sorttype = @"language";
                        subsorttype = @"";
                        
                        break;
                }

            }
        }else if (_expandedType== 1){//location
            
            if (row >= 4 && row <= 9) {
                subsorttype = @"zone:";
                
                switch (row) {
                    case 4:
                        //central
                        sorttype = [subsorttype stringByAppendingString:@"5"];
                        break;
                    case 5:
                        //east
                        sorttype = [subsorttype stringByAppendingString:@"8"];
                        break;
                    case 6:
                        //north
                        sorttype = [subsorttype stringByAppendingString:@"6"];
                        break;
                    case 7:
                        //north-East Singapore",
                        sorttype = [subsorttype stringByAppendingString:@"4"];

                        break;
                    case 8:
                        //@"  > West Singapore",
                        sorttype = [subsorttype stringByAppendingString:@"7"];
                        break;
                    case 9:
                        //@"  > Islands"
                        sorttype = [subsorttype stringByAppendingString:@"10"];
                        break;
                        
                    default:
                        sorttype = @"location_id";
                        subsorttype = @"";
                        
                        break;
                }
            }
        }
    }
    
    if([self.delegete respondsToSelector:@selector(selectITemAtIndex:expandIndex:sortType:)])
    {
        [self.delegete selectITemAtIndex:row expandIndex:_expandedType sortType:sorttype];
    }
}


@end
