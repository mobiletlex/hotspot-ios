//
//  EnglishVersionViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "EnglishVersionViewController.h"
#import "SBJSON.h"
#import "MainTabbarCtrl.h"
#import "PoemInfo.h"
#import "CommonParser.h"
#import "ShareViewController.h"
#import "MyEntriesViewCtrl.h"
#import "AppUtils.h"

@interface EnglishVersionViewController () <UITextFieldDelegate, UITextViewDelegate>

@end

@implementation EnglishVersionViewController{
    
    PoemInfo *postedPoem;
    BOOL isDone;
}

@synthesize orginalParams;
@synthesize poemInfoEditting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    
    
    self.tfTitle.delegate = self;
    self.tvContent.delegate = self;
    
    [[NSBundle mainBundle] loadNibNamed:@"ToolbarKeybardAction" owner:self options:nil];
    UIBarButtonItem *donebtn = self.actionToolbar.items[1];
    [donebtn setTitleTextAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:17], NSForegroundColorAttributeName: [UIColor blueColor]} forState:UIControlStateNormal];
    
    [self.tvContent setInputAccessoryView:self.actionToolbar];
    
    
    if (![self isCreatingNewPoem]) {
        self.title = @"EDIT";
        
        self.tfTitle.text = self.poemInfoEditting.subLanguagePoem.title;
        self.tvContent.text = self.poemInfoEditting.subLanguagePoem.content;
    
        if (self.poemInfoEditting.subLanguagePoem.content && self.poemInfoEditting.subLanguagePoem.content.length != 0) {
            self.tfContent.placeholder = @"";
        }
        
        if ([self isSummittedPoem]) {
            self.saveButton.hidden = YES;
        }
    }
    
    
    self.navigationItem.hidesBackButton = YES;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (isDone) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(BOOL) isCreatingNewPoem{
    if (self.poemInfoEditting == nil) {
        return YES;
    }
    
    return NO;
}

-(BOOL) isSummittedPoem{
    
    if (self.poemInfoEditting) {
        return self.poemInfoEditting.isSubmitted;
    }
    
    return NO;
}


-(BOOL) validateFormUpload{
    
    NSString *title = self.tfTitle.text;
    if (title == nil || [title isEqualToString:@""] ) {
        
        [self showAlert:@"Please input the title"];
        [self.tfTitle becomeFirstResponder];
        return NO;
    }
    
    if ( ![AppUtils isValidContentWithSymbolChar:title] || ![AppUtils validStringAfterTrimWhitespace:title]) {
        [self showAlert:@"Please input a valid title"];
        [self.tfTitle becomeFirstResponder];

        return NO;

    }
    
//    NSString *content = self.tvContent.text;
//    if (content == nil || [content isEqualToString:@""]) {
//        [self showAlert:@"Please input the content"];
//        return NO;
//    }
    
    return YES;
}

-(void) setupParamsForEnglishVersion{
    
//    [self.orginalParams setValue:@"1" forKey:@"poem_contents[1][language_id]"];//English
//    [self.orginalParams setValue:self.tfTitle.text forKey:@"poem_contents[1][title]"];
//    [self.orginalParams setValue:self.tvContent.text forKey:@"poem_contents[1][content]"];
    
    [self.orginalParams setValue:self.tfTitle.text forKey:@"title_alt"];
    [self.orginalParams setValue:self.tvContent.text forKey:@"content_alt"];
}

- (IBAction)onSubmitBtn:(id)sender {

    if (![self validateFormUpload]) {
        return;
    }
    
    [self setupParamsForEnglishVersion];
    
    [self.orginalParams setValue:POEM_STATUS_PUBLIC forKey:@"status_id"];//Publish

    //[self sendHttpPostRequest:SERVICE_NEW_POEM body:self.orginalParams andRequestId:1];

    if ([self isCreatingNewPoem]) {
        [self sendHttpPostRequest:SERVICE_NEW_POEM body:self.orginalParams andRequestId:1];
    }else{
        //Edit
        [self sendHttpPutRequest:[SERVICE_EDIT_POEM stringByAppendingString:self.poemInfoEditting.poemid] body:self.orginalParams andRequestId:1];

    }
}

- (IBAction)onSaveBtn:(id)sender {
    
    if (![self validateFormUpload]) {
        return;
    }
    
    [self setupParamsForEnglishVersion];

    [self.orginalParams setValue:POEM_STATUS_DRAFT forKey:@"status_id"];//draft

    //[self sendHttpPostRequest:SERVICE_NEW_POEM body:self.orginalParams andRequestId:2];
    
    if ([self isCreatingNewPoem]) {
        [self sendHttpPostRequest:SERVICE_NEW_POEM body:self.orginalParams andRequestId: 2];
    }else{
        //Edit
        [self sendHttpPostRequest:[SERVICE_EDIT_POEM stringByAppendingString:self.poemInfoEditting.poemid] body:self.orginalParams andRequestId:2];
        
    }
}

- (IBAction)onBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onDoneKeyboardAction:(id)sender {
    [self.tvContent resignFirstResponder];
}


#pragma mark- Navigation storyboard.

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   if ([segue.identifier isEqualToString:@"SEGUE_SHARE_VIEW"]){
       
       isDone = YES;
       
        ShareViewController *vc = segue.destinationViewController;
        vc.poemInfo = postedPoem;
    }
}


#pragma mark- Keyboard Handeling.

- (void) keyboardWillShow:(NSNotification *)note {
    
    if (_tvContent.isFirstResponder) {
        if (self.view.frame.origin.y >= 0){
            [self setViewMovedUp:YES withHeigh:80];
        }
        
        CGRect frame = _tvContent.frame;
        //descrease height
        if (IS_IPHONE_5) {
            frame.size.height = 250;
        }else{
            frame.size.height = 162 ;
        }
        _tvContent.frame = frame;
        
    }
}

- (void) keyboardDidHide:(NSNotification *)note {
    if (_tvContent.isFirstResponder) {
        if (self.view.frame.origin.y < 0){
            [self setViewMovedUp:NO withHeigh:80];
        }
        
        CGRect frame = _tvContent.frame;
        
        //restore full height
        if (IS_IPHONE_5) {
            frame.size.height = 320;
        }else{
            frame.size.height = 232 ;
        }

        _tvContent.frame = frame;
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.tfContent.placeholder = @"";
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        self.tfContent.placeholder = @"English translation of poem (optional)";
    }
    
    return YES;
}

- (BOOL)textView:(UITextView *)tView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    CGRect textRect = [tView.layoutManager usedRectForTextContainer:tView.textContainer];
    CGFloat sizeAdjustment = tView.font.lineHeight * [UIScreen mainScreen].scale;
    
    if (textRect.size.height >= tView.frame.size.height - sizeAdjustment) {
        if ([text isEqualToString:@"\n"]) {
            [UIView animateWithDuration:0.2 animations:^{
                [tView setContentOffset:CGPointMake(tView.contentOffset.x, tView.contentOffset.y + sizeAdjustment)];
            }];
        }
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.tfTitle) {
        
        NSString *text = textField.text;
        text = [text stringByReplacingCharactersInRange:range withString:string];
        if (text.length > 120) {
            return NO;
        }
        
    }
    
    return YES;
}


-(void) moveToSubmitListSubmitted:(BOOL) submitted {
    UITabBarController *tabbarvc = (UITabBarController *)APP_DELEGATE.window.rootViewController;
    UINavigationController *navvc = tabbarvc.viewControllers[E_TAB_MY_ENTRIES];
    NSArray *arr = navvc.viewControllers;
    if (arr && arr.count > 0) {
        MyEntriesViewCtrl *vc = (MyEntriesViewCtrl *)arr[0];
        if (submitted) {
            vc.willOpenSubmitAfterShare = YES;
        }else{
            vc.willOpenDraftList = YES;
        }
    }
    [navvc popToRootViewControllerAnimated:NO];
    tabbarvc.selectedIndex = E_TAB_MY_ENTRIES;
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    SBJSON *jsonParser = [[SBJSON alloc] init];
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSDictionary *dic = [jsonParser objectWithString:jsonString];
    
    if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
    {
        id errordic = [dic valueForKey:@"error"];
        
        if (errordic != nil) {
            
            if ([self isCreatingNewPoem]) {
                [self showAlert:@"Can not submit new poem"];
            }else{
                [self showAlert:@"Can not update poem"];
                
            }
        
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_POSTED_NEW_POEM object:nil];

            if (requestId == 2) { //Saving
                //move to myentries tab
                [self moveToSubmitListSubmitted:NO];
                
                [self showAlert:@"Your poem has been saved to draft successfully"];

            }else{ //Submit
                postedPoem = [CommonParser parsePoemInfo:dic];
                if ([self isCreatingNewPoem]) {
                    [self performSegueWithIdentifier:@"SEGUE_SHARE_VIEW" sender:self];

                }else{
                    [self moveToSubmitListSubmitted:YES];

                }
                [self showAlert:@"Your poem has been submitted successfully"];

            }
        }
    }else{
        if (dic == nil) {
            
            if ([self isCreatingNewPoem]) {
                [self showAlert:@"Can not submit new poem"];
            }else{
                [self showAlert:@"Can not update poem"];
                
            }
        
        }else{
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

@end
