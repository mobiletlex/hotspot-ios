//
//  LoginViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewCtrl : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPass;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)onCancelLognNavBtn:(id)sender;

- (IBAction)onSignUpBtn:(id)sender;
- (IBAction)onForgetPasswordBtn:(id)sender;

@end
