//
//  AcceptTermViewController.m
//  HotspotApp
//
//  Created by hmchau on 8/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "AcceptTermViewController.h"
#import "RTLabel.h"
#import "TextWebViewController.h"
#import "CommonUtils.h"

@interface AcceptTermViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation AcceptTermViewController{
    
    NSString *text1;
    NSString *text2;
    NSString *text3;
    NSString *text4;
    NSString *text5;
    NSString *text6;
    NSString *text7;
    
    NSArray *textArray;
    
    BOOL checkedArray[5];

}

@synthesize parentController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    text1 = @"I consent to the collection, use and/or disclosure of my personal data by Arts House Limited, its related corporations and their respective successors, assignees and transferees (individually a \"Company\" and collectively, the \"Companies\") for the purpose of receiving information and announcements on their programmes, promotions, offers, activities and events, including The Arts House, Goodman Arts Centre, Aliwal Arts Centre and the Singapore International Festival of Arts.";
    
    text2 = @"Either:\n(a) I am above 18 years of age; or\n(b) I am less than 18 but above 14 years of age and understand the nature of my consent to the collection, use and/or disclosure of my personal data and the consequences of such consent.";
    
    text3 = @"I have read and understood the ";
    
    text4 = @"I have reviewed and declare that the personal data I have submitted is accurate and complete.";

    
    text5 = @"I consent to the collection, use and/or disclosure of my personal data by any Company for the purpose of consumer and/or market research.";
    
    text6 = @"By clicking on the Submit Button, you indicate that you are agreeable to all of the above and that you have agreed to the terms stated in the Privacy Policy.";

    textArray = [NSArray arrayWithObjects:text1,text2,text3,text4,text5,text6, nil];
    
    self.acceptButton.enabled = NO;
    
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor whiteColor];
}


#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 || section == 4) {
        return 30;
    }
    
    return 1.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0 || section == 4) {
        UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 320, 30)];
        label.backgroundColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Futura-Bold" size:15];
        if (section == 0) {
            label.text = @"  Compulsory";
        }else{
            label.text = @"  Optional";

        }
        
        return label;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSString *text = textArray[indexPath.section];

    
    int section = indexPath.section;
    
    switch (section) {
        case 0:
            return 270;
            
        case 1:
            return 150;
            
        case 2:
            return 52;
            
        case 3:
            return 80;
            
        case 4:
            return 90;
            
        case 5:
            return 100;
            
        default:
            break;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return textArray.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"TermCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.contentView.tag = indexPath.section + 100;
    
    UIButton *button = (UIButton *)[cell.contentView viewWithTag:1];
    if (button) {
        [button addTarget:self action:@selector(onAccepItem:) forControlEvents:UIControlEventTouchUpInside];
    }

    UITextView *textview = (UITextView *)[cell.contentView viewWithTag:2];
    textview.scrollEnabled = NO;
    textview.textContainer.heightTracksTextView = YES;
    textview.textColor=[UIColor blackColor];
    textview.text = textArray[indexPath.section];
    textview.hidden = NO;
    textview.backgroundColor = [UIColor clearColor];
    
    UIView *hiddenView = [cell.contentView viewWithTag:100031];
    if (hiddenView) {
        [hiddenView removeFromSuperview];
    }
    hiddenView = [cell.contentView viewWithTag:100032];
    if (hiddenView) {
        [hiddenView removeFromSuperview];
    }

    hiddenView = [cell.contentView viewWithTag:100033];
    if (hiddenView) {
        [hiddenView removeFromSuperview];
    }

    //add hyperlink button
    if (indexPath.section == 2) {
        UIColor *bgbtncolor = [UIColor clearColor];
        UIButton *hpbtn = [[UIButton alloc] initWithFrame:CGRectMake(35, 22, 50, 30)];
        hpbtn.tag = 100033;
        hpbtn.backgroundColor = bgbtncolor;
        [cell.contentView addSubview:hpbtn];
        [hpbtn addTarget:self action:@selector(onPrivacyBtn) forControlEvents:UIControlEventTouchUpInside];

        UIButton *hpbtnright = [[UIButton alloc] initWithFrame:CGRectMake(260, 0, 50, 30)];
        hpbtnright.tag = 100032;
        hpbtnright.backgroundColor = bgbtncolor;
        [cell.contentView addSubview:hpbtnright];
        [hpbtnright addTarget:self action:@selector(onPrivacyBtn) forControlEvents:UIControlEventTouchUpInside];

        
        RTLabel *label = [[RTLabel alloc] initWithFrame: CGRectMake(textview.frame.origin.x + 5, textview.frame.origin.y + 7, textview.frame.size.width, 50)];
        label.font = [UIFont fontWithName:@"Futura" size:15.0];
        label.tag = 100031;
        label.userInteractionEnabled = NO;

        NSString *text = [NSString stringWithFormat:@"%@<font color='#E80C8A'>Privacy Policy</font>",  textArray[indexPath.section]];
        
        [label setText: text];
        [cell.contentView addSubview:label];
        
        textview.hidden = YES;
    }
    
    
    button.hidden = NO;
    if (indexPath.section == 5) {
        button.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

-(void) onPrivacyBtn{
    NSLog(@"onPrivacyBtn");
    [self performSegueWithIdentifier:@"SEGUE_SHOW_WEBTEXT" sender:self];
    
}

-(void) onAccepItem:(UIButton *) sender{
    //NSLog(@"press on item %d", sender.superview.tag);
    
    sender.selected = !sender.selected;
    
    int index = sender.superview.tag - 100;
    checkedArray[index] = sender.selected;

    
    for (int i = 0 ; i < 4;  i ++) {
        if (checkedArray[i] == NO) {
            self.acceptButton.enabled = NO;
            return;
        }
    }

    self.acceptButton.enabled = YES;

}


- (IBAction)onBackBtn:(id)sender {
    
    if (self.parentController) {
        [self.parentController performSelector:@selector(onAcceptTermOfUse:) withObject:[NSNumber numberWithBool:NO]];
    }
}

- (IBAction)onAcceptBtn:(id)sender {
    
    for (int i = 0 ; i < 4;  i ++) {
        if (checkedArray[i] == NO) {
            [self showAlert:@"Please confirm and check all Terms of use."];
            return;
        }
    }
    
    if (self.parentController) {
        [self.parentController performSelector:@selector(onAcceptTermOfUse:) withObject:[NSNumber numberWithBool:YES]];
         }
}

#pragma mark - Navigation

-(NSString *) readFileContent:(NSString *) file{
    
    NSString *_htmlText = @"";
    
    NSString *filename = [CommonUtils resoucePathForFileName:file];
    NSData *filedata = [NSData dataWithContentsOfFile:filename];
    if (filedata) {
        _htmlText = [[NSString alloc] initWithData:filedata encoding:NSUTF8StringEncoding];
    }
    
    return _htmlText;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_SHOW_WEBTEXT"]) {
        TextWebViewController *vc = (TextWebViewController *)segue.destinationViewController;
        
        vc.title = @"Privacy";
        vc.htmlText = [self readFileContent:@"privacy.txt"];
    }
}

@end
