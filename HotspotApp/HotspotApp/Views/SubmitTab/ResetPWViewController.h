//
//  ResetPWViewController.h
//  HotspotApp
//
//  Created by hmchau on 9/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface ResetPWViewController : BaseViewController

@property (nonatomic,weak) UIViewController *parentController;


@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

- (IBAction)onSendBtn:(id)sender;
- (IBAction)onCloseBtn:(id)sender;

@end
