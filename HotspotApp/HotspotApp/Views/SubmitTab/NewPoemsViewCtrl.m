//
//  SubmitViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "NewPoemsViewCtrl.h"
#import "LocationViewCtrl.h"
#import "EnglishVersionViewController.h"
#import "SBJSON.h"
#import "MainTabbarCtrl.h"
#import "PoemInfo.h"
#import "AppUtils.h"
#import "CommonParser.h"
#import "ShareViewController.h"
#import "MyEntriesViewCtrl.h"
#import "AddImageViewCtrl.h"
#import "LandmarkInfo.h"
#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"
#import "AppUtils.h"

@interface NewPoemsViewCtrl () <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate, UITextFieldDelegate , UITextViewDelegate,AddImageDelegete >

@property (weak, nonatomic) IBOutlet UITextField *tfTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfContent;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UITextView *tvContent;

@end

@implementation NewPoemsViewCtrl{
    
    NSArray* _arrayOfLanguageBtn;
    NSArray* _arrayOfCategoryBtn;
    
    int languageindex;
    int categoryindex;
    
    NSString *_locationId;
    NSMutableDictionary *_dicPostPoemParam;
    
    UIImage *_attachImage;
    NSString *_attachImageCaption;
    BOOL _attachImageWillDelete;
    
    PoemInfo *postedPoem;
}

@synthesize poemInfoEditting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetSessionPostNewPoem) name:NOTIFICATION_POSTED_NEW_POEM object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    
    [[NSBundle mainBundle] loadNibNamed:@"ToolbarKeybardAction" owner:self options:nil];
    UIBarButtonItem *donebtn = self.actionToolbar.items[1];
    [donebtn setTitleTextAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:17], NSForegroundColorAttributeName: [UIColor blueColor]} forState:UIControlStateNormal];
    
    [_tvContent setInputAccessoryView:self.actionToolbar];
    
    
    _tfTitle.delegate = self;
    _tvContent.delegate = self;
    
    
    _dicPostPoemParam = [[NSMutableDictionary alloc] init];
    
    languageindex = 0;
    categoryindex = 0;
    
    UIView *lang1 = [self.view viewWithTag:1];
    UIView *lang2 = [self.view viewWithTag:2];
    UIView *lang3 = [self.view viewWithTag:3];
    UIView *lang4 = [self.view viewWithTag:4];
    _arrayOfLanguageBtn = [NSArray arrayWithObjects:lang1,lang2,lang3,lang4, nil];
    
    for (UIButton *bt in _arrayOfLanguageBtn) {
        bt.titleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
    }
    
    UIView *cat1 = [self.view viewWithTag:10];
    UIView *cat2 = [self.view viewWithTag:11];
    _arrayOfCategoryBtn = [NSArray arrayWithObjects:cat1,cat2, nil];
    
    for (UIButton *bt in _arrayOfCategoryBtn) {
        bt.titleLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
    }
    
    self.locationNameLabel.text = @"";
    self.thumbnailImageView.hidden = YES;

    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    //Setup edit poem from draft
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    if (![self isCreatingNewPoem]) {
        self.title = @"EDIT";
        
        _tfTitle.text = self.poemInfoEditting.title;
        _tvContent.text = self.poemInfoEditting.content;
        self.tfContent.placeholder = @"";
        
        
        //setup category buttons
        for (UIButton *btn in _arrayOfCategoryBtn) {
            btn.hidden = YES;
        }
        
        if (self.poemInfoEditting.category_id != nil && [self.poemInfoEditting.category_id isEqualToString: POEM_CATEGORY_PUBLIC]) {
            ((UIButton*)_arrayOfCategoryBtn[0]).hidden = NO;
            ((UIButton*)_arrayOfCategoryBtn[0]).selected = YES;
            categoryindex = 0;
            
        }else{
            ((UIButton*)_arrayOfCategoryBtn[1]).hidden = NO;
            ((UIButton*)_arrayOfCategoryBtn[1]).selected = YES;
            categoryindex = 1;

            ((UIButton*)_arrayOfCategoryBtn[1]).center = ((UIButton*)_arrayOfCategoryBtn[0]).center;
        }
        
        
        
        //setup language buttons
        for (UIButton *btn in _arrayOfLanguageBtn) {
            btn.hidden = YES;
        }
        if ([self.poemInfoEditting hasSubLanguage]) {
            int buttonindex = [AppUtils getIndexLangugeButtonFromLanguageId:self.poemInfoEditting.languageId];
            languageindex = buttonindex;
            
            UIButton *currentButton = ((UIButton*)_arrayOfLanguageBtn[buttonindex]);
            currentButton.hidden = NO;
            currentButton.selected = YES;
            
            CGRect frame = ((UIButton*)_arrayOfLanguageBtn[0]).frame;
            currentButton.frame =  CGRectMake(frame.origin.x, frame.origin.y, currentButton.frame.size.width, currentButton.frame.size.height);
        }else{
            int buttonindex = 0;
            languageindex = buttonindex;
            
            UIButton *currentButton = ((UIButton*)_arrayOfLanguageBtn[buttonindex]);
            currentButton.hidden = NO;
            currentButton.selected = YES;
        }
        
        //disable save function if poem is submitted
        if ([self isSummittedPoem]) {
            self.btnSave.hidden = YES;
        }
        
        for (int i = 5; i <= 9 ; i ++) {
            UILabel *dotlabel = (UILabel *)[self.view viewWithTag:i];
            if (dotlabel != nil) {
                dotlabel.hidden = YES;
            }
        }
        
        
        //set up location
        _locationId = [NSString stringWithFormat:@"%d", self.poemInfoEditting.locationId];
        if (self.poemInfoEditting.locationName != nil) {
            self.locationNameLabel.text = self.poemInfoEditting.locationName;
        }
        
        //set up image thumbnail
        BOOL hasThumbnail = NO;
        if (self.poemInfoEditting.photos) {
            NSArray *photosarr = self.poemInfoEditting.photos;
            if (photosarr.count > 0) {
                NSString *url = photosarr[0];
                if (url && ![url isEqualToString:@""]) {
                    //[self.attachImageBtn setBackgroundImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal];
                    
                    //[self.thumnailImage setImageWithURL:[NSURL URLWithString:url] ];
                    [self.thumnailImage setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"img_no_photo.png"]];

                    hasThumbnail = YES;
                }
            }
        }
        
        self.thumbnailImageView.hidden = !hasThumbnail;
    }
    
}


- (void) viewWillAppear:(BOOL)animated{
    
    if(languageindex == 0){
        //Is English
        if (! [self isSummittedPoem]) {
            self.btnSave.hidden = NO;
        }
        
        [self setTitle:@"Submit" forButton:self.btnNext];
    }
    else{
        self.btnSave.hidden = YES;
        [self setTitle:@" Next" forButton:self.btnNext];
    }    
}

-(void) setTitle:(NSString *)title forButton:(UIButton *) btn{
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
}

-(BOOL) isCreatingNewPoem{
    if (self.poemInfoEditting == nil) {
        return YES;
    }
    
    return NO;
}

-(BOOL) isSummittedPoem{
    
    if (self.poemInfoEditting) {
        return self.poemInfoEditting.isSubmitted;
    }
    
    return NO;
}

- (IBAction)onImageAttachBtn:(id)sender {
    
    [self showActionSheetCameraSourceWithDelegate:self];
}

- (IBAction)onLanguageSelectBtn:(UIButton *)sender {
    
    if (![self isCreatingNewPoem]) {
        return;
    }
    
    
    languageindex = (int)sender.tag - 1;
    
    for (UIButton *btn in _arrayOfLanguageBtn) {
        btn.selected = NO;
    }
    
    sender.selected = YES;
    
    // if not english
    if(languageindex == 0){
        self.btnSave.hidden = NO;
        
        if ([self isSummittedPoem]) {
            self.btnSave.hidden = YES;
        }

        [self setTitle:@"Submit" forButton:self.btnNext];

    }
    else{
        self.btnSave.hidden = YES;
        [self setTitle:@" Next" forButton:self.btnNext];

    }
}

- (IBAction)onCategorySelectBtn:(UIButton *)sender {
    
    categoryindex = sender.tag - 10;
    
    for (UIButton *btn in _arrayOfCategoryBtn) {
        btn.selected = NO;
    }
    
    sender.selected = YES;
}

- (IBAction)onDoneKeyboardAction:(id)sender {
    [self.tvContent resignFirstResponder];
}

-(void) createUploadParam{
    
    [_dicPostPoemParam removeAllObjects];
    
    [_dicPostPoemParam setValue:_locationId forKey:@"location_id"];
    
    if (_attachImage != nil) {
        NSData *imageData = UIImageJPEGRepresentation(_attachImage, 0.5);
        [_dicPostPoemParam setValue:imageData forKey:@"photo_data"];
        
        if (_attachImageCaption != nil) {
            [_dicPostPoemParam setValue:_attachImageCaption forKey:@"caption"];
        }
    }else{
        if (![self isCreatingNewPoem]) {
            //Check delete image
            if (_attachImageWillDelete == YES) {
                [_dicPostPoemParam setValue:@"-1" forKey:@"photo"];
            }
        }
    }
    
    
    if (![self isCreatingNewPoem]) {
        //For edit poem
        [_dicPostPoemParam setValue:self.poemInfoEditting.poemid forKey:@"id"];
    }
    
    NSString * categoryid = POEM_CATEGORY_PUBLIC;
    switch (categoryindex) {
        case 0://public
            categoryid = POEM_CATEGORY_PUBLIC;
            break;
            
        case 1://school
            categoryid = POEM_CATEGORY_SCHOOL;
            break;
            
        default:
            break;
    }
    [_dicPostPoemParam setValue:categoryid forKey:@"category_id"];
    int langeageId = [AppUtils getLanguageIdFromButtonIndex:languageindex];
    
//Change server anazone
//    [_dicPostPoemParam setValue:[NSString stringWithFormat:@"%d",langeageId] forKey:@"poem_contents[0][language_id]"];//Biolanguge id
//    [_dicPostPoemParam setValue:self.tfTitle.text forKey:@"poem_contents[0][title]"];
//    [_dicPostPoemParam setValue:self.tvContent.text forKey:@"poem_contents[0][content]"];

    [_dicPostPoemParam setValue:[NSString stringWithFormat:@"%d",langeageId] forKey:@"language_id"];//Biolanguge id
    [_dicPostPoemParam setValue:self.tfTitle.text forKey:@"title"];
    [_dicPostPoemParam setValue:self.tvContent.text forKey:@"content"];
    
}

-(BOOL) validateFormUpload{
    
    if (_locationId == nil) {
        [self showAlert:@"Please choose or create a location"];
        return NO;
    }
    
    NSString *title = self.tfTitle.text;
    if (title == nil || [title isEqualToString:@""] ) {
        
        [self showAlert:@"Please input the title"];
        [self.tfTitle becomeFirstResponder];
        return NO;
    }
    if (![AppUtils isValidContentWithSymbolChar:title] || ![AppUtils validStringAfterTrimWhitespace:title]) {
        [self showAlert:@"Please input a valid title"];
        [self.tfTitle becomeFirstResponder];
        return NO;

    }
    
    NSString *content = self.tvContent.text;
    if (content == nil || [content isEqualToString:@""] ) {
        
        [self showAlert:@"Please input the content"];
        [self.tvContent becomeFirstResponder];
        return NO;
    }
    if (![AppUtils isValidContentWithSymbolChar:content] || ![AppUtils validStringAfterTrimWhitespace:content]) {
        [self showAlert:@"Please input a valid content"];
        [self.tvContent becomeFirstResponder];

        return NO;

    }
    
    return YES;
}

//On next and submit button
- (IBAction)onNextBtn:(UIButton *)sender {
    
//    if (IS_SIMULATION) {
//        postedPoem = self.poemInfoEditting;
//        [self performSegueWithIdentifier:@"SEGUE_ADD_ENG_VERSION" sender:self];
//        return;
//    }
    

    if (! [self validateFormUpload]) {
        return;
    }
    
    [self createUploadParam];
    
    [_dicPostPoemParam setValue:POEM_STATUS_PUBLIC forKey:@"status_id"];//Publish
    
    if (languageindex != 0) {
        [self performSegueWithIdentifier:@"SEGUE_ADD_ENG_VERSION" sender:self];
    }else{
       //send to Service
        
        if ([self isCreatingNewPoem]) {
            [self sendHttpPostRequest:SERVICE_NEW_POEM body:_dicPostPoemParam andRequestId:1];
        }else{
            [self sendHttpPostRequest:[SERVICE_EDIT_POEM stringByAppendingString:self.poemInfoEditting.poemid] body:_dicPostPoemParam andRequestId:1];
        }
        
    }
}

- (IBAction)onSaveBtn:(id)sender {
    if (! [self validateFormUpload]) {
        return;
    }
    
    [self createUploadParam];
    [_dicPostPoemParam setValue:POEM_STATUS_DRAFT forKey:@"status_id"];//Draft
    
    
    if ([self isCreatingNewPoem]) {
        [self sendHttpPostRequest:SERVICE_NEW_POEM body:_dicPostPoemParam andRequestId: 2];
    }else{
        //Edit
        [self sendHttpPostRequest:[SERVICE_EDIT_POEM stringByAppendingString:self.poemInfoEditting.poemid] body:_dicPostPoemParam andRequestId:2];
        
    }
}

#pragma mark- Keyboard Handeling.

- (void) keyboardWillShow:(NSNotification *)note {
    
    if (_tvContent.isFirstResponder) {
        if (self.view.frame.origin.y >= 0){
    
            [self setViewMovedUp:YES withHeigh:120];
        }
    }
    
    self.thumbnailImageView.frame = CGRectMake(-100, self.thumbnailImageView.frame.origin.y, self.thumbnailImageView.frame.size.width, self.thumbnailImageView.frame.size.height);
}

- (void) keyboardDidHide:(NSNotification *)note {
    if (_tvContent.isFirstResponder) {
        if (self.view.frame.origin.y < 0){

            [self setViewMovedUp:NO withHeigh:120];
        }
    }
    
    self.thumbnailImageView.frame = CGRectMake(27, self.thumbnailImageView.frame.origin.y, self.thumbnailImageView.frame.size.width, self.thumbnailImageView.frame.size.height);

}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.tfContent.placeholder = @"";
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        self.tfContent.placeholder = @"Poem...";
    }
    
    return YES;
}


- (BOOL)textView:(UITextView *)tView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    CGRect textRect = [tView.layoutManager usedRectForTextContainer:tView.textContainer];
    CGFloat sizeAdjustment = tView.font.lineHeight * [UIScreen mainScreen].scale;
    
    if (textRect.size.height >= tView.frame.size.height - sizeAdjustment) {
        if ([text isEqualToString:@"\n"]) {
            [UIView animateWithDuration:0.2 animations:^{
                [tView setContentOffset:CGPointMake(tView.contentOffset.x, tView.contentOffset.y + sizeAdjustment)];
            }];
        }
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.tfTitle) {
     
        NSString *text = textField.text;
        text = [text stringByReplacingCharactersInRange:range withString:string];
        if (text.length > 120) {
            return NO;
        }

    }
    
    return YES;
}


#pragma mark - UIActionSheetDelegate


-(void) showActionSheetCameraSourceWithDelegate:(id) delegate{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:delegate
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"From Camera", @"From Library",nil];
    
    [actionSheet showInView:self.view];
}


- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
    if (IS_SIMULATION){
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }else{
        picker.sourceType = buttonIndex == 0 ? UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypePhotoLibrary;
    }
    picker.allowsEditing = YES;
    
	[self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    float w = 640 ,h = 640;
    if (image.size.width > image.size.height) {
        h = w*image.size.height/image.size.width;
    }else{
        w = h*image.size.width/image.size.height;
    }
    
    CGSize newSize = CGSizeMake(w, h);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    _attachImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_LOCATION_SELECT"]){
        LocationViewCtrl *vc = segue.destinationViewController;
        vc.parentController = self;
    }else if([segue.identifier isEqualToString:@"SEGUE_ADD_ENG_VERSION"] ){
        
        EnglishVersionViewController *vc = segue.destinationViewController;
        vc.orginalParams = _dicPostPoemParam;
        
        if (![self isCreatingNewPoem]) {
            vc.poemInfoEditting = self.poemInfoEditting;
        }
    }else if ([segue.identifier isEqualToString:@"SEGUE_SHARE_VIEW"]){
        
        ShareViewController *vc = segue.destinationViewController;
        vc.poemInfo = postedPoem;
    }
    else if([segue.identifier isEqualToString:@"SEQUE_ADD_IMAGE"]){
        AddImageViewCtrl *vc = segue.destinationViewController;
        vc.delegete = self;
        
        //setup input image
        if (_attachImage) {
            vc.inputImage = _attachImage;
        }else{
            if (self.poemInfoEditting.photos) {
                NSArray *photosarr = self.poemInfoEditting.photos;
                if (photosarr.count > 0) {
                    NSString *url = photosarr[0];
                    if (url) {
                        vc.inputImageUrl = url;
                    }
                }
            }
        }
        
        //set input caption
        if (_attachImageCaption) {
            vc.inputCaption = _attachImageCaption;
        }else{
            if (self.poemInfoEditting.photoCaptions) {
                NSArray *captionsarr = self.poemInfoEditting.photoCaptions;
                if (captionsarr.count > 0) {
                    NSString *caption = captionsarr[0];
                    if (caption) {
                        vc.inputCaption = caption;
                    }
                }
            }
        }
    }
}

//Call from location selector
-(void)changeLocation:(LandmarkInfo *)locationItem{
//    NSLog(@"Changed to location id %@", locationid);
    _locationId = [NSString stringWithFormat:@"%d", locationItem.locatinId];
    self.locationNameLabel.text = locationItem.name;
}


-(void) moveToSubmitListSubmitted:(BOOL) submitted {

    UITabBarController *tabbarvc = (UITabBarController *)APP_DELEGATE.window.rootViewController;
    UINavigationController *navvc = tabbarvc.viewControllers[E_TAB_MY_ENTRIES];
    NSArray *arr = navvc.viewControllers;
    if (arr && arr.count > 0) {
        MyEntriesViewCtrl *vc = (MyEntriesViewCtrl *)arr[0];
        if (submitted) {
            vc.willOpenSubmitAfterShare = YES;
        }else{
            vc.willOpenDraftList = YES;
        }
    }
    [navvc popToRootViewControllerAnimated:NO];
    tabbarvc.selectedIndex = E_TAB_MY_ENTRIES;
}

#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    SBJSON *jsonParser = [[SBJSON alloc] init];
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSDictionary *dic = [jsonParser objectWithString:jsonString];
    
    if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
    {
        id errordic = [dic valueForKey:@"error"];
        
        if (errordic != nil) {
            [self showAlert:@"Can not submit new poem"];
        }else{
            
            if (requestId == 2) { //Saving
                //move to myentries tab
                [self moveToSubmitListSubmitted:NO];

                [self showAlert:@"Your poem has been saved to draft successfully"];

            }else{//Posted
            
                if ([self isCreatingNewPoem]) {
                    postedPoem = [CommonParser parsePoemInfo:dic];
                    if (postedPoem != nil) {
                        
                        [self performSegueWithIdentifier:@"SEGUE_SHARE_VIEW" sender:self];
                    }else{
                        [self moveToSubmitListSubmitted:NO];
                    }
                }else{
                    [self.navigationController popViewControllerAnimated:YES];
                }

                [self showAlert:@"Your poem has been submitted successfully"];
            }
            
            //[self resetSessionPostNewPoem];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_POSTED_NEW_POEM object:nil];
        }
    }else{
        if ([self isCreatingNewPoem]) {

            [self showAlert:@"Can not submit new poem"];
        }else{
            [self showAlert:@"Can not update poem"];

        }
    }
}


-(void) resetSessionPostNewPoem{
    NSLog(@"resetSessionPostNewPoem");
    
    [_dicPostPoemParam removeAllObjects];
    self.tfContent.placeholder = @"Poem...";
    self.tvContent.text = @"";
    self.tfTitle.text = @"";
    
    _attachImage = nil;
    _attachImageCaption = nil;
    //[self.attachImageBtn setBackgroundImage:[UIImage imageNamed:@"btn_image_attach.png"] forState:UIControlStateNormal];

    self.thumbnailImageView.hidden = YES;
    self.thumnailImage.image = nil;

    self.poemInfoEditting = nil;
    self.locationNameLabel.text = @"";
    _locationId = nil;
    
    categoryindex = 0;
    languageindex = 0;

    for (UIButton *btn in _arrayOfCategoryBtn) {
        btn.selected = NO;
    }
    ((UIButton*)_arrayOfCategoryBtn[1]).selected = YES;

    for (UIButton *btn in _arrayOfLanguageBtn) {
        btn.selected = NO;
    }
    ((UIButton*)_arrayOfLanguageBtn[0]).selected = YES;
}


- (void) returnSelectedImage:(UIImage*)img caption:(NSString*)caption didDeleted:(BOOL)deleted{
//    if (img != nil) {
//        //[self.attachImageBtn setBackgroundImage:img forState:UIControlStateNormal];
//
//    }else{
//        //[self.attachImageBtn setBackgroundImage:[UIImage imageNamed:@"btn_image_attach.png"] forState:UIControlStateNormal];
//    }
    
    self.thumnailImage.image = img;
    self.thumbnailImageView.hidden = img == nil;
    _attachImageWillDelete = NO;
    
    if (img == nil && deleted == YES) {
        if (![self isCreatingNewPoem]) {
            _attachImageWillDelete = YES;
        }
    }
    
    _attachImage = img;
    _attachImageCaption = caption;
}

- (IBAction)onCloseThumbnailBtn:(id)sender {
    //remove attachment image
    
    _attachImage = nil;
    _attachImageCaption = nil;
    _attachImageWillDelete = NO;
    
    if (![self isCreatingNewPoem]) {
        _attachImageWillDelete = YES;
    }
    
    
    self.thumbnailImageView.hidden = YES;
}

@end
