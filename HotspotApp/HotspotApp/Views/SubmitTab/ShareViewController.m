//
//  ShareViewController.m
//  HotspotApp
//
//  Created by hmchau on 7/27/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ShareViewController.h"
#import "FbSocialNetwork.h"
#import "TWSocialNetwork.h"
#import "IGSocialNetwork.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "MyEntriesViewCtrl.h"
#import "MainTabbarCtrl.h"

#import <MessageUI/MessageUI.h>


@interface ShareViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation ShareViewController{
    
    IGSocialNetwork *_instagramSocial;

}

@synthesize poemInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //setup Create new button
    UIBarButtonItem *createnewbtn = self.navigationItem.leftBarButtonItem;
    [createnewbtn setTitleTextAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:17], NSForegroundColorAttributeName: [UIColor blueColor]} forState:UIControlStateNormal];
    
    //setup font
    self.thankYouLabel.font = [UIFont fontWithName:@"Futura-Bold" size:19];

    self.hiddenCacheImageView.hidden = YES;
    
    //try to load image to cached before share
    if (self.poemInfo) {
        NSString *label = [NSString stringWithFormat:@"%@,%@", [AppConfiguration sharedInstance].userEmail, self.poemInfo.poemid ];
        [APP_DELEGATE GALTrackerInCategory:@"Poem" withAction:@"Submit" withLabel:label];
        
        
        //Dont need to preload image for sharing any more -> dont share image
        if (self.poemInfo.photos && self.poemInfo.photos.count >0) {
            
            MBProgressHUD* hud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText=@"Loading...";

            [self.hiddenCacheImageView setImageWithURL:[NSURL URLWithString:self.poemInfo.photos[0]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
        
        //hmchau add new
        if ( [self.poemInfo.category_id isEqualToString:POEM_CATEGORY_FEATURED]) {
            self.poemInfo.isFeaturedPoem = YES;
            
        }else{
            self.poemInfo.isFeaturedPoem = NO;
            self.poemInfo.urlshare = URL_DOWNLOAD_APP;
        }
    }
}

- (IBAction)onCreateNewBtn:(id)sender {
    //Back button
    
    UITabBarController *tabvc = self.tabBarController;
    UINavigationController *navvc = tabvc.viewControllers[E_TAB_MY_ENTRIES];
    NSArray *arr = navvc.viewControllers;
    if (arr && arr.count > 0) {
        MyEntriesViewCtrl *vc = (MyEntriesViewCtrl *)arr[0];
        vc.willOpenSubmitAfterShare = YES;
    }
    [navvc popToRootViewControllerAnimated:NO];
    tabvc.selectedIndex = E_TAB_MY_ENTRIES;
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)onFbShareBtn:(id)sender {
    [self doPostWithButton:0];
}

- (IBAction)onTWShareBtn:(id)sender {
    [self doPostWithButton:1];

}

- (IBAction)onIGShareBtn:(id)sender {
    [self doPostWithButton:2];
}


- (void)doPostWithButton: (NSInteger)buttonIndex{
    if (! self.poemInfo) {
        return;
    }
    
    [APP_DELEGATE GALTrackerInCategory:@"Poem" withAction:@"Share" withLabel:self.poemInfo.poemid];

    
    UIImage *sharedImage;
    if (self.poemInfo.photos && self.poemInfo.photos.count > 0){
        NSString *imageurl = self.poemInfo.photos[0];
        
        SDImageCache *imageCache = [SDImageCache sharedImageCache];
        sharedImage = [imageCache imageFromMemoryCacheForKey:imageurl];
    }
    
    switch (buttonIndex) {
        case 0:
        {
            FbSocialNetwork *fb = [[FbSocialNetwork alloc] init];
            fb.parentController = self;
            
            NSString *formatBody = @"Your friend has shared \"%@\" by %@ with you!\nTo read the poem, download the Text in the City app. Submit your own place-based poem and stand a chance to win up to $1,500 and be showcased in a featured collection!";

            NSString *body = [NSString stringWithFormat:formatBody, self.poemInfo.title, self.poemInfo.author];
            
            body = [body stringByAppendingFormat:@"\n\nApple Store\n%@", URL_DOWNLOAD_APP];
            
            body = [body stringByAppendingFormat:@"\n\nGoogle Playstore\n%@", URL_DOWNLOAD_APP_ANDROID];
            
            if ([fb shareContent:body image:sharedImage url:nil]){
            
            }else{
                
                [self showAlert:@"Facebook account is not available on your device. Please check it in the Settings"];
            }
        }
            
            break;
            
        case 1:
        {
            TWSocialNetwork *fb = [[TWSocialNetwork alloc] init];
            fb.parentController = self;
            NSString *title = self.poemInfo.title;

            NSString *formatBody = @"To read \"%@\" by %@, download the mobile app!";
            NSString *body = [NSString stringWithFormat:formatBody, title, self.poemInfo.author];
            
            NSString *footer = @"";
            footer = [footer stringByAppendingFormat:@"\n%@", URL_DOWNLOAD_APP];
            footer = [footer stringByAppendingFormat:@"\n%@", URL_DOWNLOAD_APP_ANDROID];
            //Footer size 48 character
            //Remain still 92 char
            
            if (body.length > 92) {
                title = [title substringToIndex:title.length - 4];
                title = [title stringByAppendingString:@"..."];
                body = [NSString stringWithFormat:formatBody, title, self.poemInfo.author];
                while (body.length > 92) {
                    title = [title substringToIndex:title.length - 4];
                    title = [title stringByAppendingString:@"..."];
                    body = [NSString stringWithFormat:formatBody, title, self.poemInfo.author];
                }
            }
            
            if ([fb shareContent:[body stringByAppendingString:footer] image:nil url:nil]){
            }else{
                
                [self showAlert:@"Twitter account is not available on your device. Please check it in the Settings"];
            }
            
        }
            break;
            
            
        case 2://Share email
        {
            [self openEmailComposer:sharedImage];
        }
            break;
            
        default:
            
            //[self showOnprogressMsg];
            break;
    }
}

- (void)openEmailComposer:(UIImage *)image
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if( picker == nil ){
        //[self showAlert:@"You will need to setup a mail account on your device before you can send mail"];
        return;
    }
    
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"A Poem from Text In The City"];
    
    NSString *formatBody = @"Your friend has shared \"%@\" by %@ with you!\nTo read the poem, download the Text in the City app. Submit your own place-based poem and stand a chance to win up to $1,500 and be showcased in a featured collection!";

    NSString *body = [NSString stringWithFormat:formatBody, self.poemInfo.title, self.poemInfo.author];
    
    body = [body stringByAppendingFormat:@"\n\nApple Store\n%@", URL_DOWNLOAD_APP];
    
    body = [body stringByAppendingFormat:@"\n\nGoogle Playstore\n%@", URL_DOWNLOAD_APP_ANDROID];

    // This is not an HTML formatted email
    [picker setMessageBody:body isHTML:NO];
    
    if (image) {
        // Create NSData object as PNG image data from camera image
        NSData *data = UIImagePNGRepresentation(image);
        
        // Attach image data to the email
        // 'CameraImage.png' is the file name that will be attached to the email
        [picker addAttachmentData:data mimeType:@"image/png" fileName:@"PoemImage"];
    }
    
    // Show email view
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    NSLog(@"didFinishWithResult %@", error);
    [controller dismissViewControllerAnimated:YES completion:^{
        
    }];
}


@end
