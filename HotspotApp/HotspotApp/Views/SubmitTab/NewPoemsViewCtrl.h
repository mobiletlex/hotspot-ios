//
//  SubmitViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@class PoemInfo;

@interface NewPoemsViewCtrl : BaseViewController

- (IBAction)onImageAttachBtn:(id)sender;

- (IBAction)onLanguageSelectBtn:(UIButton *)sender;

- (IBAction)onCategorySelectBtn:(UIButton *)sender;

- (IBAction)onDoneKeyboardAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIToolbar *actionToolbar;

@property (strong, nonatomic) PoemInfo * poemInfoEditting;

@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *attachImageBtn;

@property (weak, nonatomic) IBOutlet UIView *thumbnailImageView;
- (IBAction)onCloseThumbnailBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *thumnailImage;



@end
