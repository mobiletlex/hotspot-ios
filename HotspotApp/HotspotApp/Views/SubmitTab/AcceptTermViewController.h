//
//  AcceptTermViewController.h
//  HotspotApp
//
//  Created by hmchau on 8/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface AcceptTermViewController : BaseViewController

@property (nonatomic,retain) UIViewController *parentController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)onBackBtn:(id)sender;
- (IBAction)onAcceptBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@end
