//
//  AddImageViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 8/3/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "AddImageViewCtrl.h"
#import "NewPoemsViewCtrl.h"
#import "UIImageView+WebCache.h"

@interface AddImageViewCtrl (){
        UIImage *_attachImage;
    __weak IBOutlet UIImageView *imgImageView;
    __weak IBOutlet UITextField *tfCaption;
    __weak IBOutlet UIBarButtonItem *btnDone;
}

@end

@implementation AddImageViewCtrl{
    
    BOOL imageChanged;
    BOOL imageDeleted;
}

@synthesize inputImage;
@synthesize inputCaption;
@synthesize inputImageUrl;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)onAddChangeBtn:(id)sender {
     [self showActionSheetCameraSourceWithDelegate:self];
}

- (IBAction)onRemoveBtn:(id)sender {
    _attachImage = nil;
    tfCaption.text = @"";
    imgImageView.image = nil;
    
    imageChanged = YES;
    imageDeleted = YES;
}



- (IBAction)onDoneBtn:(id)sender {
//    if(_attachImage != nil && [tfCaption.text isEqualToString:@""]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please input Caption for the image" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    else{
//        [self.delegete returnSelectedImage:_attachImage caption:tfCaption.text];
//        [self.navigationController popViewControllerAnimated:YES];
//    }

    if (imageChanged) {
        [self.delegete returnSelectedImage:_attachImage caption:tfCaption.text didDeleted:imageDeleted];
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [btnDone setTitleTextAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:17], NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    
    UIBarButtonItem *createnewbtn = self.navigationItem.leftBarButtonItem;
    [createnewbtn setTitleTextAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:17], NSForegroundColorAttributeName: [UIColor blueColor]} forState:UIControlStateNormal];

    
    if (IS_IPHONE_4) {
        [self.view viewWithTag:10012].hidden = YES;
    }
    
    
    if (self.inputImage) {
        imgImageView.image = self.inputImage;
    }else{
        if (self.inputImageUrl != nil) {
            [imgImageView setImageWithURL:[NSURL URLWithString:self.inputImageUrl] ];
        }
    }
    
    tfCaption.text = self.inputCaption;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


#pragma mark - UIActionSheetDelegate


-(void) showActionSheetCameraSourceWithDelegate:(id) delegate{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:delegate
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"From Camera", @"From Library",nil];
    
    [actionSheet showInView:self.view];
}


- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
    if (IS_SIMULATION){
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }else{
        picker.sourceType = buttonIndex == 0 ? UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypePhotoLibrary;
    }
    picker.allowsEditing = YES;
    
	[self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    imageChanged = YES;
    imageDeleted = NO;
    
    float w = 640 ,h = 640;
    if (image.size.width > image.size.height) {
        h = w*image.size.height/image.size.width;
    }else{
        w = h*image.size.width/image.size.height;
    }
    
    CGSize newSize = CGSizeMake(w, h);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    _attachImage = UIGraphicsGetImageFromCurrentImageContext();
    imgImageView.image = _attachImage;
    UIGraphicsEndImageContext();
}
#pragma mark- Keyboard Handeling.

- (void) keyboardWillShow:(NSNotification *)note {
    
    if (tfCaption.isFirstResponder) {
        if (self.view.frame.origin.y >= 0){
            [self setViewMovedUp:YES withHeigh:110];
        }
    }
}

- (void) keyboardDidHide:(NSNotification *)note {
    if (tfCaption.isFirstResponder) {
        [self setViewMovedUp:NO withHeigh:110];
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
//    tfCaption.placeholder = @"";
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
//    if (textView.text.length == 0) {
//        self.tfContent.placeholder = @"Poem...";
//    }
    
    return YES;
}
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCancelButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
