//
//  LoginViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LoginViewCtrl.h"
#import "CommonUtils.h"
#import "ASIHTTPRequest.h"
#import "SBJSON.h"
#import "AcceptTermViewController.h"
#import "WEPopoverController.h"
#import "ResetPWViewController.h"

@interface LoginViewCtrl ()

@end

@implementation LoginViewCtrl{
    
    BOOL alreadyAcceptTermOfUse;
    WEPopoverController *_popoverController;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tfEmail.delegate = self;
    _tfPass.delegate = self;
    
    if([self isModalPresenting]){
        
        UIBarButtonItem *cancelBtn = self.navigationItem.rightBarButtonItem;
        [cancelBtn setTitleTextAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:17], NSForegroundColorAttributeName: [UIColor blueColor]} forState:UIControlStateNormal];

    }else{
        
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    
    if (! IS_SIMULATION) {
        _tfEmail.text = @"";
        _tfPass.text = @"";
    }
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}


-(BOOL) isModalPresenting{
    if (self.tabBarController == nil) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_ACCEPT_TERM"]) {
        UINavigationController *nav = segue.destinationViewController;
        AcceptTermViewController *vc = nav.viewControllers[0];
        
        vc.parentController = self;
    }
}


- (IBAction)onCancelLognNavBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSignUpBtn:(id)sender {
    
    if(alreadyAcceptTermOfUse || IS_SIMULATION){
        [self performSegueWithIdentifier:@"SEGUE_SIGNUP_VIEW" sender:self];
    }else{
        [self performSegueWithIdentifier:@"SEGUE_ACCEPT_TERM" sender:self];
    }
}

- (IBAction)onForgetPasswordBtn:(id)sender {
    
    UIStoryboard *storyboard = self.storyboard;
    ResetPWViewController *resetpwView = [storyboard instantiateViewControllerWithIdentifier:@"ResetPWViewController"];
    resetpwView.parentController = self;
    
    int popupheight = 330;
    
    resetpwView.contentSizeForViewInPopover = CGSizeMake(297 , 270 );
    
    if (!_popoverController) {
        _popoverController = [[WEPopoverController alloc] initWithContentViewController:resetpwView];
    }
    else{
        _popoverController.contentViewController = resetpwView;
    }
    
    CGPoint point = self.view.center;
    CGRect rect = CGRectMake(point.x, point.y - popupheight/2 + 4 , 2, 2);
    
    [_popoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void) dismissPopoverViewController{
    if (_popoverController) {
        [_popoverController dismissPopoverAnimated:YES];
    }
}


- (IBAction)btnSubmitTap:(id)sender {
    
    NSString *email = self.tfEmail.text;
    if (email == nil || [email isEqualToString:@""]) {
        [self showAlert:@"Please input a valid email address"];
        [self.tfEmail becomeFirstResponder];
        return;
    }
    
    if (![CommonUtils isValidEmail:email]) {
        [self showAlert:@"Your email is incorrect. Please check again"];
        [self.tfEmail becomeFirstResponder];

        return;
    }
    
    if (self.tfPass.text == nil || [self.tfPass.text isEqualToString:@""]){
        [self showAlert:@"Please input your password"];
        [self.tfPass becomeFirstResponder];

        return;
    }
    
    NSString *password = self.tfPass.text;
    
    // Request Service....
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:email forKey:@"email"];
    [dic setValue:password forKey:@"password"];

    [self sendHttpPostRequest:SERVICE_LOGIN body:dic andRequestId:1];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - TermOfUse
-(void) onAcceptTermOfUse:(NSNumber *) number{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        BOOL accept = [number boolValue];
        if (accept) {
            alreadyAcceptTermOfUse = YES;
            [self performSegueWithIdentifier:@"SEGUE_SIGNUP_VIEW" sender:self];
        }
    }];
}

#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    //NSLog(@"didReceiveData");
    
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if ([jsonString isEqualToString:@""])
    {

    }else{
        
        SBJSON *jsonParser = [[SBJSON alloc] init];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSDictionary *dic = [jsonParser objectWithString:jsonString];
        
        if (dic != nil) {
            
            NSString *error = [dic valueForKey:@"error"];
            if (error != nil) {
                
                [self showAlert:error];
                return;
            }
            
//            NSDictionary *error = [dic valueForKey:@"error"];
//            if (error != nil) {
//                NSArray *array = error.allKeys;
//                if (array && array.count > 0) {
//                    NSArray *msgarr = [error valueForKey:array[0]];
//                    if (msgarr && msgarr.count > 0) {
//                        [self showAlert:[NSString stringWithFormat:@"%@", msgarr[0] ]];
//                    }else{
//                        [self showAlert:@"Can not sign up, unknow error"];
//                    }
//                    
//                }else{
//                    [self showAlert:@"Can not sign up, unknow error"];
//                }
//            }

            NSString *key = [dic valueForKey:@"access_token"];
            if (key != nil) {
                [AppConfiguration sharedInstance].sessionToken = key;
                [AppConfiguration sharedInstance].userEmail = self.tfEmail.text;
                [AppConfiguration sharedInstance].lastName = [dic valueForKey:@"last_name"];
                [AppConfiguration sharedInstance].fristName = [dic valueForKey:@"first_name"];

                [[AppConfiguration sharedInstance] saveConfig];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                [APP_DELEGATE appLogined];
            }else{
                [self showAlert:@"Can not sign up, unknow error"];
            }
        
            return;
        }
    }
    
    [self showNetworkErrMsg];

}


@end
