//
//  LocationViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LocationViewCtrl.h"
#import "LandmarkParser.h"
#import "LandmarkManager.h"
#import "AppUtils.h"
#import "NearbyLocationsViewController.h"

@interface LocationViewCtrl () <UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate, UITextFieldDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation LocationViewCtrl{
    
    NSMutableArray *_locationList;
    NSMutableArray *_filteredArray;
    BOOL listLoaded;
    NSString *_selectedNewZoneId;
}

@synthesize parentController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_searchBar setPinkBorder];
    UITextField *txfSearchField = [_searchBar valueForKey:@"_searchField"];
    if(txfSearchField){
        txfSearchField.clearButtonMode = UITextFieldViewModeNever;
        txfSearchField.clearsOnBeginEditing = NO;

    }
    
    _searchBar.delegate = self;
    
    
    _locationList = [[NSMutableArray alloc] init];
    _filteredArray = [[NSMutableArray alloc] init];
 
    //get location nearby
    CLLocationCoordinate2D currenlocation = APP_DELEGATE.currentUserLocation;
    NSString *url = [SERVICE_LOCATION_NEAR_BY stringByAppendingFormat:@"?lat=%f&lon=%f",currenlocation.latitude, currenlocation.longitude ];
    
    [self sendHttpGetRequest:url andRequestId:1];
    
    self.createLocatonView.hidden = YES;
}



#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (listLoaded) {
//        if ([self containSearchItem]) {
//            return _filteredArray.count + 1;
//        }
        return _filteredArray.count;
    }else{
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"LocationCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] init];
        //cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    UILabel *l1 = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *l2 = (UILabel *)[cell.contentView viewWithTag:2];
    l1.text = @"";
    l2.text = @"";
    
//    UIButton * btn = (UIButton *)[cell.contentView viewWithTag:100];
//    if (btn) {
//        [btn removeFromSuperview];
//    }
//    
//    BOOL containsearchtext = [self containSearchItem];
//    if (indexPath.row == 0) {
//        if (containsearchtext) {
//            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//            //l1.text = @"Create new location";
//            //l2.text = [NSString stringWithFormat:@"With name : %@", self.searchBar.text ];
//            
//            UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 10, 200, 35)];
//            [btn setBackgroundImage:[UIImage imageNamed:@"btn_red_colored_long.png"] forState:UIControlStateNormal];
//            [btn setTitle:@"Create a new location" forState:UIControlStateNormal];
//            btn.tag = 100;
//            btn.userInteractionEnabled = NO;
//            btn.titleLabel.font = [UIFont systemFontOfSize:15.0];
//            
//            [cell.contentView addSubview:btn];
//            
//            return cell;
//        }
//    }
    
    
    //int subindex = containsearchtext == YES ? 1 : 0;
    LandmarkInfo *item = _filteredArray[indexPath.row ];

    l1.text = item.name;
    l2.text = item.address;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (indexPath.row == 0) {
//        if ([self containSearchItem]) {
//            //Creat new location
//            NSLog(@"Creat new location");
//            
//            [self.searchBar resignFirstResponder];
//            [self showNewLocatonForm];
//            
//            //[self processCreateNewLocationWithName:self.searchBar.text];
//            return;
//        }
//    }
    
    int subindex = [self containSearchItem] == YES ? 1 : 0;
    LandmarkInfo *item = _filteredArray[indexPath.row - subindex];
    
    //check valid position
    NSString *locationname = [[LandmarkManager shareInstance] zoneNameById:item.zoneId];
    if (locationname == nil || [locationname isEqualToString:@""] || [locationname isEqualToString:@"Unknow"]) {
        
        [self showAlert:@"This location isn't belong to any zone, please select another one"];
        return;
    }
    
    if (self.parentController) {
        [self.parentController performSelector:@selector(changeLocation:) withObject:item];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(BOOL) containSearchItem{
//    NSString *searchtext = self.searchBar.text;
//    if (searchtext == nil || searchtext.length == 0) {
//        
//        return NO;
//    }
    
    return NO;
}

-(void) processCreateNewLocationWithName:(NSString *) locationname{
    
    [self.searchBar resignFirstResponder];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Address for location : \n%@", locationname] message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
}

-(void) startCreateNewLocation:(NSString *) address{
    NSString *nearestzoneid = [[LandmarkManager shareInstance] getZoneIdNearByLocation:APP_DELEGATE.currentUserLocation];
    if (nearestzoneid == nil) {
        [self showAlert:@"Can not find any zone id"];
        return;
    }
    
    NSString *locationname = self.searchBar.text;
    
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    [body setValue:nearestzoneid forKey:@"zone_id"];
    [body setValue:locationname forKey:@"name"];
    [body setValue:address forKey:@"address"];
    [body setValue:@"" forKey:@"fun_fact"];
    [body setValue:[NSString stringWithFormat:@"%f", APP_DELEGATE.currentUserLocation.latitude] forKey:@"lat"];
    [body setValue:[NSString stringWithFormat:@"%f", APP_DELEGATE.currentUserLocation.longitude] forKey:@"lon"];
    
    [self sendHttpPostRequest:SERVICE_REQUEST_NEW_LOCATION body:body andRequestId:2];
}

#pragma mark - Networking
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    //NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (data) {
        switch (requestId) {
            case 1:
                listLoaded = YES;
                _locationList = [LandmarkParser parse:data];
                if (_locationList) {
                    //reload data
                    [_filteredArray removeAllObjects];
                    [_filteredArray addObjectsFromArray:_locationList];
                    
                    [self.tableView reloadData];
                }else{
                    [self showNetworkErrMsg];
                }
                break;
                
            case 2:
            {
                LandmarkInfo *item = [LandmarkParser parseLocationItem:data];
                if(item == nil){
                    [self showAlert:@"Can not create new location"];
                }else{
                    [APP_DELEGATE GALTrackerInCategory:@"Location" withAction:@"Submit" withLabel:item.name];
                    //Send back to create new poem
                    
                    if (self.parentController) {
                        [self.parentController performSelector:@selector(changeLocation:) withObject: item];
                    }
                    [self showAlert:@"Your location has been added successfully"];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
                break;
                
            default:
                break;
        }
        
    }else{
        [self showNetworkErrMsg];
    }
}

#pragma mark - UISearchBarDelegete
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    self.createLocatonView.hidden = YES;
    
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self.searchBar resignFirstResponder];
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchBarText = _searchBar.text;
    [_filteredArray removeAllObjects];
    
    if(searchBarText.length>0){
        
        for (int i = 0 ; i < _locationList.count ; i++ ) {
            
            LandmarkInfo *itemlandmark = _locationList[i];
            
            NSString *item = itemlandmark.name;
            NSRange titleResultsRange = [item rangeOfString:searchBarText options:NSCaseInsensitiveSearch];
            
            if (titleResultsRange.length > 0){
                [_filteredArray addObject:_locationList[i]];
            }
        }
        
    }else{
        for (int i = 0  ; i < _locationList.count ; i ++ ) {
            [_filteredArray addObject:_locationList[i] ];
        }
    }
    
    [self.tableView reloadData];
    
    if ([searchText length] == 0) {
        [self performSelector:@selector(hideKeyboardWithSearchBar:) withObject:searchBar afterDelay:0];
    }
}


- (void)hideKeyboardWithSearchBar:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

//#pragma mark - UIAlertViewDelegate
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    
//    if (buttonIndex != alertView.cancelButtonIndex) {
//        UITextField * alertTextField = [alertView textFieldAtIndex:0];
//        //NSLog(@"alerttextfiled - %@",alertTextField.text);
//        
//        //[self startCreateNewLocation:alertTextField.text];
//    }
//}

- (IBAction)onCloseSearchBtn:(id)sender {
    self.searchBar.text = @"";

    [self.searchBar resignFirstResponder];

    [self searchBar:self.searchBar textDidChange:@""];
}

- (IBAction)onCancelCreateNew:(id)sender {
    
    self.createLocatonView.hidden = YES;
}


-(BOOL) validateLocationInfo{
    
    NSString *locationName = self.tfLocationName.text;
    NSString *locationAddress = self.tfLocationAddress.text;

    BOOL invalidlocation = (locationName == nil || [locationName isEqualToString:@""] || ![AppUtils validStringAfterTrimWhitespace:locationName]);
    
    BOOL invalidAddress = (locationAddress == nil || [locationAddress isEqualToString:@""] || ![AppUtils validStringAfterTrimWhitespace:locationAddress]);
    
    if (invalidAddress && invalidlocation) {
        
        [self showAlert:@"Please fill in Location name, Address to submit a new location."];
        [self.tfLocationName becomeFirstResponder];
        return NO;

    }
    
    if (invalidlocation) {
        [self showAlert:@"Please fill in Location name to submit a new location."];
        [self.tfLocationName becomeFirstResponder];
        return NO;
    }
    
    if (invalidAddress) {
        [self showAlert:@"Please fill in Address to submit a new location."];
        [self.tfLocationAddress becomeFirstResponder];
        return NO;
    }
    

    //hmchau : remove ,optional
//    NSString *locationFunfact = self.tfLocationFunfact.text;
//    if (locationFunfact == nil || [locationFunfact isEqualToString:@""] || ![AppUtils validStringAfterTrimWhitespace:locationFunfact]) {
//        //[self showAlert:@"Please input valid Location fun fact"];
//        [self.tfLocationFunfact becomeFirstResponder];
//        return NO;
//    }

    return YES;
}

- (IBAction)onCreateNewLocation:(id)sender {
    if (! [CLLocationManager locationServicesEnabled]){
        [self showAlert:@"Can not detect your location, please turn on and allow access Location service"];
        return;
    }
    
//    NSString *nearestzoneid = [[LandmarkManager shareInstance] getZoneIdNearByLocation:APP_DELEGATE.currentUserLocation];
//    if (nearestzoneid == nil) {
//        [self showAlert:@"Can not detect your location, please check your GPS signal"];
//        return;
//    }
    
    if (_selectedNewZoneId == nil) {
        [self showAlert:@"Please select zone for new location"];
        return;
    }
    
    if (![self validateLocationInfo]) {
        //[self showAlert:@"Please fill in Location name, Address to submit a new location."];
        return;
    }
    
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    [body setValue:_selectedNewZoneId forKey:@"zone_id"];
    [body setValue:self.tfLocationName.text forKey:@"name"];
    [body setValue:self.tfLocationAddress.text forKey:@"address"];
    
    NSString *fact = self.tfLocationFunfact.text;
    if (fact == nil) {
        fact = @"";
    }
    [body setValue:fact forKey:@"fun_fact"];
    [body setValue:[NSString stringWithFormat:@"%f", APP_DELEGATE.currentUserLocation.latitude] forKey:@"lat"];
    [body setValue:[NSString stringWithFormat:@"%f", APP_DELEGATE.currentUserLocation.longitude] forKey:@"lon"];
    
    [self sendHttpPostRequest:SERVICE_REQUEST_NEW_LOCATION body:body andRequestId:2];
}

-(void) showNewLocatonForm{
    
    self.tfLocationAddress.text = @"";
    self.tfLocationFunfact.text = @"";
    self.tfLocationName.text = @"";//self.searchBar.text;
    self.lbSelectedZoneName.text = @"";
    
    self.createLocatonView.hidden = NO;
}

- (IBAction)onCreateNewLocationBtn:(id)sender {
    
    NSLog(@"Creat new location");
    [self.searchBar resignFirstResponder];
    [self showNewLocatonForm];
    
    
    //auto select zone id
    _selectedNewZoneId = [[LandmarkManager shareInstance] getZoneIdNearByLocation:APP_DELEGATE.currentUserLocation];
    if (_selectedNewZoneId != nil) {
        NSString *zoneName = [[LandmarkManager shareInstance] zoneNameById:[_selectedNewZoneId intValue]];
        if ([zoneName isEqualToString:@"Unknow"]) {
            _selectedNewZoneId = nil;
        }else{
            
            self.lbSelectedZoneName.text = zoneName;
        }
    }
}

- (IBAction)onSelectZoneBtn:(id)sender {

    UIActionSheet *actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Central Singapore",@"East Singapore", @"North Singapore",@"North-East Singapore", @"West Singapore", @"Islands" , nil];
    
    [actionsheet showInView:self.view];
}

- (IBAction)onDetectAddress:(id)sender {
    
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (actionSheet.cancelButtonIndex != buttonIndex) {
        switch (buttonIndex) {
                
            case 0:
                //central
                _selectedNewZoneId = @"5";
                break;
            case 1:
                //east
                _selectedNewZoneId = @"8";
                break;
            case 2:
                //north
                _selectedNewZoneId = @"6";
                break;
            case 3:
                //north-East Singapore",
                _selectedNewZoneId = @"4";

                break;
            case 4:
                //@"  > West Singapore",
                _selectedNewZoneId = @"7";
                break;
            case 5:
                //@"  > Islands"
                _selectedNewZoneId = @"10";
                break;
                
            default:
                break;
        }
        
        if(_selectedNewZoneId != nil){
        
            NSString *zoneName = [[LandmarkManager shareInstance] zoneNameById:[_selectedNewZoneId intValue]];
            if ([zoneName isEqualToString:@"Unknow"]) {
                _selectedNewZoneId = nil;
                self.lbSelectedZoneName.text = @"";
            }else{
                
                self.lbSelectedZoneName.text = zoneName;
            }
        }
        
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_NEARBY_LOCATIONS"]) {
        NearbyLocationsViewController * vc = [segue destinationViewController];
        vc.parentController = self;
    }
}

-(void) selectLocation:(NSArray *) locationData{
    self.tfLocationName.text  = locationData[0];
    self.tfLocationAddress.text = locationData[1];
}


@end
