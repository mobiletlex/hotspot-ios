//
//  LocationViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/17/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface LocationViewCtrl : BaseViewController

@property (nonatomic) UIViewController *parentController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)onCloseSearchBtn:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *createLocatonView;
@property (weak, nonatomic) IBOutlet UITextField *tfLocationName;
@property (weak, nonatomic) IBOutlet UITextField *tfLocationAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfLocationFunfact;
@property (weak, nonatomic) IBOutlet UILabel *lbSelectedZoneName;

- (IBAction)onCancelCreateNew:(id)sender;
- (IBAction)onCreateNewLocation:(id)sender;

- (IBAction)onCreateNewLocationBtn:(id)sender;
- (IBAction)onSelectZoneBtn:(id)sender;
- (IBAction)onDetectAddress:(id)sender;

@end
