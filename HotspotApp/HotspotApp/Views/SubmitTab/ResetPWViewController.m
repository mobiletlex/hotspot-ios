//
//  ResetPWViewController.m
//  HotspotApp
//
//  Created by hmchau on 9/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ResetPWViewController.h"
#import "CommonUtils.h"
#import "SBJSON.h"


@interface ResetPWViewController () <UITextFieldDelegate>

@end

@implementation ResetPWViewController

@synthesize parentController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IS_IPHONE_4)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:@"UIKeyboardWillShowNotification"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidHide:)
                                                     name:@"UIKeyboardWillHideNotification"
                                                   object:nil];
    }
}

#define KEYBOARD_MOVE_UP 30

- (void) keyboardWillShow:(NSNotification *)note {
    
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES withHeigh:KEYBOARD_MOVE_UP];
    }
}

- (void) keyboardDidHide:(NSNotification *)note {
    if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO withHeigh:KEYBOARD_MOVE_UP];
    }
}

//- (void) keyboardWillShow:(NSNotification *)note {
//    
//    CGRect rect = APP_DELEGATE.window.frame;
//    
//    if (rect.origin.y >= 0){
//        //[self setViewMovedUp:YES withHeigh:KEYBOARD_MOVE_UP];
//        
//        rect.origin.y -= KEYBOARD_MOVE_UP;
//        APP_DELEGATE.window.frame = rect;
//    }
//}
//
//- (void) keyboardDidHide:(NSNotification *)note {
//    CGRect rect = APP_DELEGATE.window.frame;
//    
//    if (rect.origin.y < 0){
//        //[self setViewMovedUp:NO withHeigh:KEYBOARD_MOVE_UP];
//        rect.origin.y += KEYBOARD_MOVE_UP;
//        APP_DELEGATE.window.frame = rect;
//        
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)onSendBtn:(id)sender {
    [self.tfEmail resignFirstResponder];

    //Email
    if (![CommonUtils isValidEmail:self.tfEmail.text] || self.tfEmail.text.length > 50) {
        [self showAlert:@"Please enter a valid email address"];
        [self.tfEmail becomeFirstResponder];
        return;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:self.tfEmail.text forKey:@"email"];
        
    [self sendHttpPostRequest:SERVICE_RESET_PASSWORD body:dic andRequestId:1];
}

- (IBAction)onCloseBtn:(id)sender {
    if (self.parentController) {
        [self.parentController performSelector:@selector(dismissPopoverViewController)];
    }
}


#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if (data != nil) {
        
        SBJSON *jsonParser = [[SBJSON alloc] init];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSDictionary *dic = [jsonParser objectWithString:jsonString];
        NSString *error = [dic valueForKey:@"error"];
        if (error != nil) {
            
            [self showAlert:error];
            
            [self.tfEmail becomeFirstResponder];

        }else{
            
            [self showAlert:@"Please check your email to reset password page"];
            if (self.parentController) {
                [self.parentController performSelector:@selector(dismissPopoverViewController)];
            }
        }
    }else{
        [self showNetworkErrMsg];
    }
}




@end
