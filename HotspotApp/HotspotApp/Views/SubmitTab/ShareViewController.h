//
//  ShareViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/27/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
#import "PoemInfo.h"
@interface ShareViewController : BaseViewController

@property (nonatomic,retain) PoemInfo * poemInfo;

@property (weak, nonatomic) IBOutlet UILabel *thankYouLabel;

- (IBAction)onFbShareBtn:(id)sender;
- (IBAction)onTWShareBtn:(id)sender;
- (IBAction)onIGShareBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *hiddenCacheImageView;
- (IBAction)onCreateNewBtn:(id)sender;

@end
