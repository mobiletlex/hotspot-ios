//
//  SignUpViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "SignUpViewCtrl.h"
#import "CommonUtils.h"
#import "SBJSON.h"
#import "AcceptTermViewController.h"
#import "AppUtils.h"

@interface SignUpViewCtrl ()
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfPenName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfPhone;
@property (weak, nonatomic) IBOutlet UITextField *tfPasswordConfirm;

@end

@implementation SignUpViewCtrl{
    
    BOOL alreadyAcceptTermOfUse;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //if (IS_IPHONE_4)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:@"UIKeyboardWillShowNotification"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidHide:)
                                                     name:@"UIKeyboardWillHideNotification"
                                                   object:nil];
    }

    
    
    // Do any additional setup after loading the view.
    _tfFirstName.delegate = self;
    _tfLastName.delegate = self;
    _tfPenName.delegate = self;
    _tfEmail.delegate = self;
    _tfPassword.delegate = self;
    _tfPasswordConfirm.delegate = self;
    _tfPhone.delegate = self;
    
    if (! IS_SIMULATION) {
        _tfFirstName.text = @"";
        _tfLastName.text = @"";
        _tfPenName.text = @"";
        _tfEmail.text = @"";
        _tfPassword.text = @"";
        _tfPasswordConfirm.text = @"";
        _tfPhone.text = @"";
    }
}


#pragma mark- Keyboard Handeling.

#define KEYBOARD_MOVE_UP IS_IPHONE_4?100:20

- (void) keyboardWillShow:(NSNotification *)note {
 
    if (_tfPassword.isFirstResponder || _tfPassword.isFirstResponder) {
        
        if (self.view.frame.origin.y >= 0){
            [self setViewMovedUp:YES withHeigh:KEYBOARD_MOVE_UP];
        }
    }
}

- (void) keyboardDidHide:(NSNotification *)note{
    if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO withHeigh:KEYBOARD_MOVE_UP];
    }
}

- (IBAction)btnSignUpTap:(id)sender {
    
    //First Name
    NSString *firstname = self.tfFirstName.text;
    if (firstname == nil || [firstname isEqualToString:@""]){
        [self showAlert:@"Please input your first name"];
        [self.tfFirstName becomeFirstResponder];
        return;
    }
    
    if (![AppUtils isValidContentWithSymbolChar:firstname] || firstname.length > 50) {
        [self showAlert:@"Please correct your first name"];
        [self.tfFirstName becomeFirstResponder];

        return;

    }
    
    //Last Name
    NSString *lastname = self.tfLastName.text;
    if (lastname == nil || [lastname isEqualToString:@""]){
        [self showAlert:@"Please input your last name"];
        [self.tfLastName becomeFirstResponder];
        return;
    }
    if (![AppUtils isValidContentWithSymbolChar:lastname] || lastname.length > 50) {
        [self showAlert:@"Please correct your last name"];
        [self.tfLastName becomeFirstResponder];

        return;
        
    }
    
    //Pen Name
    NSString *penname = self.tfPenName.text;
    if (penname == nil || [penname isEqualToString:@""]){
        [self showAlert:@"Please input your pen name"];
        [self.tfPenName becomeFirstResponder];
        return;
    }
    if (![AppUtils isValidContentWithSymbolChar:penname] || penname.length > 50) {
        [self showAlert:@"Please correct your pen name"];
        [self.tfPenName becomeFirstResponder];

        return;
    }
    
    
    //Email
    if (![CommonUtils isValidEmail:self.tfEmail.text] || self.tfEmail.text.length > 50) {
        [self showAlert:@"Please input a valid email address"];
        [self.tfEmail becomeFirstResponder];
        return;
    }
    
    //phone
    if (![self.tfPhone.text isEqualToString:@""]) {
        if (![CommonUtils isValidPhone:self.tfPhone.text]) {
            [self showAlert:@"Please enter a valid phone number"];
            [self.tfPhone becomeFirstResponder];
            return;
        }
    }

    
    // password
    NSString *password = self.tfPassword.text;
    if (password == nil || [password isEqualToString:@""] || password.length < 6 || password.length > 16){
        [self showAlert:@"Password must be between 6 and 16 characters long, no spaces"];
        [self.tfPassword becomeFirstResponder];
        return;
    }else{
        
//        NSString *filteredStr;
//        NSScanner *scanner = [NSScanner scannerWithString:password];
//        NSCharacterSet *validChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"];
//        [scanner scanUpToCharactersFromSet:validChars intoString:NULL];
//        [scanner scanCharactersFromSet:validChars intoString:&filteredStr];
//        
//        if (filteredStr == nil || filteredStr.length == 0 || password.length != filteredStr.length) {
//            [self showAlert:@"Password must be between 6 and 16 characters long, no spaces"];
//            [self.tfPassword becomeFirstResponder];
//            return;
//        }

        NSRange range = [password rangeOfString:@" "];
        if (range.length != 0) {
            [self showAlert:@"Password format is invalid"];
            [self.tfPassword becomeFirstResponder];
            return;
        }
    }
    
    NSString *confpassword = self.tfPasswordConfirm.text;
    if (! [password isEqualToString:confpassword]) {
        [self showAlert:@"The confim password does not match"];
        [self.tfPasswordConfirm becomeFirstResponder];
        return;
    }
    
    [self doRegister];
    
}

-(void) doRegister{
    
    [APP_DELEGATE GALTrackerInCategory:@"Account" withAction:@"Register" withLabel:self.tfEmail.text];

    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:self.tfEmail.text forKey:@"email"];
    
    [dic setValue:self.tfPassword.text forKey:@"password"];
    [dic setValue:self.tfPassword.text forKey:@"password_confirmation"];

    [dic setValue:self.tfFirstName.text forKey:@"first_name"];
    [dic setValue:self.tfLastName.text forKey:@"last_name"];
    [dic setValue:self.tfPenName.text forKey:@"pen_name"];
    [dic setValue:self.tfPhone.text forKey:@"phone"];

    [self sendHttpPostRequest:SERVICE_REGISTER body:dic andRequestId:1];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_ACCEPT_TERM"]) {
        UINavigationController *nav = segue.destinationViewController;
        AcceptTermViewController *vc = nav.viewControllers[0];
        
        vc.parentController = self;
    }
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    if (! IS_IPHONE_4) {
//        return YES;
//    }
    
    if (textField == _tfPasswordConfirm || textField == _tfPassword || textField == _tfEmail) {
        
        if (self.view.frame.origin.y >= 0){
            [self setViewMovedUp:YES withHeigh:KEYBOARD_MOVE_UP];
        }
    }else{
        
        if (self.view.frame.origin.y < 0){
            [self setViewMovedUp:NO withHeigh:KEYBOARD_MOVE_UP];
        }
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    int currentTag = textField.tag;
    UITextField *next = (UITextField *)[self.view viewWithTag:currentTag +1];
    if (next) {
        [next becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.tfEmail || textField == self.tfLastName || textField == self.tfFirstName || textField == self.tfPenName) {
        
        NSString *text = textField.text;
        text = [text stringByReplacingCharactersInRange:range withString:string];
        if (text.length > 50) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if (data != nil) {
        
        SBJSON *jsonParser = [[SBJSON alloc] init];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSDictionary *dic = [jsonParser objectWithString:jsonString];
        NSDictionary *error = [dic valueForKey:@"error"];
        if (error != nil) {
            NSArray *array = error.allKeys;
            if (array && array.count > 0) {
                NSArray *msgarr = [error valueForKey:array[0]];
                if (msgarr && msgarr.count > 0) {
                    
                    NSString *errorMsg = [NSString stringWithFormat:@"%@", msgarr[0]];
                    NSRange range = [errorMsg rangeOfString:@"email"];
                    if (range.length != 0) {
                        [self.tfEmail becomeFirstResponder];
                    }else{
                        range = [errorMsg rangeOfString:@"penname"];
                        if (range.length != 0) {
                            [self.tfPenName becomeFirstResponder];
                        }
                    }
                    
                    [self showAlert:errorMsg];
                }else{
                    [self showAlert:@"Sign up failed"];
                }

            }else{
                [self showAlert:@"Sign up failed"];
            }
        }else{
            
            NSString *key = [dic valueForKey:@"access_token"];
            if (key != nil) {
                [self showAlert:@"Signup is successful"];

                [AppConfiguration sharedInstance].sessionToken = key;
                [AppConfiguration sharedInstance].userEmail = self.tfEmail.text;
                [AppConfiguration sharedInstance].lastName = [dic valueForKey:@"last_name"];
                [AppConfiguration sharedInstance].fristName = [dic valueForKey:@"first_name"];

                [[AppConfiguration sharedInstance] saveConfig];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                [APP_DELEGATE appLogined];
            }else{
                [self showAlert:@"Sign up failed"];
            }
        }
    }else{
        [self showNetworkErrMsg];
    }
}
@end