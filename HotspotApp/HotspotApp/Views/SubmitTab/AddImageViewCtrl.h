//
//  AddImageViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 8/3/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@protocol AddImageDelegete <NSObject>

- (void) returnSelectedImage:(UIImage*)img caption:(NSString*)caption didDeleted:(BOOL) deleted;

@end


@interface AddImageViewCtrl : BaseViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate, UITextFieldDelegate , UITextViewDelegate,UINavigationBarDelegate>

@property (weak, nonatomic) id<AddImageDelegete> delegete;

@property (weak, nonatomic) UIImage *inputImage;
@property (weak, nonatomic) NSString *inputImageUrl;

@property (weak, nonatomic) NSString *inputCaption;


- (IBAction)onCancelButton:(id)sender;

@end
