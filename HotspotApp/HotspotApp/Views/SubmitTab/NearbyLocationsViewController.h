//
//  NearbyLocationsViewController.h
//  HotspotApp
//
//  Created by hmchau on 9/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface NearbyLocationsViewController : BaseViewController


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain,nonatomic) UIViewController *parentController;

@end
