//
//  EnglishVersionViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@class PoemInfo;

@interface EnglishVersionViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextField *tfTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfContent;
@property (weak, nonatomic) IBOutlet UITextView *tvContent;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;


@property (strong, nonatomic) NSMutableDictionary *orginalParams;
@property (strong, nonatomic) PoemInfo * poemInfoEditting;


- (IBAction)onDoneKeyboardAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIToolbar *actionToolbar;


@end
