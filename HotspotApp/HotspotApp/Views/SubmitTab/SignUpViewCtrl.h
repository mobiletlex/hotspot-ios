//
//  SignUpViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface SignUpViewCtrl : BaseViewController<UITextFieldDelegate>

@end
