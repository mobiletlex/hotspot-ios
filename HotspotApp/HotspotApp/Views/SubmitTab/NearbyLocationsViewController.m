//
//  NearbyLocationsViewController.m
//  HotspotApp
//
//  Created by hmchau on 9/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "NearbyLocationsViewController.h"
#import "GoogleLocationParser.h"

//
//@interface LocationInfo : NSObject
//    @property (nonatomic, retain) NSString* name;
//    @property (nonatomic, retain) NSString* address;
//
//@end


@interface NearbyLocationsViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation NearbyLocationsViewController{
    
    NSMutableArray *_locationList;
}

@synthesize parentController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _locationList = [[NSMutableArray alloc] init];
    
    NSString *lat = [NSString stringWithFormat:@"%f", APP_DELEGATE.currentUserLocation.latitude];
    NSString *lon = [NSString stringWithFormat:@"%f", APP_DELEGATE.currentUserLocation.longitude];
    lat = [lat stringByReplacingOccurrencesOfString:@"," withString:@"."];
    lon = [lon stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@,%@&radius=%d&key=AIzaSyBWET3gTcKp12I_zekNT4qxfsLK2K4wu_I",lat,lon, 40 ];
    
    [self sendHttpGetRequest:url andRequestId:1];
}


#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    //    if ([self loadMoreAvailable]) {
    //
    //        return 35.0;
    //    }
    
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _locationList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *name = cell.textLabel;// (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *address = cell.detailTextLabel; //(UILabel *)[cell.contentView viewWithTag:2];
    name.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:16];
    address.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:12];
    
    GoogleLocationInfo *item = [_locationList objectAtIndex:indexPath.row];
    
    name.text = item.name;
    address.text = item.address;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.parentController) {
        GoogleLocationInfo *item = [_locationList objectAtIndex:indexPath.row];

        NSArray *arr = [NSArray arrayWithObjects:item.name,item.address, nil];
        [self.parentController performSelector:@selector(selectLocation:) withObject: arr];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Networking
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    //NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (data) {
        
        NSArray *array = [GoogleLocationParser parse:data];
        if (array) {
            //reload data
            [_locationList addObjectsFromArray:array];
            [self.tableView reloadData];
        }else{
            [self showNetworkErrMsg];
        }

    
    }else{
        [self showNetworkErrMsg];
    }
}



@end
