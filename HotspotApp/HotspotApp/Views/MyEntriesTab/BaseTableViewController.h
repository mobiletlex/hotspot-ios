//
//  BaseTableViewController.h
//  HotspotApp
//
//  Created by hmchau on 7/27/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface BaseTableViewController : UITableViewController

-(void) showAlert:(NSString*) msg;
-(void) showOnprogressMsg;
-(void) showNetworkErrMsg;
@end
