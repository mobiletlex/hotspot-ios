//
//  MyEntriesViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "MyEntriesViewCtrl.h"

typedef enum : NSUInteger {
    E_Draft_Tab,
    E_Submission_Tab,
    E_Archive_Photo_Tab,
} TabTableView;
@interface MyEntriesViewCtrl (){
    UIViewController* currentViewController;
    int currentIdx;
    __weak IBOutlet UIButton *btnDraft;
    __weak IBOutlet UIButton *btnSubmission;
    __weak IBOutlet UIButton *btnArchivePhoto;
}
@end

@implementation MyEntriesViewCtrl{
    
}


@synthesize willOpenSubmitAfterShare;
@synthesize willOpenDraftList;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)draftButtonTap:(id)sender {
    [self segmentChangedToIndex:E_Draft_Tab];
    btnDraft.selected = YES;
    btnSubmission.selected = NO;
    btnArchivePhoto.selected = NO;
}
- (IBAction)submisstionBtnTap:(id)sender {
    [self segmentChangedToIndex:E_Submission_Tab];
     btnDraft.selected = NO;
    btnSubmission.selected = YES;
    btnArchivePhoto.selected = NO;
}
- (IBAction)archivalPhotoTap:(id)sender {
    [self segmentChangedToIndex:E_Archive_Photo_Tab];
     btnDraft.selected = NO;
    btnSubmission.selected = NO;
    btnArchivePhoto.selected = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIViewController *vc = [self viewControllerForSegmentIndex:E_Draft_Tab];
    [self addChildViewController:vc];
    vc.view.frame = self.container.bounds;
    [self.container addSubview:vc.view];
    currentViewController = vc;
    currentIdx = E_Draft_Tab;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //NSLog(@"My entries viewWillAppear");
    
    if (self.willOpenSubmitAfterShare) {
        self.willOpenSubmitAfterShare = NO;
        [self submisstionBtnTap:nil];
        
    }else if (self.willOpenDraftList){
        self.willOpenDraftList = NO;
        [self draftButtonTap:nil];
    }
}


-(void) segmentChangedToIndex:(int)index{
    if(index == currentIdx){
        return;
    }
    UIViewController *vc = [self viewControllerForSegmentIndex:index];
    
    
    [self addChildViewController:vc];
    [self transitionFromViewController:currentViewController toViewController:vc duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [currentViewController.view removeFromSuperview];
        vc.view.frame = self.container.bounds;
        [self.container addSubview:vc.view];
    } completion:^(BOOL finished) {
        [vc didMoveToParentViewController:self];
        [currentViewController removeFromParentViewController];
        currentViewController = vc;
            currentIdx = index;
    }];
}

- (UIViewController *)viewControllerForSegmentIndex:(NSInteger)index {
    UIViewController *vc;
    switch (index) {
        case E_Draft_Tab:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DraftTable"];
            break;
        case E_Submission_Tab:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmitionTable"];
            break;
        case E_Archive_Photo_Tab:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyArchivePhotos"];
            break;
    }
    vc.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height);
    return vc;
}

@end
