//
//  MyEntriesViewCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/8/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"

@interface MyEntriesViewCtrl : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *container;

@property (assign) BOOL willOpenSubmitAfterShare;
@property (assign) BOOL willOpenDraftList;

@end
