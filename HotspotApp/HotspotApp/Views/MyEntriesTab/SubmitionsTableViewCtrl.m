//
//  SubmitionsTableViewCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "SubmitionsTableViewCtrl.h"
#import "MyEntriesParser.h"
#import "LandmarkManager.h"
#import "NewPoemsViewCtrl.h"
#import "DetailViewController.h"
#import "CommonUtils.h"

@interface SubmitionsTableViewCtrl () <UIAlertViewDelegate>

@end

@implementation SubmitionsTableViewCtrl{
    
    NSMutableArray *_poemList;
    int deleteRow;
    BOOL willReloadPoemlist;

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willReloadPoemList) name:NOTIFICATION_POSTED_NEW_POEM object:nil];

    _poemList = [[NSMutableArray alloc] init];
    willReloadPoemlist = YES;

}

-(void) willReloadPoemList{
    willReloadPoemlist = YES;

}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (willReloadPoemlist) {
        willReloadPoemlist = NO;
        [self requestScreenData];
    }
}
-(void)requestScreenData{
    //    NSString *geturl = [SERVICE_REQUEST_MY_ENTRIES stringByAppendingFormat:@"?status_id=%@&category_id=%@",POEM_STATUS_PUBLIC,POEM_CATEGORY_PUBLIC];
    NSString *geturl = [SERVICE_REQUEST_MY_ENTRIES stringByAppendingFormat:@"?status_id=%@",POEM_STATUS_PUBLIC];
    
    [self sendHttpGetRequest:geturl andRequestId:1];
    
}
//-(void) onEditBtn:(UIButton *) sender{
//    NSLog(@"onEditBtn");
//    
//    int index = (int)sender.superview.tag - 100;
//    [self performSegueWithIdentifier:@"SEGUE_EDIT_POEM" sender:[NSIndexPath indexPathForRow:index inSection:0]];
//}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _poemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MyEntriesSubmiCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell.contentView.tag = 100 + indexPath.row;
    UILabel *titlename = (UILabel *)[cell.contentView viewWithTag:1];
    titlename.font = [UIFont fontWithName:@"FuturaKoyuItalic" size:18];

    
    UILabel *content = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *centralname = (UILabel *)[cell.contentView viewWithTag:3];
    UIButton *editBtn = (UIButton *)[cell.contentView viewWithTag:4];
    //[editBtn addTarget:self action:@selector(onEditBtn:) forControlEvents:UIControlEventTouchUpInside];
    editBtn.userInteractionEnabled = NO;
    
    
    UILabel *monthLabel = (UILabel *)[cell.contentView viewWithTag:5];
    UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:6];
    
    PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
    titlename.text = item.title;
    content.text = item.content;
    centralname.text = [[LandmarkManager shareInstance] zoneNameById:item.zoneId];

    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:item.updated_at];
    
    if (date) {
        NSDateComponents *datecomp = [CommonUtils getSystemTimeComponentFrom:date];
        int day = datecomp.day;
        dateLabel.text = [NSString stringWithFormat: day > 9 ? @"%d": @"0%d", day];
        monthLabel.text = [CommonUtils monthNameInShortFromNumber:datecomp.month];
    }
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    //[self performSegueWithIdentifier:@"SEGUE_POEM_DETAIL" sender:indexPath];
//    [self performSegueWithIdentifier:@"SEGUE_EDIT_POEM" sender:indexPath];
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //try to load location detail
    deleteRow = indexPath.row;
    PoemInfo *item = [_poemList objectAtIndex:deleteRow];
    if (item) {
        [self sendHttpGetRequest:[SERVICE_REQUEST_LOCATION_DETAIL stringByAppendingFormat:@"%d", item.locationId] andRequestId:3];
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        deleteRow = indexPath.row;
        
        PoemInfo *item = [_poemList objectAtIndex:indexPath.row];
        if (item) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this poem?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            
            [alert show];
        }
    }
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SEGUE_EDIT_POEM"]) {
        
        PoemInfo *item = _poemList[deleteRow];
        item.isSubmitted = YES;
        
        NewPoemsViewCtrl *editvc = (NewPoemsViewCtrl *)segue.destinationViewController;
        editvc.poemInfoEditting = item;
    
    }else if ([segue.identifier isEqualToString:@"SEGUE_POEM_DETAIL"]){
        
        PoemInfo *item = _poemList[deleteRow];
        item.isFeaturedPoem = NO;

        DetailViewController *vc = (DetailViewController *) segue.destinationViewController;
        vc.poemInfo = item;
        vc.isReadOnly = YES;

    }
}


#pragma mark - Network callback
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    //NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (data != nil) {
        
        switch (requestId) {
            case 1:
            {
                NSArray *poemarr = [MyEntriesParser parse:data];
                if (poemarr) {
                    
                    [_poemList removeAllObjects];
                    
                    [_poemList addObjectsFromArray:poemarr];
                    [self.tableView reloadData];
                    
                    if (_poemList.count == 0) {
                        [self showAlert:@"There are no poems"];
                    }
                }
            }
                break;
                
            case 2://DELETE
            {
                NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                SBJSON *jsonParser = [[SBJSON alloc] init];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSDictionary *dic = [jsonParser objectWithString:jsonString];
                
                if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
                {
                    id errordic = [dic valueForKey:@"error"];
                    if (errordic != nil) {
                        [self showAlert:@"Cano not delete poem."];
                    }else{
                        [self showAlert:@"Your poem has been deleted successfully"];

                        [_poemList removeObjectAtIndex:deleteRow];
                        [self.tableView reloadData];
                    }
                }else{
                    [self showAlert:@"There is something wrong"];
                }
            }
                break;
                
                
            case 3://Get detail location to start editting
            {
                NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                SBJSON *jsonParser = [[SBJSON alloc] init];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSDictionary *dic = [jsonParser objectWithString:jsonString];
                
                if (dic != nil && [dic isKindOfClass:[NSDictionary class]] )
                {
                    PoemInfo *item = [_poemList objectAtIndex:deleteRow];
                    item.locationName = [dic objectForKey:@"name"];
                    
                    [self performSegueWithIdentifier:@"SEGUE_EDIT_POEM" sender:self];
                    
                }else{
                    [self showAlert:@"Can not change this poem, please try again later"];
                }
            }
                
            default:
                break;
        }
        
        
    }else{
        [self showNetworkErrMsg];
    }
    
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != alertView.cancelButtonIndex) {
        PoemInfo *item = [_poemList objectAtIndex:deleteRow];
        if (item) {
            
            NSString *deleteurl = [SERVICE_DELETE_POEM stringByAppendingFormat:@"%@", item.poemid];
            [self sendHttpDeleteRequest:deleteurl body:nil andRequestId:2];
        }
    }
}


@end
