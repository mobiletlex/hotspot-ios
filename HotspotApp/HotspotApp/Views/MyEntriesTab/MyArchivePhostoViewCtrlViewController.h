//
//  MyArchivePhostoViewCtrlViewController.h
//  HotspotApp
//
//  Created by reoxinh on 7/20/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyArchivePhostoViewCtrlViewController : UICollectionViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;



@end
