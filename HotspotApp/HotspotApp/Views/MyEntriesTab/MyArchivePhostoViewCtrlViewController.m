//
//  MyArchivePhostoViewCtrlViewController.m
//  HotspotApp
//
//  Created by reoxinh on 7/20/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "MyArchivePhostoViewCtrlViewController.h"
#import "LandmarkManager.h"
#import "CommonUtils.h"
#import "UIImageView+WebCache.h"

@interface MyArchivePhostoViewCtrlViewController ()

@end

@implementation MyArchivePhostoViewCtrlViewController{
    
    NSMutableArray *_archivementList;
    NSMutableDictionary *_archivementListImageUrl;

    
    NSArray *_archivementHeaderList;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    _archivementList = [[NSMutableArray alloc] init];
    _archivementListImageUrl = [[NSMutableDictionary alloc] init];

}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [_archivementList removeAllObjects];
    [_archivementListImageUrl removeAllObjects];
    
    NSDictionary *dict = [[LandmarkManager shareInstance] generateTableSessionArray];
    
    NSArray *sortedKeys = [[dict allKeys] sortedArrayUsingSelector: @selector(compare:)];
    sortedKeys = [[sortedKeys reverseObjectEnumerator] allObjects];

    for (NSString *key in sortedKeys) {
        [_archivementList addObject: [dict objectForKey: key]];
    }
    
    _archivementHeaderList = sortedKeys;
    
    for (NSArray *arr in _archivementList) {
        for (NSString *item in arr) {
            if (item) {
                LandmarkInfo *iteminfo = [[LandmarkManager shareInstance] getLocationDetailById:[item intValue]];
                
                if (iteminfo.photos && iteminfo.photos.count > 0) {
                    [_archivementListImageUrl setValue:iteminfo.photos[0] forKey:item];
                }
            }
        }
    }
    
    
    [self.collectionView reloadData];
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _archivementList.count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    NSArray *array = [_archivementList objectAtIndex:section];
    if (array) {
        return array.count;
    }
    
    return 0;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCollectionViewIdentifier" forIndexPath:indexPath];
    
    UIImageView*image = (UIImageView*)[cell viewWithTag:100];
    
    NSArray *array = [_archivementList objectAtIndex:indexPath.section];
    if (array && array.count >= indexPath.row) {
        NSString *locationid = array[indexPath.row];
        if (locationid) {
            NSString *url = [_archivementListImageUrl valueForKey:locationid];
            [image setImageWithURL:[NSURL URLWithString:url]];
        }else{
            image.image = [UIImage imageNamed:@"img_selected_tab_bg.png"];
        }
    }
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderIdentifier" forIndexPath:indexPath];
        UILabel* headerLabel = (UILabel* )[reusableview viewWithTag:100];
        headerLabel.font = [UIFont fontWithName:@"Futura-Bold" size:18];
        
        NSString *datetimeString = _archivementHeaderList[indexPath.section];
        int year = [[datetimeString substringToIndex:4] intValue];
        int month = [[datetimeString substringWithRange:NSMakeRange(4,2)] intValue];
        int day = [[datetimeString substringWithRange:NSMakeRange(6,2)] intValue];
        
        //NSDate *date = [CommonUtils getCurrentDateFromYear:year month:month day:day];
        //headerLabel.text = [CommonUtils dateTimeStringFromDate:date];
        
        headerLabel.text = [CommonUtils dateTimeStringFromYear:year month:month day:day];

    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterCollectionViewIdentifier" forIndexPath:indexPath];
    }
    return reusableview;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"Selected View index=%d",indexPath.row);
    NSArray *array = [_archivementList objectAtIndex:indexPath.section];
    if (array && array.count >= indexPath.row) {
        NSString *locationid = array[indexPath.row];
        if (locationid) {
            
            LandmarkInfo *item = [[LandmarkManager shareInstance] getLocationDetailById:[locationid intValue]];
            
            if (item == nil) {
                return;
            }
            
            [APP_DELEGATE showLocationInfoView:item];
        }
    }

}
@end
