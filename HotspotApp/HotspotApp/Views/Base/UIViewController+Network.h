//
//  UIViewController+Network.h
//  Template
//
//  Created by XAV-MAC13 on 17/12/13.
//  Copyright (c) 2013 XAV-MAC13. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Network)


-(BOOL) isInternetAvailable;
-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId;

-(void)sendHttpPostRequest:(NSString*)url body:(NSMutableDictionary*)body andRequestId:(int)requestId;
-(void)sendHttpPutRequest:(NSString*)url body:(NSMutableDictionary*)body andRequestId:(int)requestId;
-(void)sendHttpDeleteRequest:(NSString*)url body:(NSMutableDictionary*)body andRequestId:(int)requestId;

-(void)sendHttpGetRequest:(NSString*)url andRequestId:(int)requestId;


@end
