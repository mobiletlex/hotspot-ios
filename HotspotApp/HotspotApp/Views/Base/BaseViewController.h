//
//  BaseViewController.h
//  RaoVatApp
//
//  Created by hmchau on 6/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Network.h"

@interface BaseViewController : UIViewController

- (IBAction)onNaviBackBtn:(id)sender;

-(void) setViewMovedUp:(BOOL)movedUp withHeigh:(float) height;
-(void) showAlert:(NSString*) msg;
-(void) showProgressIndicatorEndWithSelector:(SEL) target;
-(void) showOnprogressMsg;
-(void) showNetworkErrMsg;

+ (UIImage*)getDarkBlurredImageWithTargetView:(UIView *)targetView;

@end
