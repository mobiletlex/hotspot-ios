//
//  BaseViewController.m
//  RaoVatApp
//
//  Created by hmchau on 6/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "UIImage+ImageEffects.h"

@interface BaseViewController ()

@end

@implementation BaseViewController{
    SEL _waitSel;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor *barcolor = [UIColor whiteColor];
    if(IS_OS7_AND_LATER) {
        self.navigationController.navigationBar.barTintColor = barcolor;
         self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    else{
        self.navigationController.navigationBar.tintColor = barcolor;
    }
    
    //self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:223/255.0 green:0.0 blue:119/255.0 alpha:1.0], NSFontAttributeName:[UIFont boldSystemFontOfSize:20] };
    
     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:223/255.0 green:0.0 blue:119/255.0 alpha:1.0], NSFontAttributeName:[UIFont fontWithName:@"Futura-Bold" size:18] };
    
    self.navigationController.navigationBar.translucent = YES;
    
    
    //UIBarButtonItem *back = self.navigationItem.backBarButtonItem;
//    NSArray *arr = self.navigationController.navigationBar.items;
//    UINavigationBar *bar = self.navigationController.navigationBar;
//    if (self.navigationController.navigationBar.items.count >= 2)
//    {
//        self.navigationController.navigationBar.topItem.title = @"";
//    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNaviBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) setViewMovedUp:(BOOL)movedUp withHeigh:(float) height
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        //NSLog(@"move up %f",height);
        rect.origin.y -= height;
    }
    else
    {
        //NSLog(@"move down %f",height);
        rect.origin.y += height;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


-(void) showAlert:(NSString*) msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void) showOnprogressMsg{
    [self showAlert:@"In Progress"];
}

-(void) showNetworkErrMsg{
    [self showAlert:@"Can not connect server, please check your connection"];
}


-(void) showIndicator{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate showIndicator];
}

-(void) hideIndicator{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate hideIndicator];
}

-(void) showProgressIndicatorEndWithSelector:(SEL) target{
    [self showIndicator];
    _waitSel = target;
    
    [self performSelector:@selector(showProgressIndicatorEnd) withObject:nil afterDelay:0.5];
}

-(void) showProgressIndicatorEnd{
    [self hideIndicator];
    [self performSelector:_waitSel withObject:nil];
}

+ (UIImage*)getDarkBlurredImageWithTargetView:(UIView *)targetView
{
    CGSize size = targetView.frame.size;
    
    UIGraphicsBeginImageContext(size);
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(c, 0, 0);
    [targetView.layer renderInContext:c];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [image applyBlurWithRadius:1.0 tintColor:[UIColor colorWithWhite:10 alpha:0.1] saturationDeltaFactor:2.0 maskImage:nil];
}

@end
