//
//  CustomeTabBarItem.m
//  HotspotApp
//
//  Created by hmchau on 7/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CustomeTabBarItem.h"

@implementation CustomeTabBarItem

- (void)awakeFromNib {
    [self setImage:self.image];
}

- (void)setImage:(UIImage *)image {
    [super setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    self.selectedImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
