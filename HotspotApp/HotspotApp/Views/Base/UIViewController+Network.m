//
//  UIViewController+Network.m
//  Template
//
//  Created by XAV-MAC13 on 17/12/13.
//  Copyright (c) 2013 XAV-MAC13. All rights reserved.
//
#define kMessageInternetNotAvailable @"Please enable internet connection on your device"
#import "UIViewController+Network.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "ASIFormDataRequest.h"
#import "JsonUtil.h"

@implementation UIViewController (Network)

-(void)didReceiveData:(NSData*)data error:(NSError*)error andRequestId:(int)requestId
{
    
}
-(BOOL) isInternetAvailable :(int) requestId
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    
    switch (netStatus)
    {
        case NotReachable:        {
            
            if (requestId == INITIAL_REQUEST_LOCATION || requestId != INITIAL_REQUEST_ZONE || requestId != INITIAL_REQUEST_EVENTS) {
                
            }else{
                [self showMessage:kMessageInternetNotAvailable];

            }

            return NO;
            break;
        }
            
        case ReachableViaWWAN:        {
            
            
            break;
        }
        case ReachableViaWiFi:        {
            
            
            break;
        }
    }
    return YES;
    
}

-(void) showMessage:(NSString*)msg
{
    UIAlertView* alertView=[[UIAlertView alloc]initWithTitle:msg message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
}

-(NSString *)urlEncodeUTF:(NSString *) url {
    NSString* escapedUrlString = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return escapedUrlString;
}

-(void) commonHttpRequestInUrl:(NSString *) url withMethod:(NSString *)method withHeader:(NSDictionary*) params withBody:(NSDictionary*) body andRequestId:(int) requestId{
    
    if ([self isInternetAvailable:requestId]) {
        
        if (requestId == INITIAL_REQUEST_LOCATION || requestId == INITIAL_REQUEST_ZONE || requestId == INITIAL_REQUEST_EVENTS) {
            
        }else{
            MBProgressHUD* hud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText=@"Loading...";
        }
        
        
        NSOperationQueue* operationQueue=[[NSOperationQueue alloc]init];
        [operationQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
            
            NSString * encodedurl = url;//[self urlEncodeUTF:url];
            
            ASIFormDataRequest* request=[[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:encodedurl]];
            //[request setRequestMethod:method];
            
            //custome setting for server
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request addRequestHeader:@"Accept" value:@"application/json"];
            [request addRequestHeader:@"X-Api-Key" value:X_API_SECRET_KEY]; //IyN0ZXh0aW50aGVjaXR5IyM=

            NSString *token = [AppConfiguration sharedInstance].sessionToken;
            if (token != nil && ![token isEqualToString:@""]) {
                [request addRequestHeader:@"X-Auth-Token" value:token];
            }
            
            [request setTimeOutSeconds:20];
            //[request setNumberOfTimesToRetryOnTimeout:2];
            
            request.delegate = self;
            
            //set Headers
            for (NSString* key in params.allKeys) {
                [request addRequestHeader:key value:[params valueForKey:key]];
            }
            
            if(body){
                BOOL attachImage = NO;
                for (NSString* key in body.allKeys) {
                    if ([key isEqualToString:@"photo_data"]) {
                        attachImage = YES;
                    }else{
                        [request addPostValue:[body valueForKey:key] forKey:key];
                    }
                }
                
                if (attachImage) {
                    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
                    
                    NSData *imageData = [body valueForKey:@"photo_data"];
                    [request addData:imageData withFileName:@"poem_image" andContentType:@"image/jpeg" forKey:@"photo"];
                }
            }
            
            if ([method isEqualToString:@"DELETE"]) {
                [request buildPostBody];
            }
            
            [request setRequestMethod:method];

            
            [request startSynchronous];
            NSError* error=request.error;
            NSData* data=request.responseData;
            
//            NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSLog(@"Commont Result: %@", returnString);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self didReceiveData:data error:error andRequestId:requestId];
                
            });
            
        }]];
    }else{
        if (requestId == INITIAL_REQUEST_LOCATION || requestId == INITIAL_REQUEST_ZONE) {
            
            [self performSelector:@selector(willPostMessageToHome:) withObject:[NSNumber numberWithInt:requestId] afterDelay:0.5];
        }
    }
}

-(void) willPostMessageToHome:(NSNumber *) reqid {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self didReceiveData:nil error:nil andRequestId:[reqid intValue]];
    });
}

-(void)sendHttpGetRequest:(NSString*)url andRequestId:(int)requestId
{
    [self commonHttpRequestInUrl:url withMethod:@"GET" withHeader:nil withBody:nil andRequestId:requestId];
}

-(void)sendHttpPostRequest:(NSString*)url body:(NSMutableDictionary*)body andRequestId:(int)requestId{
    
    [self commonHttpRequestInUrl:url withMethod:@"POST" withHeader:nil withBody:body andRequestId:requestId];
}

-(void)sendHttpPutRequest:(NSString*)url body:(NSMutableDictionary*)body andRequestId:(int)requestId{
    
    [self commonHttpRequestInUrl:url withMethod:@"PUT" withHeader:nil withBody:body andRequestId:requestId];
}

-(void)sendHttpDeleteRequest:(NSString*)url body:(NSMutableDictionary*)body andRequestId:(int)requestId{
    
    [self commonHttpRequestInUrl:url withMethod:@"DELETE" withHeader:nil withBody:body andRequestId:requestId];
}


@end
