//
//  AppDelegate.m
//  RaoVatApp
//
//  Created by hmchau on 6/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "UserDefined.h"
#import "LandmarkManager.h"
#import "LoadingViewController.h"
#import "MainTabbarCtrl.h"
#import "NewPoemsViewCtrl.h"
#import "AppConfiguration.h"
#import "LocationIntroduceViewController.h"
#import "WEPopoverController.h"
#import "CommonUtils.h"
#import "TutorialViewController.h"
#import "AppConfiguration.h"

#import <SystemConfiguration/SystemConfiguration.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"


@interface AppDelegate () <LocationIntroduceDelegate>

@end

@implementation AppDelegate{
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentLocationCordinate;
    WEPopoverController *_popoverController;
    LoadingViewController *_loadingPage;
    
    TutorialViewController *_tutorialPage;
}

@synthesize currentUserLocation = currentLocationCordinate;
@synthesize arrCity;
@synthesize isLogined;


-(void) setupGoogleAnalytiscs{
    [GAI sharedInstance].trackUncaughtExceptions = NO;

    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 2;
    
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-55451450-1"];
    [GAI sharedInstance].defaultTracker = tracker;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    //[CommonUtils printAllFonts];
    
    isLogined = [AppConfiguration isLogin];
    [self setupGoogleAnalytiscs];
    
    if (IS_OS7_AND_LATER)
    {
//        [UITabBarItem.appearance setTitleTextAttributes:
//         @{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
//        
//        [UITabBarItem.appearance setTitleTextAttributes:
//         @{NSForegroundColorAttributeName : THEME_COLOR } forState:UIControlStateSelected];
//        
//        //[UITabBarItem.appearance setTin]
//        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:233/255.0 green:20/255.0 blue:146/255.0 alpha:1]];
        
        [UITabBarItem.appearance setTitleTextAttributes:
         @{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
        
        [UITabBarItem.appearance setTitleTextAttributes:
         @{NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
        
        //[UITabBarItem.appearance setTin]
        [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
        
        [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"img_selected_tab_bg.png"]];
    }

    //Change back button image for all views
    UIImage *backBtnIcon = [UIImage imageNamed:@"back_btn"];
    if (IS_OS7_AND_LATER) {
        [UINavigationBar appearance].backIndicatorImage = backBtnIcon;
        [UINavigationBar appearance].backIndicatorTransitionMaskImage = backBtnIcon;
        
        //[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000) forBarMetrics:UIBarMetricsDefault];
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:
         @{NSFontAttributeName: [UIFont systemFontOfSize:1], NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateNormal];

    }
    
    UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;
    
    tabBar.tabBar.autoresizesSubviews = NO;
    tabBar.tabBar.clipsToBounds = YES;
    [self startLocationTimer];
    
    UIStoryboard *storyboard = tabBar.storyboard;
    _loadingPage = [storyboard instantiateViewControllerWithIdentifier:@"LoadingPage"];
    [tabBar.view addSubview:_loadingPage.view];
    
    tabBar.selectedIndex = 0;
    
    
    if ([AppConfiguration sharedInstance].firstRunApp) {
        [self showTutorialPage];
    }
    
    return YES;
}


							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

-(void) showAlert:(NSString*) msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    if (! [CLLocationManager locationServicesEnabled]){
        [self showAlert:@"Turn On Location Services to Allow \"TextInTheCity\" to determine Your Location"];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void) showIndicator{
    
    UIView *view = [self.window viewWithTag:10010];
    if (view != nil) {
        return;
    }
    
    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    [hub setLabelText:@"Loading..."];
    hub.tag = 10010;

}

-(void) hideIndicator{
    [MBProgressHUD hideHUDForView:self.window animated:YES];
}


#pragma mark - CLLocationManager

-(void)startLocationUpdates{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}


-(void)startLocationTimer{
    [self startLocationUpdates];
    
//    NSTimer *_timer = [NSTimer scheduledTimerWithTimeInterval:2
//                                                  target:self
//                                                selector:@selector(changeAccuracy)
//                                                userInfo:nil
//                                                 repeats:NO];

    NSTimer *_timer = [NSTimer scheduledTimerWithTimeInterval:10
                                                       target:self
                                                     selector:@selector(changeAccuracy)
                                                     userInfo:nil
                                                      repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void) changeAccuracy {
    //NSLog(@"##############Start scanning landmark"  );
    
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];

    //[self performSelectorInBackground:@selector(scanningInBackground) withObject:nil];
}

-(void) scanningInBackground{
    if ([[LandmarkManager shareInstance] scanLandmarkInRegion:currentLocationCordinate]) {
        [self performSelectorOnMainThread:@selector(scanFoundLandmark) withObject:nil waitUntilDone:NO];
    }
}

-(void) scanFoundLandmark{
    NSLog(@"Found landmark arroud here");
    
    NSArray *arr = [LandmarkManager shareInstance].foundUnlockLandmarks;
    if (arr && arr.count > 0) {
        NSString *str = @"";
        
        for (LandmarkInfo *item in arr) {
            NSString *itemname = item.name;
            if (str.length == 0) {
                str = [str stringByAppendingFormat:@"%@", [itemname uppercaseString]];

            }else{
                str = [str stringByAppendingFormat:@", %@ ", [itemname uppercaseString]];
            }
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"UNLOCKED!" message:[NSString stringWithFormat:@"You have unlocked \"%@\"", str] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 10001;
        [alert show];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCAION_UNLOCK object:nil];
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    currentLocationCordinate = newLocation.coordinate;
    
//    [UserDefaultsUtil saveNSObject:[NSString stringWithFormat:@"%f",currentLocationCordinate.latitude] forKey:kSP_USER_LAT];
//    [UserDefaultsUtil saveNSObject:[NSString stringWithFormat:@"%f",currentLocationCordinate.longitude] forKey:kSP_USER_LONG];
    
    [locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
    [locationManager setDistanceFilter:99999];
    
    //NSLog(@"didUpdateToLocation");
    //[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_LOCATION_UPDATE object:nil];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"locationManager : didFailWithError %@", error);
    
    if(IS_SIMULATION){
        //Tan Binh
        currentLocationCordinate = CLLocationCoordinate2DMake(10.800217, 106.655380);
    }else{
        if (error && error.code == kCLErrorDenied) {
            
            if ([CLLocationManager locationServicesEnabled]){
                [self showAlert:@"Turn On Location Services to Allow \"TextInTheCity\" to determine Your Location"];
            }
        }
    }
    
}
-(void) appLogined{
    isLogined = YES;
    MainTabbarCtrl* tabBar = (MainTabbarCtrl *)self.window.rootViewController;
    if(tabBar.selectedIndex == E_TAB_SUBMIT){
        
        UINavigationController* nav =  [tabBar.viewControllers objectAtIndex:E_TAB_SUBMIT];
        NewPoemsViewCtrl*  newPoemsViewCtrl = [tabBar.storyboard instantiateViewControllerWithIdentifier:@"NEW_POEMS_STORYBOARD"];
        nav.viewControllers = [NSArray arrayWithObjects:newPoemsViewCtrl, nil];
        
    }else if (tabBar.selectedIndex == E_TAB_MY_ENTRIES ){
        
        UINavigationController* nav =  [tabBar.viewControllers objectAtIndex:E_TAB_MY_ENTRIES];
        NewPoemsViewCtrl*  myentriesview = [tabBar.storyboard instantiateViewControllerWithIdentifier:@"MY_ENTRIES_STORYBOARD"];
        nav.viewControllers = [NSArray arrayWithObjects:myentriesview, nil];
    }
    
    
}

-(void) appLogOut{
    isLogined = NO;
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 10001) {
        NSArray *arr = [LandmarkManager shareInstance].foundUnlockLandmarks;
        if (arr != nil && arr.count > 0) {
            LandmarkInfo *item = arr[0];
            
            [self showLocationInfoView: item];
        }
    }
}


-(void) showLocationInfoView:(LandmarkInfo *) landmark {
    
    if (landmark == nil) {
        return;
    }
    

    UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;
    UIStoryboard *storyboard = tabBar.storyboard;
    
    LocationIntroduceViewController *locationdetail = [storyboard instantiateViewControllerWithIdentifier:@"LocationIntroduceViewController"];
    
    locationdetail.delegate = self;
    locationdetail.locationInfo = landmark;
    locationdetail.contentSizeForViewInPopover = CGSizeMake(300 , 440 );
    
    if (!_popoverController) {
        _popoverController = [[WEPopoverController alloc] initWithContentViewController:locationdetail];
    }
    else{
        _popoverController.contentViewController = locationdetail;
    }
    
    CGPoint point = self.window.center;
    CGRect rect = CGRectMake(point.x, point.y - 440/2 + 4, 1, 1);
    
    //get top view of tabbar
    UIViewController *topview = [self topViewControllerWithRootViewController:tabBar];
    if (topview != nil) {
        [_popoverController presentPopoverFromRect:rect inView:topview.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}


//Call from popup delegate
-(void) onCloseButton{
    
    if (_popoverController) {
        [_popoverController dismissPopoverAnimated:YES];
    }
}


- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        
        for (UIView *view in [rootViewController.view subviews])
        {
            id subViewController = [view nextResponder];
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]])
            {
                return [self topViewControllerWithRootViewController:subViewController];
            }
        }
        return rootViewController;
    }
}

-(void) showTutorialPage{
    UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;
    
    UIStoryboard *storyboard = tabBar.storyboard;
    _tutorialPage = [storyboard instantiateViewControllerWithIdentifier:@"TutorialPage"];
    int tabindex = tabBar.selectedIndex;
    if (tabindex != 4){
        _tutorialPage.hideBackButton = YES;
    }
    
    [tabBar.view addSubview:_tutorialPage.view];
    
}

-(void) updateUnreadEventCountBadge{
    UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;

    UITabBarItem *tbi = (UITabBarItem *)tabBar.tabBar.items[4];

    int unreadcount = [[LandmarkManager shareInstance] countUnreadEvent];
    if (unreadcount != 0) {
        NSString *text = [NSString stringWithFormat:@"%d", unreadcount];
        tbi.badgeValue = text;
    }else{
        tbi.badgeValue = nil;
    }
}

-(void) GALTrackerInCategory:(NSString *)category withAction:(NSString *) action  withLabel:(NSString *) label{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:category
                                                                           action:action
                                                                            label:label
                                                                            value:nil];
    [tracker send:[builder build]];
}


@end
