//
//  AppDelegate.h
//  RaoVatApp
//
//  Created by hmchau on 6/10/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountInfo.h"
#import <CoreLocation/CoreLocation.h>

@class LandmarkInfo;

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

@property (assign) CLLocationCoordinate2D currentUserLocation;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray* arrCity;
@property (strong, nonatomic) AccountInfo* accountInfo;
@property (nonatomic) bool isLogined;
-(void) showIndicator;
-(void) hideIndicator;
-(void) appLogined;
-(void) appLogOut;

-(void) showLocationInfoView:(LandmarkInfo *) landmark;
-(void) showTutorialPage;
-(void) updateUnreadEventCountBadge;

-(void) GALTrackerInCategory:(NSString *)category withAction:(NSString *) action  withLabel:(NSString *) label;

@end
