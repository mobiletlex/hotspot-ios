//
//  AppUtils.h
//  HotspotApp
//
//  Created by hmchau on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtils : NSObject

+(NSString *) zonePrefixByZoneId:(int) zoneId;


+(NSString *) languageNameFromLanguageId:(int) languageId;
+(int) getIndexLangugeButtonFromLanguageId:(int) languageId;
+(int) getLanguageIdFromButtonIndex:(int) buttonIndex;

+(NSString *) monthNameInShortFromNumber:(int) number;

+(double) distanceBetweenCoordinate:(CLLocationCoordinate2D) loc1 toOther:(CLLocationCoordinate2D) loc2;
+(BOOL) isValidContentWithSymbolChar:(NSString *) poemTitleContent;
+(BOOL) validStringAfterTrimWhitespace:(NSString *) str;

@end
