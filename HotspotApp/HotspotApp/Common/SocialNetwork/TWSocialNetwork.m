//
//  TWSocialNetwork.m
//  HotspotApp
//
//  Created by hmchau on 7/31/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "TWSocialNetwork.h"

@implementation TWSocialNetwork


-(BOOL) shareContent:(NSString *) content image:(UIImage*) image url:(NSString *) url{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
//        int medialengh = 0;
//        if (image) {
//            [controller addImage:image];
//            medialengh = 32;
//            if (url) {
//                medialengh += 33;
//                [controller addURL:[NSURL URLWithString:url]];
//            }
//        }
//        
//        if (image == nil && url != nil) {
//            //Attach url into content if have no image
//            if (content.length  + 32 > 140 - medialengh) {
//                content = [content substringToIndex:140 - medialengh - 32 - 3 - 1];//2 = "..." 1 = "\n"
//                content = [content stringByAppendingString:@"..."];
//            }
//
//            content = [content stringByAppendingFormat:@"\n%@", url];
//        }else{
//            //has image without url
//            if (content.length > 140 - medialengh) {
//                content = [content substringToIndex:140 - medialengh - 3];
//                content = [content stringByAppendingString:@"..."];
//            }
//        }
        
        [controller setInitialText:content];
    
        
        [self.parentController presentViewController:controller animated:YES completion:nil];
        
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            if (result == SLComposeViewControllerResultDone) {
                NSLog(@"Posted");
            } else if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"Post Cancelled");
            } else {
                NSLog(@"Post Failed");
            }
        }];
        
        return YES;
    }
    
    return NO;
}


@end
