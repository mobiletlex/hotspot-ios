//
//  FbSocialNetwork.m
//  HotspotApp
//
//  Created by hmchau on 7/31/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "FbSocialNetwork.h"

@implementation FbSocialNetwork


-(BOOL) shareContent:(NSString *) content image:(UIImage*) image url:(NSString *) url{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        if (image) {
            [controller addImage:image];
            
            if (url) {
                [controller addURL:[NSURL URLWithString:url]];
            }
        }
            
        if (image== nil && url != nil) {
            content = [content stringByAppendingFormat:@"\n%@", url];
        }
        
        [controller setInitialText:content];


        [self.parentController presentViewController:controller animated:YES completion:nil];
        
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            if (result == SLComposeViewControllerResultDone) {
                NSLog(@"Posted");
            } else if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"Post Cancelled");
            } else {
                NSLog(@"Post Failed");
            }
        }];
        
        return YES;
    }
    
    return NO;
}

@end
