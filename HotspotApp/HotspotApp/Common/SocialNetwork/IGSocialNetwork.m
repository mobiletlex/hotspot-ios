//
//  IGSocialNetwork.m
//  HotspotApp
//
//  Created by hmchau on 7/31/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "IGSocialNetwork.h"

@interface IGSocialNetwork () <UIDocumentInteractionControllerDelegate>

@end

@implementation IGSocialNetwork{
    UIDocumentInteractionController *docController;
    
}

-(BOOL) shareContent:(NSString *) content image:(UIImage*) image url:(NSString *) url{
    
    NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ig_image.igo"];
    
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:savePath atomically:YES];
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
 
        NSURL *imageUrl = [NSURL fileURLWithPath:savePath];
        
        docController = [UIDocumentInteractionController interactionControllerWithURL:imageUrl];
        docController.UTI = @"com.instagram.exclusivegram";
        docController.delegate = self;
        
        if (url) {
            content = [content stringByAppendingFormat:@"\n%@",url];
        }
        
        docController.annotation = [NSDictionary dictionaryWithObject:content forKey:@"InstagramCaption"];
        
        UIViewController *vc = self.parentController;
        [docController presentOpenInMenuFromRect:CGRectZero inView:vc.view animated:YES];
        
        return YES;
    }
    
    return NO;
}


#pragma mark - UIDocumentInteractionControllerDelegate

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application{
    
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application{
    
}

@end
