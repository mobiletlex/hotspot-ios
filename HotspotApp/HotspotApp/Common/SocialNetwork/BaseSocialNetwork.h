//
//  BaseSocialNetwork.h
//  HotspotApp
//
//  Created by hmchau on 7/31/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>

@interface BaseSocialNetwork : NSObject

@property (weak, nonatomic) UIViewController *parentController;

-(BOOL) shareContent:(NSString *) content image:(UIImage*) image url:(NSString *) url ;

@end
