//
//  MainTabbarCtrl.h
//  HotspotApp
//
//  Created by reoxinh on 7/19/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    E_TAB_EXPLORER = 0,
    E_TAB_POEMS,
    E_TAB_SUBMIT,
    E_TAB_MY_ENTRIES,
    E_TAB_SETTING,
} TAB_INDEX;
@interface MainTabbarCtrl : UITabBarController<UITabBarControllerDelegate>

@end
