//
//  CommonUtils.m
//  RaoVatApp
//
//  Created by hmchau on 6/15/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonUtils.h"

@implementation CommonUtils


+(BOOL) isValidPhone:(NSString *) number{
    
    NSString *temp = number;
    
    NSString *numberString;
    NSScanner *scanner = [NSScanner scannerWithString:temp];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    if (temp.length != numberString.length) {
        return NO;
    }
    
    return YES;

}

+(BOOL) isValidEmail:(NSString *)checkString
{
    if (checkString == nil || [checkString isEqualToString:@""]) {
        return NO;
    }
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(int) randomNumber:(int) inside{
    return arc4random_uniform(inside);
}


+ (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

+ (NSString *)resoucePathForFileName:(NSString *)name{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:name];
}



+(NSString *) timeStringFromSecond:(int) second{
    
    int minute = second / 60;
    int remainsecond = second % 60;
    
    NSString *timeString = @"";
    timeString = minute < 10 ? [timeString stringByAppendingFormat:@"0%d:",minute]:[timeString stringByAppendingFormat:@"%d:",minute] ;
    
    timeString = remainsecond < 10 ? [timeString stringByAppendingFormat:@"0%d",remainsecond]:[timeString stringByAppendingFormat:@"%d",remainsecond] ;
    
    return timeString;
}

//default format
+ (NSString *) dateTimeStringFromDate:(NSDate *) date{
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"us"]];
    
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"YYYYMMMd" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"us"]]];
    
    NSString *timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
    
//    NSString *timestamp = [NSDateFormatter localizedStringFromDate:date
//                                                         dateStyle:kCFDateFormatterFullStyle
//                                                         timeStyle:NSDateFormatterNoStyle];
//
//    return timestamp;

}

+ (NSString *)suffixForDay:(int)day
{
    if (day >= 11 && day <= 13) {
        return @"th";
    } else if (day % 10 == 1) {
        return @"st";
    } else if (day % 10 == 2) {
        return @"nd";
    } else if (day % 10 == 3) {
        return @"rd";
    } else {
        return @"th";
    }
}

+ (NSString *) dateTimeStringFromYear:(int) year month:(int) month day:(int) day{
    
    NSString *monthstr = [CommonUtils monthNameFromNumber:month];
    NSString *yearstr = [NSString stringWithFormat:@"%d",year];
    NSString *datestr = [NSString stringWithFormat:@"%d%@",day,[CommonUtils suffixForDay:day]];
    
    return [NSString stringWithFormat:@"%@ %@, %@",monthstr,datestr,yearstr];
}

+(NSString *) monthNameFromNumber:(int) number{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"us"]];
    
    NSString *monthName = [[df monthSymbols] objectAtIndex:(number-1)];
    return monthName;
}

+(NSString *) monthNameInShortFromNumber:(int) number{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"us"]];
    
    NSString *monthName = [[df shortMonthSymbols] objectAtIndex:(number-1)];
    return monthName;
}



+(NSString *) monthNameFromDate:(NSDate *) today{
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSYearCalendarUnit |
    NSMonthCalendarUnit |
    NSWeekdayCalendarUnit |
    NSDayCalendarUnit |
    NSHourCalendarUnit|
    NSMinuteCalendarUnit|
    NSSecondCalendarUnit;
    
    [gregorian setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents* components = [gregorian components:unitFlags fromDate:today];
    
    //NSLog(@"System time : %@",components);
    return [CommonUtils monthNameFromNumber:components.month];
}

+(NSString *) weedDayFromDate:(NSDate *) today{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"us"]];
    
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:today];
    
    return dayName;
}

+(NSString *) weedDayFromDateInShort:(NSDate *) today{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"us"]];
    
    [dateFormatter setDateFormat:@"EE"];
    NSString *dayName = [dateFormatter stringFromDate:today];
    
    return dayName;
}


+(BOOL) yearoldFromDate:(NSDate *) dateofbirth limitedYearOld:(int) yearold{
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSYearCalendarUnit |
    NSMonthCalendarUnit |
    NSWeekdayCalendarUnit |
    NSDayCalendarUnit |
    NSHourCalendarUnit|
    NSMinuteCalendarUnit|
    NSSecondCalendarUnit;
    
    [gregorian setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents* components = [gregorian components:unitFlags fromDate:[NSDate date]];
    components.year = components.year - yearold;
    
    NSDate *checkedDate = [gregorian dateFromComponents:components];
    
    int cal = [checkedDate compare:dateofbirth];
    if (NSOrderedAscending == cal ) {
        return NO;
    }
    
    return YES;
}

+(void) printAllFonts{
    NSArray *names = [UIFont familyNames];
	NSArray *fontFaces;
	NSLog(@"Font FamilyNames");
    for (NSString *name in names)
    {
        NSLog(@"Font Family:  %@",name);
        fontFaces = [UIFont fontNamesForFamilyName:name];
        for (NSString *fname in fontFaces)
        {
            NSLog(@"              %@",fname);
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//DATETIMEUTILS


+(NSDate *) getUserTime{
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    return destinationDate;
}

+(NSDateComponents *) getSystemTime{
    NSDate *today = [CommonUtils getUserTime];
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSYearCalendarUnit |
    NSMonthCalendarUnit |
    NSWeekdayCalendarUnit |
    NSDayCalendarUnit |
    NSHourCalendarUnit|
    NSMinuteCalendarUnit|
    NSSecondCalendarUnit;
    
    [gregorian setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents* components = [gregorian components:unitFlags fromDate:today];
    
    //NSLog(@"System time : %@",components);
    return components;
}

+(NSDate *) getCurrentDateFromYear:(int) year month:(int) month day:(int) day{
    NSDateComponents *components = [NSDateComponents new];
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    [gregorian setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    components.year = year;
    components.month = month;
    components.day = day;
    components.hour = 10;
    components.minute = 0;
    components.second = 0;
    
    NSDate *date = [gregorian dateFromComponents: components];
    return date;
}


//+(NSDateComponents *) getSystemTimeComponentFrom:(NSDate*) today{
//    
//    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    unsigned unitFlags = NSYearCalendarUnit |
//    NSMonthCalendarUnit |
//    NSWeekdayCalendarUnit |
//    NSDayCalendarUnit |
//    NSHourCalendarUnit|
//    NSMinuteCalendarUnit|
//    NSSecondCalendarUnit;
//    
//    [gregorian setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//    NSDateComponents* components = [gregorian components:unitFlags fromDate:today];
//    
//    //NSLog(@"System time : %@",components);
//    return components;
//}


+(NSDateComponents *) getSystemTimeComponentFrom:(NSDate*) theday{
    
    NSDate* sourceDate = theday;
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* today = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    
    
    
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSYearCalendarUnit |
    NSMonthCalendarUnit |
    NSWeekdayCalendarUnit |
    NSDayCalendarUnit |
    NSHourCalendarUnit|
    NSMinuteCalendarUnit|
    NSSecondCalendarUnit;
    
    [gregorian setTimeZone:[NSTimeZone systemTimeZone]];
    NSDateComponents* components = [gregorian components:unitFlags fromDate:today];
    
    //NSLog(@"System time : %@",components);
    return components;
}


+ (NSDate *) getLocalTime{
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    return destinationDate;
}


+(NSDate *) getDateFromTodayWithDateDistance:(int) distanceDate{
    
    NSDate *today = [CommonUtils getLocalTime];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    [components setDay: -distanceDate];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *lastday = [gregorian dateByAddingComponents:components toDate:today options:0];
    
    return lastday;
}





@end
