//
//  MainTabbarCtrl.m
//  HotspotApp
//
//  Created by reoxinh on 7/19/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "MainTabbarCtrl.h"
#import "LoginViewCtrl.h"
@interface MainTabbarCtrl ()

@end
@implementation MainTabbarCtrl

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    NSString *gallabel = @"Settings screen";
    switch (tabBarController.selectedIndex) {
        case E_TAB_EXPLORER:
            gallabel = @"Explore screen";
            break;
        case E_TAB_POEMS:
            gallabel = @"Poems screen";
            break;
        case E_TAB_SUBMIT:
            gallabel = @"Submit screen";
            break;
        case E_TAB_MY_ENTRIES:
            gallabel = @"My Entries screen";
            break;
    }
    [APP_DELEGATE GALTrackerInCategory:@"Tab open" withAction:@"Open" withLabel:gallabel];
    
    
    if (tabBarController.selectedIndex == E_TAB_SETTING) {
        UITabBarItem *tbi = (UITabBarItem *)tabBarController.tabBar.items[4];
        tbi.badgeValue = nil;
    }else{
        [APP_DELEGATE updateUnreadEventCountBadge];
    }
    
    if(tabBarController.selectedIndex == E_TAB_SUBMIT || tabBarController.selectedIndex ==  E_TAB_MY_ENTRIES){
        if(!APP_DELEGATE.isLogined){
            //Replace root view by login view
            
           UINavigationController* nav =  [tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
            UIViewController *root = nav.viewControllers[0];
            if (! [root isKindOfClass:[LoginViewCtrl class]]) {
                
                LoginViewCtrl* lgViewCtrl = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN_VIEW_CTRL"];
                nav.viewControllers = [NSArray arrayWithObjects:lgViewCtrl, nil];

            }
        }else{
            //Replace loginview by rootview
            
            UINavigationController* nav =  [tabBarController.viewControllers objectAtIndex:tabBarController.selectedIndex];
            UIViewController *root = nav.viewControllers[0];
            if ([root isKindOfClass:[LoginViewCtrl class]]) {
                
                NSString *storyboardid = @"MY_ENTRIES_STORYBOARD";
                
                if(tabBarController.selectedIndex == E_TAB_SUBMIT){
                    storyboardid = @"NEW_POEMS_STORYBOARD";
                }
                
                UIViewController *root = [tabBarController.storyboard instantiateViewControllerWithIdentifier:storyboardid];
                nav.viewControllers = [NSArray arrayWithObjects:root, nil];
            }
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
