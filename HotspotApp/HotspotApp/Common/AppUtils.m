//
//  AppUtils.m
//  HotspotApp
//
//  Created by hmchau on 7/18/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "AppUtils.h"

@implementation AppUtils

+(NSString *) zonePrefixByZoneId:(int) zoneId{
    
    switch (zoneId) {
        case 5://Central
            return @"C";
            break;
            
        case 8://East
            return @"E";
            break;

        case 6://North
            return @"N";
            break;
            
        case 4://northeast
            return @"NE";
            break;
            
        case 7://West
            return @"W";
            break;
            
        case 10://Islands
            return @"I";
            break;
            
        default:
            break;
    }
    
    return @"";
}


+(NSString *) languageNameFromLanguageId:(int) languageId{
    switch (languageId) {
        case 1:
            return @"Eng";
            
        case 2:
            return @"中文";

        case 3 :
            return @"Tamil";
            
        case 4:
            return @"Melayu";
            
        default:
            break;
    }
    
    return @"Unknow";
}

+(int) getIndexLangugeButtonFromLanguageId:(int) languageId{
    switch (languageId) {
        case 1:
            return 0;
            
        case 2:
            return 1;
            
        case 3:
            return 3;
            
        case 4 :
            return 2;
            
        default:
            break;
    }
    
    return 0;
}

+(int) getLanguageIdFromButtonIndex:(int) buttonIndex{
    
    switch (buttonIndex) {
        case 0://English
            return 1;
            
        case 1://Chiness
            return 2;
            
        case 2://Malay
            return 4;
            
        case 3://Tamil
            return 3;
            
        default:
            break;
    }

    return 1;

}


+(NSString *) monthNameInShortFromNumber:(int) number{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"us"]];
    
    NSString *monthName = [[df shortMonthSymbols] objectAtIndex:(number-1)];
    return monthName;
}


+(double) distanceBetweenCoordinate:(CLLocationCoordinate2D) loc1 toOther:(CLLocationCoordinate2D) loc2{
    
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:loc1.latitude longitude:loc1.longitude];
    
    CLLocation *pinLocation = [[CLLocation alloc] initWithLatitude:loc2.latitude longitude:loc2.longitude ];
        
    CLLocationDistance distanceinmetre = [pinLocation distanceFromLocation:userLocation];
    
    return distanceinmetre;
}


+(BOOL) isValidContentWithSymbolChar:(NSString *) poemTitleContent{
    if (poemTitleContent == nil) {
        return NO;
    }

    NSCharacterSet* symbols = [NSCharacterSet symbolCharacterSet];
    NSString *filteredString;
    NSScanner *scanner = [NSScanner scannerWithString:poemTitleContent];
    [scanner scanUpToCharactersFromSet:symbols intoString:NULL];
    [scanner scanCharactersFromSet:symbols intoString:&filteredString];

    if (filteredString != nil && filteredString.length != 0) {

        return NO;
    }
    return YES;
}

+(BOOL) validStringAfterTrimWhitespace:(NSString *) str{
    if (str == nil) {
        return NO;
    }
    
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    return str.length != 0;
}

@end
