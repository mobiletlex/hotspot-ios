//
//  UISearchBar+Border.h
//  HotspotApp
//
//  Created by reoxinh on 7/19/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (Border)
-(void) setPinkBorder;
@end
