//
//  CommonUtils.h
//  RaoVatApp
//
//  Created by hmchau on 6/15/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonUtils : NSObject



+(BOOL) isValidPhone:(NSString *) number;
+(BOOL) isValidEmail:(NSString *)checkString;
+(int) randomNumber:(int) inside;
+ (NSString *)documentsPathForFileName:(NSString *)name;
+ (NSString *)resoucePathForFileName:(NSString *)name;

+(void) printAllFonts;

+(NSDateComponents *) getSystemTime;
+(NSDate *) getCurrentDateFromYear:(int) year month:(int) month day:(int) day;

//Date time utils
+(NSString *) timeStringFromSecond:(int) second;
+(NSString *) monthNameFromNumber:(int) number;
+(NSString *) monthNameInShortFromNumber:(int) number;
+(NSString *) monthNameFromDate:(NSDate *) today;
+(NSString *) weedDayFromDate:(NSDate *) today;
+(NSString *) weedDayFromDateInShort:(NSDate *) today;
+(BOOL) yearoldFromDate:(NSDate *) dateofbirth limitedYearOld:(int) yearold;

+(NSDateComponents *) getSystemTimeComponentFrom:(NSDate*) today;
+ (NSDate *) getLocalTime;
+(NSDate *) getDateFromTodayWithDateDistance:(int) distanceDate;
+ (NSString *) dateTimeStringFromDate:(NSDate *) date;
+ (NSString *) dateTimeStringFromYear:(int) year month:(int) month day:(int) day;

@end
