//
//  AppConfiguration.m
//  RaoVatApp
//
//  Created by hmchau on 6/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "AppConfiguration.h"
#import "MBProgressHUD.h"

@implementation AppConfiguration{
    
    NSString *_userEmail;
    NSString *_lastName;
    NSString *_firstName;

    
    
    
    NSString *_userBirthDay;
    NSString *_userLang;
    NSString *_avatarPath;
    
}

+(AppConfiguration *)sharedInstance{
    static AppConfiguration* instance;
    
    if (instance == nil) {
        instance = [[AppConfiguration alloc] init];
    }
    
    return instance;
}

+(BOOL) isLogin{
    
    if([AppConfiguration sharedInstance].sessionToken == nil || [[AppConfiguration sharedInstance].sessionToken isEqualToString:@""]){
        return NO;
    }
    return  YES;
}

-(id) init{
    self = [super init];
    if (!self) {
        
        return nil;
    }
    
    [self loadConfig];
    
    return self;
}
//Using
@synthesize sessionToken;
@synthesize fontSizePreview;
@synthesize userEmail = _userEmail;
@synthesize lastName = _lastName;


@synthesize cityName;
@synthesize cityId;
@synthesize userPassword;
@synthesize rememberAccount;
@synthesize userId;
@synthesize fristName = _firstName;
@synthesize avataPath = _avatarPath;
@synthesize userBirthDay = _userBirthDay;
@synthesize userLang = _userLang;

#define USER_SESSION_TOKEN @"session_toke_live"
#define USER_FONT_SIZE @"font_size"


#define USER_FIRSTUSE_KEY @"first_use"
#define USER_CITYNAME_KEY @"city_name"
#define USER_CITYID_KEY @"city_id"
#define USER_EMAIL_KEY @"user_email"
#define USER_PW_KEY @"user_password"
#define USER_REMEMBER_ACCOUNT_KEY @"remember_account"



#define USER_NAME_KEY @"user_name"
#define USER_BIRTHDAY @"user_birthday"
#define USER_LANG @"user_lang"

#define USER_FIRSTNAME @"user_firstName"
#define USER_LASTNAME @"user_lastName"
#define USER_AVATAR @"user_avatar"


-(void) loadConfig{
    
    self.firstRunApp = ![[NSUserDefaults standardUserDefaults] boolForKey:USER_FIRSTUSE_KEY];
    self.cityName = [[NSUserDefaults standardUserDefaults] stringForKey:USER_CITYNAME_KEY];
    self.cityId = [[NSUserDefaults standardUserDefaults] stringForKey:USER_CITYID_KEY];
    _userEmail = [[NSUserDefaults standardUserDefaults] stringForKey:USER_EMAIL_KEY];
    self.userPassword = [[NSUserDefaults standardUserDefaults] stringForKey:USER_PW_KEY];
    self.rememberAccount = [[NSUserDefaults standardUserDefaults] boolForKey:USER_REMEMBER_ACCOUNT_KEY];
    self.userId = nil;
    
    self.fontSizePreview = [[NSUserDefaults standardUserDefaults] floatForKey:USER_FONT_SIZE];
    if (self.fontSizePreview < 1.0) {
        self.fontSizePreview = 1.0;
    }
    self.sessionToken = [[NSUserDefaults standardUserDefaults] stringForKey:USER_SESSION_TOKEN];
    _firstName = [[NSUserDefaults standardUserDefaults] stringForKey:USER_FIRSTNAME];
    _lastName = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LASTNAME];

    
    _userBirthDay = [[NSUserDefaults standardUserDefaults] stringForKey:USER_BIRTHDAY];
    _userLang = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LANG];
    _avatarPath = [[NSUserDefaults standardUserDefaults] stringForKey:USER_AVATAR];
}

-(void) saveConfig{
    [[NSUserDefaults standardUserDefaults] setValue:self.sessionToken forKey:USER_SESSION_TOKEN];
    [[NSUserDefaults standardUserDefaults] setFloat:self.fontSizePreview forKey:USER_FONT_SIZE ];
    [[NSUserDefaults standardUserDefaults] setValue:_firstName forKey:USER_FIRSTNAME];
    [[NSUserDefaults standardUserDefaults] setValue:_lastName forKey:USER_LASTNAME];
    [[NSUserDefaults standardUserDefaults] setValue:_userEmail forKey:USER_EMAIL_KEY];

    
    [[NSUserDefaults standardUserDefaults] setBool:!self.firstRunApp forKey:USER_FIRSTUSE_KEY];
    [[NSUserDefaults standardUserDefaults] setValue:self.cityName forKey:USER_CITYNAME_KEY];
    [[NSUserDefaults standardUserDefaults] setValue:self.cityId forKey:USER_CITYID_KEY];
    [[NSUserDefaults standardUserDefaults] setValue:self.userPassword forKey:USER_PW_KEY];
    [[NSUserDefaults standardUserDefaults] setBool:self.rememberAccount forKey:USER_REMEMBER_ACCOUNT_KEY];
    
    [[NSUserDefaults standardUserDefaults] setValue:_userBirthDay forKey:USER_BIRTHDAY];
    [[NSUserDefaults standardUserDefaults] setValue:_userLang forKey:USER_LANG];
    [[NSUserDefaults standardUserDefaults] setValue:_avatarPath forKey:USER_AVATAR];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) clearSession{
    self.userId = nil;
    self.sessionToken = nil;
    self.userEmail = nil;
    _lastName = nil;
    _firstName = nil;

    _userBirthDay = nil;
    _userLang = nil;
    _avatarPath = nil;
    
    [self saveConfig];
}



@end
