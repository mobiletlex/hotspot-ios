//
//  AppConfiguration.h
//  RaoVatApp
//
//  Created by hmchau on 6/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppConfiguration : NSObject

//Using
@property (nonatomic, retain) NSString *sessionToken;
@property (assign) float fontSizePreview;
@property (nonatomic, retain) NSString *userEmail;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * fristName;


@property (assign) BOOL firstRunApp;
@property (nonatomic, strong) NSString * cityId;
@property (nonatomic, strong) NSString * cityName;
@property (nonatomic, retain) NSString *userPassword;
@property (assign) BOOL rememberAccount;
@property (nonatomic, retain) NSString *userId;

@property (nonatomic, retain) NSString *userBirthDay;
@property (nonatomic, retain) NSString *userLang;

@property (nonatomic, strong) NSString * avataPath;


-(void) loadConfig;

-(void) saveConfig;

-(void) clearSession;


+(AppConfiguration *)sharedInstance;
+(BOOL) isLogin;

@end
