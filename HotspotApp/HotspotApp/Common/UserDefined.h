//
//  UserDefined.h
//  RaoVatApp
//
//  Created by hmchau on 6/11/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//
#import "AppDelegate.h"
#ifndef RaoVatApp_UserDefined_h
#define RaoVatApp_UserDefined_h

#define POEM_STATUS_PUBLIC @"1"
#define POEM_STATUS_DRAFT @"2"

#define POEM_CATEGORY_FEATURED @"1"
#define POEM_CATEGORY_PUBLIC @"2"
#define POEM_CATEGORY_SCHOOL @"3"

#define INITIAL_REQUEST_ZONE 10244
#define INITIAL_REQUEST_LOCATION 10245
#define INITIAL_REQUEST_EVENTS 10246

#define SERVICE_BASE_URL @"https://textinthecity.sg/api/v2"
//#define SERVICE_BASE_URL @"http://textinthecity.epsilon-mobile.com/api/v2"


// HOTSPOT APP API
#define SERVICE_REQUEST_ZONE                    (SERVICE_BASE_URL @"/zones")
#define SERVICE_REQUEST_LOCATIONS_IN_ZONE       (SERVICE_BASE_URL @"/zones/%@/locations")

#define SERVICE_LOCATION_NEAR_BY                (SERVICE_BASE_URL @"/locations/near_by")
#define SERVICE_REQUEST_NEW_LOCATION            (SERVICE_BASE_URL @"/locations")
#define SERVICE_REQUEST_FEATURED_LOCATION       (SERVICE_BASE_URL @"/locations/featured")
#define SERVICE_REQUEST_LOCATION_DETAIL         (SERVICE_BASE_URL @"/locations/")

#define SERVICE_GET_POEMLIST                    (SERVICE_BASE_URL @"/poems")
#define SERVICE_LOAD_EXPLORER                   (SERVICE_BASE_URL @"/poems/random")
#define SERVICE_NEW_POEM                        (SERVICE_BASE_URL @"/poems")
#define SERVICE_LIKE_POEM                       (SERVICE_BASE_URL @"/poems/%@/like")
#define SERVICE_RATE_POEM                       (SERVICE_BASE_URL @"/poems/%@/rate")
#define SERVICE_REPORT_POEM                     (SERVICE_BASE_URL @"/poems/%@/report")
#define SERVICE_REQUEST_MY_ENTRIES              (SERVICE_BASE_URL @"/poems/my_entries")
#define SERVICE_EDIT_POEM                       (SERVICE_BASE_URL @"/poems/")
#define SERVICE_DELETE_POEM                     (SERVICE_BASE_URL @"/poems/")
#define SERVICE_SEARCH_POEM                     (SERVICE_BASE_URL @"/poems/search")
#define SERVICE_EVENT_LIST                      (SERVICE_BASE_URL @"/events")


#define SERVICE_LOGIN                           (SERVICE_BASE_URL @"/login")
#define SERVICE_REGISTER                        (SERVICE_BASE_URL @"/register")
#define SERVICE_RESET_PASSWORD                  (SERVICE_BASE_URL @"/request_reset_password")


#define URL_DOWNLOAD_APP @"https://itunes.apple.com/us/app/text-in-the-city/id917615665?ls=1&mt=8"
#define URL_DOWNLOAD_APP_ANDROID @"https://play.google.com/store/apps/details?id=com.epsilon.textinthecity"


#define NOTIFICATION_INIT_LOADING_DONE @"notification_initial_loading_done"
#define NOTIFICATION_LOCAION_UNLOCK @"notification_location_unlock"
#define NOTIFICATION_POSTED_NEW_POEM @"notification_post_new_poem_completed"


#define THEME_COLOR [UIColor colorWithRed:232.0/255.0 green:12.0/255.0 blue:138.0/255.0 alpha:1.0]
#define THEME_COLOR_GREEN [UIColor colorWithRed:29.0/255.0 green:165.0/255.0 blue:155.0/255.0 alpha:1.0]

#define APP_DELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS7_AND_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_DEBUGGING YES
#define IS_SIMULATION TARGET_IPHONE_SIMULATOR

#define X_API_SECRET_KEY [NSString stringWithFormat:@"%@",[ASIHTTPRequest base64forData:[@"##textinthecity##" dataUsingEncoding:NSUTF8StringEncoding]]]

//#define X_API_SECRET_LOGIN_KEY [NSString stringWithFormat:@"%@",[ASIHTTPRequest base64forData:[@"####vocards####" dataUsingEncoding:NSUTF8StringEncoding]]]


#endif


