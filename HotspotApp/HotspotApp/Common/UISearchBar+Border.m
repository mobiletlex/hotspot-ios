//
//  UISearchBar+Border.m
//  HotspotApp
//
//  Created by reoxinh on 7/19/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "UISearchBar+Border.h"

@implementation UISearchBar (Border)
-(void) setPinkBorder{
    UITextField *txfSearchField = [self valueForKey:@"_searchField"];
    [txfSearchField setBackgroundColor:[UIColor whiteColor]];
//    [txfSearchField setLeftView:UITextFieldViewModeNever];
//    [txfSearchField setBorderStyle:UITextBorderStyleRoundedRect];
    txfSearchField.layer.borderWidth = 1.0f;
    txfSearchField.layer.cornerRadius = 5.0f;
    txfSearchField.layer.borderColor = THEME_COLOR.CGColor;
}
@end
