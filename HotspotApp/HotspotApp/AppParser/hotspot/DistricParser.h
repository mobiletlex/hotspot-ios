//
//  DistricParser.h
//  HotspotApp
//
//  Created by hmchau on 7/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonParser.h"
#import "PoemListInfo.h"

@interface DistricParser : CommonParser

+(id) parseRowPoemList:(NSData*) data;

@end
