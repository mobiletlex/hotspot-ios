//
//  DistricParser.m
//  HotspotApp
//
//  Created by hmchau on 7/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "DistricParser.h"
#import "PoemInfo.h"

@implementation DistricParser

+(id) parse:(NSData*) data{
    if(data)
    {
        
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSDictionary *dicObject = [jsonParser objectWithString:jsonString];
        
        if (dicObject) {
            PoemListInfo * listpoem = [[PoemListInfo alloc] init];
            
            NSString *currentpage = [CommonParser getString:dicObject forKey:@"current_page"];
            listpoem.currentPage = [currentpage intValue];
            
            NSString *totalpage = [CommonParser getString:dicObject forKey:@"last_page"];
            listpoem.totalPage = [totalpage intValue];
            
            NSArray *arrayObject = [CommonParser getArray:dicObject forKey:@"data"];
            if(arrayObject)
            {
                listpoem.dataArray = [DistricParser parseArray: arrayObject ];
            }
            
            return listpoem;

        }
    }
    
    return nil;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            PoemInfo *item = [CommonParser parsePoemInfo:jsonObject ];
            
            [datas addObject:item];
        }
    }
    
    return datas;
}


+(id) parseRowPoemList:(NSData*) data{
    if(data)
    {
        
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *arrayObject = [jsonParser objectWithString:jsonString];
        
        if (arrayObject && [arrayObject isKindOfClass:[NSArray class]]) {

            PoemListInfo * listpoem = [[PoemListInfo alloc] init];

            if(arrayObject)
            {
                listpoem.dataArray = [DistricParser parseArray: arrayObject ];
            }
            
            return listpoem;
            
        }
    }
    
    return nil;
}



@end
