//
//  EventParser.h
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonParser.h"
#import "EventInfo.h"

@interface EventParser : CommonParser

@end
