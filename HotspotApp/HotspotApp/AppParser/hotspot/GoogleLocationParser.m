//
//  GoogleLocationParser.m
//  HotspotApp
//
//  Created by hmchau on 9/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "GoogleLocationParser.h"

@implementation GoogleLocationParser

+(id) parse:(NSData*) data{
    NSMutableArray *array;
    
    if(data)
    {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSDictionary *dicData = [jsonParser objectWithString:jsonString];
        NSArray *arrayObject = [CommonParser getArray:dicData forKey:@"results"];
        if(arrayObject)
		{
            array = [GoogleLocationParser parseArray: arrayObject ];
        }
    }
    
    return array;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            
            GoogleLocationInfo *item = [[GoogleLocationInfo alloc] init];
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            item.name = [CommonParser getString:jsonObject forKey:@"name"];
            item.address = [CommonParser getString:jsonObject forKey:@"vicinity"];
            
            if (item.name == nil || item.address == nil) {
                continue;
            }
            
            [datas addObject:item];
            
        }
    }
    return datas;
}


@end
