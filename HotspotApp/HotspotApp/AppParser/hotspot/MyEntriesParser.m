//
//  MyEntriesParser.m
//  HotspotApp
//
//  Created by hmchau on 7/28/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "MyEntriesParser.h"

@implementation MyEntriesParser


+(id) parse:(NSData*) data{
    if(data)
    {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *arrayObject = [jsonParser objectWithString:jsonString];
    
        if(arrayObject)
		{
            return  [MyEntriesParser parseArray: arrayObject ];
        }
    }
    
    return nil;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            PoemInfo *item = [CommonParser parsePoemInfo:jsonObject ];
            
            [datas addObject:item];
        }
    }
    return datas;
}


@end
