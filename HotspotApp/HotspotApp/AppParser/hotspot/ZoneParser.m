    //
//  ZoneParser.m
//  HotspotApp
//
//  Created by hmchau on 7/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ZoneParser.h"

@implementation ZoneParser

+(id) parse:(NSData*) data{
    NSMutableArray *array;
    
    if(data)
    {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *arrayObject = [jsonParser objectWithString:jsonString];
        if(arrayObject)
		{
            array = [ZoneParser parseArray: arrayObject ];
        }
    }
    
    return array;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            ZoneInfo *item = [[ZoneInfo alloc] init];
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            
            [item setName:[CommonParser getString:jsonObject forKey:@"name"]];
            [item setZoneId:[[CommonParser getNumber:jsonObject forKey:@"id"] intValue] ];
            
            double lat = [[CommonParser getNumber:jsonObject forKey:@"lat"] doubleValue];
            double log = [[CommonParser getNumber:jsonObject forKey:@"lon"] doubleValue];
            item.location = CLLocationCoordinate2DMake(lat, log);
            
            [datas addObject:item];
        }
    }
    return datas;
}


@end
