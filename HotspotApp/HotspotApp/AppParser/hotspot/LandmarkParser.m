//
//  LandmarkParser.m
//  HotspotApp
//
//  Created by hmchau on 7/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "LandmarkParser.h"

@implementation LandmarkParser


+(id) parse:(NSData*) data{
    NSMutableArray *array;
    
    if(data)
    {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *arrayObject = [jsonParser objectWithString:jsonString];
        if(arrayObject)
		{
            array = [LandmarkParser parseArray: arrayObject ];
        }
    }
    
    return array;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            LandmarkInfo *item = [[LandmarkInfo alloc] init];
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            
            [item setAddress:[CommonParser getString:jsonObject forKey:@"address"]];
            [item setName:[CommonParser getString:jsonObject forKey:@"name"]];
            [item setLocatinId:[[CommonParser getNumber:jsonObject forKey:@"id"] intValue] ];
            [item setZoneId:[[CommonParser getNumber:jsonObject forKey:@"zone_id"] intValue] ];
            item.unlock_required = [[CommonParser getNumber:jsonObject forKey:@"unlock_required"] boolValue];
            
            double lat = [[CommonParser getNumber:jsonObject forKey:@"lat"] doubleValue];
            double log = [[CommonParser getNumber:jsonObject forKey:@"lon"] doubleValue];
            item.location = CLLocationCoordinate2DMake(lat, log);
            [item setFunfact:[CommonParser getString:jsonObject forKey:@"fun_fact"]];
            
            NSArray* photosarray = [CommonParser getArray:jsonObject forKey:@"photos"];
            if(photosarray != nil && photosarray.count > 0){
                NSMutableArray *parsePhotos = [[NSMutableArray alloc] init];
                for (NSDictionary *item in photosarray) {
                    [parsePhotos addObject: [CommonParser getString:item forKey:@"link"] ];
                }
                
                [item setPhotos: parsePhotos];
            }
            
            
            [datas addObject:item];
        }
    }
    return datas;
}

+(id) parseLocationItem:(NSData*) data{
    
    SBJSON *jsonParser = [[SBJSON alloc] init];
    NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSDictionary *jsonObject = [jsonParser objectWithString:jsonString];
    
    if(jsonObject && [jsonObject isKindOfClass:[NSDictionary class]] )
    {
        LandmarkInfo *item = [[LandmarkInfo alloc] init];
        
        [item setAddress:[CommonParser getString:jsonObject forKey:@"address"]];
        [item setName:[CommonParser getString:jsonObject forKey:@"name"]];
        [item setLocatinId:[[CommonParser getNumber:jsonObject forKey:@"id"] intValue] ];
        [item setZoneId:[[CommonParser getNumber:jsonObject forKey:@"zone_id"] intValue] ];
        
        double lat = [[CommonParser getNumber:jsonObject forKey:@"lat"] doubleValue];
        double log = [[CommonParser getNumber:jsonObject forKey:@"lon"] doubleValue];
        item.location = CLLocationCoordinate2DMake(lat, log);
        [item setFunfact:[CommonParser getString:jsonObject forKey:@"fun_fact"]];

        NSArray* photosarray = [CommonParser getArray:jsonObject forKey:@"photos"];
        if(photosarray != nil && photosarray.count > 0){
            NSMutableArray *parsePhotos = [[NSMutableArray alloc] init];
            for (NSDictionary *item in photosarray) {
                [parsePhotos addObject: [CommonParser getString:item forKey:@"link"] ];
            }
            
            [item setPhotos: parsePhotos];
        }

        
        return item;
    }

    return nil;
}


@end
