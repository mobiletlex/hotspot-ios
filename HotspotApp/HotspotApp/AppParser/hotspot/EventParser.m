//
//  EventParser.m
//  HotspotApp
//
//  Created by hmchau on 9/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "EventParser.h"

@implementation EventParser

+(id) parse:(NSData*) data{
    NSMutableArray *array;
    
    if(data)
    {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *arrayObject = [jsonParser objectWithString:jsonString];
        if(arrayObject)
		{
            array = [EventParser parseArray: arrayObject ];
        }
    }
    
    return array;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            EventInfo *item = [[EventInfo alloc] init];
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            
            item.eventId = [CommonParser getString:jsonObject forKey:@"id"];
            item.title = [CommonParser getString:jsonObject forKey:@"title"];
            item.content = [CommonParser getString:jsonObject forKey:@"short_content"];
            item.eventUrl = [CommonParser getString:jsonObject forKey:@"link"];
            
            [datas addObject:item];
        }
    }
    return datas;
}


@end
