//
//  ExploreParser.m
//  HotspotApp
//
//  Created by hmchau on 7/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "ExploreParser.h"
#import "PoemInfo.h"

@implementation ExploreParser



+(id) parse:(NSData*) data{
    NSMutableArray *array;
    
    if(data)
    {
        SBJSON *jsonParser = [[SBJSON alloc] init];
        NSString* jsonString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *arrayObject = [jsonParser objectWithString:jsonString];
        if(arrayObject)
		{
            array = [ExploreParser parseArray: arrayObject ];
        }
    }
    
    return array;
}

+(NSMutableArray *) parseArray:(NSArray *) jsonArray{
    
    NSMutableArray* datas = [[NSMutableArray alloc]init];
    
    if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
        for(int i=0;i<jsonArray.count;i++){
            NSDictionary* jsonObject=[jsonArray objectAtIndex:i];
            PoemInfo *item = [CommonParser parsePoemInfo:jsonObject ];
            
            [datas addObject:item];
        }
    }
    return datas;
}

@end
