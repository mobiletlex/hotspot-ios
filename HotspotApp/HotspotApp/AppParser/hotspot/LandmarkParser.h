//
//  LandmarkParser.h
//  HotspotApp
//
//  Created by hmchau on 7/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonParser.h"
#import "LandmarkInfo.h"

@interface LandmarkParser : CommonParser


+(id) parseLocationItem:(NSData*) data;

@end
