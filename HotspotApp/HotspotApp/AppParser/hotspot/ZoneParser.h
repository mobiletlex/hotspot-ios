//
//  ZoneParser.h
//  HotspotApp
//
//  Created by hmchau on 7/14/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonParser.h"
#import "ZoneInfo.h"

@interface ZoneParser : CommonParser

@end
