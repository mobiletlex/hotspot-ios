//
//  GoogleLocationParser.h
//  HotspotApp
//
//  Created by hmchau on 9/13/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonParser.h"
#import "GoogleLocationInfo.h"

@interface GoogleLocationParser : CommonParser

@end
