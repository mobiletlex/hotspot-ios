//
//  CommonParser.m
//  RaoVatApp
//
//  Created by hmchau on 6/15/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import "CommonParser.h"
#import "PoemInfo.h"

@implementation CommonParser

+(id) parse:(NSData*) data{
    return nil;
}

+(NSString*) getString:(NSDictionary*)jsonObject forKey:(NSString*)key {
    NSString* value = nil;
    @try {
        if(jsonObject)
        {
            value=[jsonObject valueForKey:key];
            
            if((NSNull*)value==[NSNull null]){
                value=nil;
            }
            
        }
    } @catch (NSException* e) {
        NSLog(@"%@",e);
    }
    return value;
}
+(NSDictionary*) getDictionary:(NSDictionary*) jsonObject forKey:(NSString*)key {
    NSDictionary* value = nil;
    @try {
        if(jsonObject)
        {
            value=[jsonObject valueForKey:key];
            
            
            if((NSNull*)value==[NSNull null]){
                value=nil;
            }
        }
    } @catch (NSException* e) {
        NSLog(@"%@",e);
    }
    return value;
}
+(NSMutableArray*) getArray:(NSDictionary*) jsonObject forKey:(NSString*)key {
    NSMutableArray* value = nil;
    @try {
        if(jsonObject)
        {
            value=[jsonObject valueForKey:key];
            
            
            if((NSNull*)value==[NSNull null]){
                value=nil;
            }
            
        }
    } @catch (NSException* e) {
        NSLog(@"%@",e);
    }
    return value;
}
+(NSNumber*) getNumber:(NSDictionary*) jsonObject forKey:(NSString*)key {
    NSNumber* value = nil;
    @try {
        if(jsonObject)
        {
            value=[jsonObject valueForKey:key];
            
            
            if((NSNull*)value==[NSNull null]){
                value=nil;
            }
        }
    } @catch (NSException* e) {
        NSLog(@"%@",e);
    }
    return value;
}


+(PoemInfo *) parsePoemInfo:(NSDictionary*) jsonObject{
    PoemInfo *item = [[PoemInfo alloc] init];
    
    [item setAuthor:[CommonParser getString:jsonObject forKey:@"author"]];
    if ([item.author isEqualToString:@""]) {
        item.author = @"Unknow";
    }
    
    NSString *locationid = [CommonParser getString:jsonObject forKey:@"location_id"];
    [item setLocationId: [locationid intValue] ];
    
    NSString *zoneid = [CommonParser getString:jsonObject forKey:@"zone_id"];
    item.zoneId = [zoneid intValue];
    
    [item setAudiolink:[CommonParser getString:jsonObject forKey:@"audio_link"]];
    [item setUrlshare:[CommonParser getString:jsonObject forKey:@"url"]];
    [item setPoemUrlShare:[CommonParser getString:jsonObject forKey:@"url"]];
    
    [item setCategory_id: [NSString stringWithFormat:@"%@",[CommonParser getString:jsonObject forKey:@"category_id"]]];
    [item setPoemid:[NSString stringWithFormat:@"%@",[CommonParser getString:jsonObject forKey:@"id"]]];
    [item setUpdated_at:[CommonParser getString:jsonObject forKey:@"updated_at"]];
    NSNumber *liked = [CommonParser getNumber:jsonObject forKey:@"is_liked"];
    if (liked) {
        item.liked = [liked boolValue];
    }
    
    NSNumber *reported = [CommonParser getNumber:jsonObject forKey:@"is_reported"];
    if (reported) {
        item.reported = [reported boolValue];
    }
    
    NSNumber *rate = [CommonParser getNumber:jsonObject forKey:@"average_rating"];
    if (rate) {
        item.average_rating = [rate floatValue];
    }
    
    NSArray* photosarray = [CommonParser getArray:jsonObject forKey:@"photos"];
    if(photosarray != nil && photosarray.count > 0){
        NSMutableArray *parsePhotos = [[NSMutableArray alloc] init];
        NSMutableArray *photocaptions = [[NSMutableArray alloc] init];

        for (NSDictionary *item in photosarray) {
            [photocaptions addObject: [CommonParser getString:item forKey:@"caption"] ];
            NSDictionary *linkDics = [CommonParser getDictionary:item forKey:@"links"];
            if (linkDics) {
                [parsePhotos addObject: [CommonParser getString:linkDics forKey:@"standard"] ];
            }else{
                [parsePhotos addObject: @""];
            }

        }
        
        [item setPhotos: parsePhotos];
        [item setPhotoCaptions:photocaptions];
    }

    NSDictionary *poet = [CommonParser getDictionary:jsonObject forKey:@"poet"];
    if (poet) {
        item.poetInfo = [[PoetIInfo alloc] init];
        item.poetInfo.poetId = [CommonParser getString:poet forKey:@"id"];
        item.poetInfo.name = [CommonParser getString:poet forKey:@"name"];
        item.poetInfo.pen_name = [CommonParser getString:poet forKey:@"pen_name"];
        item.poetInfo.bio = [CommonParser getString:poet forKey:@"bio"];
        
        NSDictionary *images = [CommonParser getDictionary:poet forKey:@"photo_links"];
        if (images) {
            item.poetInfo.photourl = [CommonParser getString:images forKey:@"avatar"];

        }
    }
    
    
//    NSArray* contentarr = [CommonParser getArray:jsonObject forKey:@"contents"];
//    if(contentarr != nil && contentarr.count > 0){
//        
//        NSDictionary* maincontents;
//        
//        if(contentarr.count > 1){
//            //Load sub language content
//            NSDictionary* subcontents = [contentarr objectAtIndex:0];
//            int templangid = [[CommonParser getString:subcontents forKey:@"language_id"] intValue];
//            if (templangid == 1){//Sublang is english
//                maincontents = [contentarr objectAtIndex:1];
//
//            }else{
//                maincontents = [contentarr objectAtIndex:0];
//                subcontents = [contentarr objectAtIndex:1];
//            }
//            
//            item.subLanguagePoem = [[PoemInfo alloc] init];
//            
//            item.subLanguagePoem.languageId = [[CommonParser getString:subcontents forKey:@"language_id"] intValue];
//            item.subLanguagePoem.title = [CommonParser getString:subcontents forKey:@"title"];
//            item.subLanguagePoem.content = [CommonParser getString:subcontents forKey:@"content"];
//            
//            //set main language
//            item.languageId = [[CommonParser getString:maincontents forKey:@"language_id"] intValue];
//            [item setTitle:[CommonParser getString:maincontents forKey:@"title"]];
//            [item setContent:[CommonParser getString:maincontents forKey:@"content"]];
//            
//        }else{
//            
//            //set main language
//            maincontents = [contentarr objectAtIndex:0];
//            item.languageId = [[CommonParser getString:maincontents forKey:@"language_id"] intValue];
//            [item setTitle:[CommonParser getString:maincontents forKey:@"title"]];
//            [item setContent:[CommonParser getString:maincontents forKey:@"content"]];
//        }
//    }
    
    item.title = [CommonParser getString:jsonObject forKey:@"title"];
    item.content = [CommonParser getString:jsonObject forKey:@"content"];
    item.languageId = [[CommonParser getString:jsonObject forKey:@"language_id"] intValue];

    //english ver
    NSString *eng_title = [CommonParser getString:jsonObject forKey:@"title_alt"];
    NSString *eng_content = [CommonParser getString:jsonObject forKey:@"content_alt"];
    if (eng_title && eng_content && item.languageId != 1) {
        item.subLanguagePoem = [[PoemInfo alloc] init];
        
        item.subLanguagePoem.languageId = 1;//english
        item.subLanguagePoem.title = eng_title;
        item.subLanguagePoem.content = eng_content;
    }
    
    return item;
}

@end
