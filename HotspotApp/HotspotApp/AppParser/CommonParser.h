//
//  CommonParser.h
//  RaoVatApp
//
//  Created by hmchau on 6/15/14.
//  Copyright (c) 2014 MobileHouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+SBJSON.h"
#import "SBJSON.h"

@class PoemInfo;

@interface CommonParser : NSObject

+(id) parse:(NSData*) data;



+(NSString*) getString:(NSDictionary*)jsonObject forKey:(NSString*)key ;
+(NSDictionary*) getDictionary:(NSDictionary*) jsonObject forKey:(NSString*)key ;
+(NSMutableArray*) getArray:(NSDictionary*) jsonObject forKey:(NSString*)key ;
+(NSNumber*) getNumber:(NSDictionary*) jsonObject forKey:(NSString*)key;

+(PoemInfo *) parsePoemInfo:(NSDictionary*) jsonObject;

@end
